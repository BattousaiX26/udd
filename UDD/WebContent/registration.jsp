<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- css -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/background.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/Registration/registration.css">
<title>Registration</title>
</head>
<body>
<br>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6 pb-5">
	        <!--Form with header-->
	        <form class="validatedForm" id="form">
	            <div class="card border-info rounded-0">
	                <div class="card-header p-0">
	                    <div class="bg-primary text-white text-center py-2">
	                        <h2><i class="fas fa-registered"></i></i>Registration</h2>
	                        <p class="m-0 font-italic">Employees can register their account here!</p>
	                    </div>
	                </div>
	                <div class="card-body p-3">
	                    <!--Body-->	           
	                    <div class="form-group">
	                        <div class="input-group mb-2">
	                            <div class="input-group-prepend">
	                                <div class="input-group-text"><i class="fa fa-user text-primary"></i></div>
	                            </div>
	                            <input type="text" class="form-control inputField" id="username" name="username" placeholder="Enter desired username" onkeyup="loadXMLDoc()" required>	                       		
	                        </div>
	                        <span id="err"></span> 	               
	                    </div>	              
	                    <div class="form-group">
	                        <div class="input-group mb-2">
	                            <div class="input-group-prepend">
	                                <div class="input-group-text"><i class="fas fa-key text-primary"></i></div>
	                            </div>
	                            <input type="password" class="form-control inputField" id="password" name="password" placeholder="Enter desired password" required>
	                        </div>	                    
	                    </div>
	                    <div class="form-group">
	                        <div class="input-group mb-2">
	                            <div class="input-group-prepend">
	                                <div class="input-group-text"><i class="fas fa-key text-primary"></i></div>
	                            </div>
	                            <input type="password" class="form-control inputField" id="confirm_password" name="confirm_password" placeholder="Confirm password" onchange="check()" required>                                                   
	                        </div>
	                        <span id='message' class="font-italic"></span> <!-- shows password matching -->
	                    </div>
	                    <div class="form-group">
	                        <div class="input-group mb-2">
	                            <div class="input-group-prepend">
	                                <div class="input-group-text"><i class="fa fa-user text-primary"></i></div>
	                            </div>
	                            <input type="text" class="form-control inputField" id="fullname" name="fullname" placeholder="Enter full name" required>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <div class="input-group mb-2">	                   
	                            <div class="input-group-prepend">
	                                <div class="input-group-text"><i class="fas fa-universal-access text-primary"></i></div>
	                            </div>
	                            <select class="form-control inputField" id="user_level" name="user_level" required>
	                            	<option selected disabled>Choose user level</option>
	                            	<option value="user">User</option>
	                            	<option value="administrator">Administrator</option>
	                            </select>
	                        </div>
	                    </div> 
	                    <div class="form-group">
	                        <div class="input-group mb-2">
	                            <div class="input-group-prepend">
	                                <div class="input-group-text"><i class="fas fa-building text-primary"></i></div>
	                            </div>
	                            <select class="form-control inputField" id="section" name="section" required>
	                            	<option selected disabled>-----Choose section-----</option>
	                            	<option value="receiving">Receiving</option>
	                            	<option value="evaluation">Evaluation</option>
	                            	<option value="releasing">Releasing</option>
	                            	<option value="printing">Printing</option>
	                            	<option value="finalEvaluation">Final Evaluation</option>
	                            </select>	                           
	                        </div>
	                    </div>    
	                    <div class="form-group" id="registrationStatus"></div> 
	                    <div class="form-group text-center"> 
	                    	Click <a href="${pageContext.request.contextPath}">here</a> to go to Login page
	                    </div> 
	                    <div class="text-center">
	                        <input type="button" id="btnRegister" value="Register" class="btn btn-primary btn-block rounded-0 py-2">
	                    </div>
	                </div>
	            </div>
	        </form>
	    </div>
	</div>
</div>
<!-- script -->
<script defer src="${pageContext.request.contextPath}/JS/jquery-3.2.1/jquery-3.2.1.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/js/bootstrap.bundle.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/js/bootstrap.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS//bootbox/bootbox.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/fontawesome-free-5.0.8/svg-with-js/js/fontawesome-all.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/registration/registration.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/registration/errorMessage.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/registration/availability.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/save.js"></script>
</body>
</html>