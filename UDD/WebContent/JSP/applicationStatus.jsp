<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Application Status</title>
</head>
<body>
<jsp:include page="navbar.jsp" />

<div class="container p-5">
	<div class="row">
		<c:if test="${empty recordsWithStatus}">
			<div class="col-md-3"></div>
			<div class="col-md-6" id="mpNotFound">
				 <div class="card border-danger rounded-0 text-center">
				 	<div class="card-header p-0">
			 		  	<div class="bg-danger text-white text-center py-2">
	                        <h2><i class="fas fa-exclamation-triangle text-warning"></i>Warning!<i class="fas fa-exclamation-triangle text-warning"></i></h2>		                                          
	                    </div>
				 	</div>
				 	<div class="card-body p-3">
	                	<div class="text-center py-2">
	                        <h1 class="text-danger">TRANSACTION NUMBER NOT FOUND!</h1>	                                                          
	                    </div>
	                    <div class="text-center py-2">
	                        Click <a href="${pageContext.request.contextPath}/JSP/releasing.jsp">here</a> to go back	                                                          
	                    </div>
	                </div>
				 </div>
        	</div>  
        	<div class="col-md-3"></div>   
		</c:if>
		<div class="col">
			<c:forEach items="${recordsWithStatus}" var="items">
				<form>
					<div class="card border-info rounded-0">
						<div class="card-header p-0">
		                    <div class="bg-primary text-white text-center py-2">
		                        <h2><i class="fas fa-flag"></i>Application Status</h2>	                      	          
		                    </div>                 
			        	</div>
			        	<div class="card-body p-3">     
			        		<div class="form-row">
			        			<div class="form-group col-sm-6">	
			                    	<label for="mpNo">MP Number</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="mpNo" id="mpNo" value="<c:out value="${items.mpNo}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>	
			                    <div class="form-group col-sm-6">	
			                    	<label for="transactionNo">Transaction Number</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="transactionNo" id="transactionNo" value="<c:out value="${items.transactionNumber}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>	
			        		</div>
			        		<div class="form-row">
			        			<div class="form-group col-sm-6">	
			                    	<label for="businessName">Business Name</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="businessName" id="businessName" value="<c:out value="${items.businessName}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>	
			                    <div class="form-group col-sm-6">	
			                    	<label for="status" class="font-italic font-weight-bold text-danger"> Application Status <i class="blink fas fa-exclamation-triangle text-warning"></i></label><br>          
			                    	<div class="input-group mb-2 ">	           		  	                           									
										<input type="text" class="form-control" name="status" id="status" value="<c:out value="${items.status}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>				                
			        		</div>
			        		<div class="form-row">
			        			<div class="form-group col-sm-6">	
			                    	<label for="address">Address</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="address" id="address" value="<c:out value="${items.address}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>	
			                    <div class="form-group col-sm-3">	
			                    	<label for="dateApplied">Date Applied / Encoded</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="dateApplied" id="dateApplied" value="<c:out value="${items.dateApplied}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>	
			                    <div class="form-group col-sm-3">	
			                    	<label for="dateIssued">Date Issued / Evaluated</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="dateIssued" id="dateIssued" value="<c:out value="${items.dateIssued}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>	
			        		</div>
			        		<div class="form-row">
			        			<div class="form-group col-sm-3">	
			                    	<label for="district">District</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="district" id="district" value="<c:out value="${items.district}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>	
			                    <div class="form-group col-sm-5">	
			                    	<label for="barangay">Barangay</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="barangay" id="barangay" value="<c:out value="${items.barangay}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>				         
			        		</div>
			        	</div>
					</div>
				</form>
			</c:forEach>
		</div>
	</div>
</div>
<script defer src="${pageContext.request.contextPath}/JS/bounce.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/jquery-blink/jquery.blink.js"></script>	
<script defer src="${pageContext.request.contextPath}/JS/blink.js"></script>
</body>
</html>