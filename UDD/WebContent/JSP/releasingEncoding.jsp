<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Releasing Encoding</title>
</head>
<body>
<jsp:include page="navbar.jsp" />

<c:if test="${not empty releasingRecords }">
<div class="container p-5">
	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<div class="card border-info rounded">
				<div class="card-header p-0">
	                <div class="bg-primary text-white text-center py-2">
	                    <h2><i class="fas fa-keyboard"></i>Encoding</h2>	                                      
	                </div>
	            </div>	          
	            <div class="card-body">
	              	<c:forEach items="${releasingRecords}" var="items">
		                <form id="form">
			                <div class="form-row">
								<div class="form-group col-sm-6">	
			                    	<label for="mpNo">MP Number</label><br>          
			                    	<div class="input-group mb-2">	                          	                           
										<input type="text" class="form-control" name="mpNo" id="mpNo" value="<c:out value="${items.mpNumber}"/>" readonly>   
			                        </div>	                                           
				                </div>
				                <div class="form-group col-sm-6">	
			                    	<label for="transactionNo">Transaction Number</label><br>          
			                    	<div class="input-group mb-2">	                          	                           
										<input type="text" class="form-control" name="transactionNo" id="transactionNo" value="<c:out value="${items.transactionNumber}"/>" readonly>   
			                        </div>	                                           
				                </div>
				            </div>	
				            <div class="form-row">
								<div class="form-group col-sm-12">	
			                    	<label for="businessName">Business Name</label><br>          
			                    	<div class="input-group mb-2">	                          	                           
										<input type="text" class="form-control" name="businessName" id="businessName" value="<c:out value="${items.businessName}"/>" readonly>   
			                        </div>	                                           
				                </div>			              
				            </div>
				            <div class="form-row">
								<div class="form-group col-sm-12">	
			                    	<label for="businessName">Taxpayer Name</label><br>          
			                    	<div class="input-group mb-2">	                          	                           
										<input type="text" class="form-control" name="taxPayerName" id="taxPayerName" value="<c:out value="${items.taxPayerName}"/>" readonly>   
			                        </div>	                                           
				                </div>			              
				            </div>
				            <div class="form-row">
								<div class="form-group col-sm-4">	
			                    	<label for="amount">Amount</label><br>          
			                    	<div class="input-group mb-2">	                          	                           
										<input type="text" class="form-control" name="amount" id="amount" value="<c:out value="${items.amount}"/>" readonly>   
			                        </div>	                                           
				                </div>
				                <div class="form-group col-sm-4">	
			                    	<label for="sealNo">Seal Number</label><br>          
			                    	<div class="input-group mb-2">	                          	                           
										<input type="text" class="form-control required-control" name="sealNo" id="sealNo" value="<c:out value="${items.sealNumber}"/>">   
			                        </div>	                                           
				                </div>
				                <div class="form-group col-sm-4">	
			                    	<label for="receiptNo">Receipt Number</label><br>          
			                    	<div class="input-group mb-2">	                          	                           
										<input type="text" class="form-control required-control" name="receiptNo" id="receiptNo" value="<c:out value="${items.receiptNumber}"/>">   
			                        </div>	                                           
				                </div>
				            </div>				  
				            <div class="text-center p-2">
				             	<p class="text-center" id="saveStatus"></p>  
		                		<button type="button" id="btnSave" value="Save" class="btn btn-primary btn-block rounded-0 py-2"><span><i class="fas fa-save"></i>Save</span></button>
		                	</div>				   
						</form> 
					</c:forEach>               
	            </div>
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
</div>
</c:if>

<script defer src="${pageContext.request.contextPath}/JS/releasing.js"></script>	
</body>
</html>