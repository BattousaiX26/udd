<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Releasing</title>
</head>
<body>
<jsp:include page="navbar.jsp" />

<div class="jumbotron">
   	<h1 class="display-5">Releasing</h1>
    <p class="lead">Retrieve and view saved record, generate order of payment or check status of application!</p>
</div>
<div class="container p-2">
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="card mb-4 border-info rounded">
				<div class="card-header p-0">
	                   <div class="bg-primary text-white text-center py-2">
	                       <h2><i class="fas fa-search"></i>Releasing</h2>	                                          
	                   </div>
	            </div>
	            <div class="card-body text-center">
	                <form action="${pageContext.request.contextPath}/JSP/releasing" method="get">
				    	<div class="form-group">										    	
							<input type="text" class="form-control" name="transactionNo" id="transactionNo" placeholder="Enter Transaction Number here">				      	
						    <br>	
						    <div class="form-group">
						    	<select name="action" id="action" class="form-control" required>
						      		<option disabled selected value = "">-----Choose Action-----</option>
						      		<option value="getMasterRecords">Retrieve record based on Transaction Number</option>
						      		<option value="getReleasingRecords">View Saved Releasing Record</option>
						      		<option value="generateOrderOfPayment">Generate Order of Payment</option>
						      		<option value="checkStatus">Check Status of Application</option>				    
						      	</select>	   
						    </div>
						    <button type="submit" class="btn btn-primary btn-md btn-block"><i class="fas fa-search"></i>Search</button>		
						</div>  
		  			</form>   
	            </div>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
</div>

</body>
</html>