<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Reports Searched</title>
<style>
table{
	white-space:nowrap;
}
</style>
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/DataTables-1.10.18/css/dataTables.bootstrap.min.css">
</head>
<body>
<jsp:include page="navbar.jsp" />

<div class="container-fluid p-5">
	<div class="row bg-light h-100">
		<div class="col-sm-12 text-center">
			<h1 class="text-center">REPORTS</h1>
		</div>
		<div class="col-sm-12 table-responsive">
		    <table class="table table-bordered table-paginate table-striped table-paginate display" style="width:100%" id="table-reports">
		    	<thead class="thead-dark">
				    <tr>
				      <th scope="col">#</th>
				      <th scope="col">TRANSACTION NO</th>
				      <th scope="col">BUSINESS_NAME</th>
				      <th scope="col">TAXPAYERS_NAME</th>
				      <th scope="col">NO_OF_EMPLOYEES_MALE</th>
				      <th scope="col">NO_OF_EMPLOYEES_FEMALE</th>
				      <th scope="col">SEX</th>
				      <th scope="col">RIGHT_OVER_LAND</th>
				      <th scope="col">LANDRATE</th>
				      <th scope="col">CAPITALIZATION</th>
				      <th scope="col">ADDRESS</th>
				      <th scope="col">BARANGAY</th>
				      <th scope="col">DISTRICT</th>
				      <th scope="col">BUSINESS_ACTIVITY_MAINTILE1</th>
				      <th scope="col">BUSINESS_ACTIVITY_SUBTILE1</th>
				      <th scope="col">BUSINESS_ACTIVITY_MAINTILE2</th>
				      <th scope="col">BUSINESS_ACTIVITY_SUBTILE2</th>
				      <th scope="col">BUSINESS_ACTIVITY_MAINTILE3</th>
				      <th scope="col">BUSINESS_ACTIVITY_SUBTILE3</th>
				      <th scope="col">BUSINESS_ACTIVITY_MAINTILE4</th>
				      <th scope="col">BUSINESS_ACTIVITY_SUBTILE4</th>
				      <th scope="col">BUSINESS_ACTIVITY_MAINTILE5</th>
				      <th scope="col">BUSINESS_ACTIVITY_SUBTILE5</th>
				      <th scope="col">MP NUMBER</th>
				      <th scope="col">CPDO NUMBER</th>
				      <th scope="col">DATE_APPLIED</th>
				      <th scope="col">DATE_VALID</th>
				      <th scope="col">DATE_ISSUED</th>
				      <th scope="col">VALID_UNTIL</th>
				      <th scope="col">STATUS</th>
				      <th scope="col">VERIFIER</th>
				      <th scope="col">LOT_AREA</th>
				      <th scope="col">BUSINESS_AREA</th>
				      <th scope="col">PISC</th>
				      <th scope="col">ZONING_CLASS</th>
				      <th scope="col">PISC</th>
				      <th scope="col">ZONING_CLASS</th>
				      <th scope="col">EVALUATION</th>
				      <th scope="col">ZONING_OFFICIAL</th>
				      <th scope="col">REMARKS</th>
				      <th scope="col">REMARKS2</th>
				      <th scope="col">REMARKS3</th>
				      <th scope="col">OTHER_REMARKS</th>
				      <th scope="col">OTHER_REMARKS2</th>
				      <th scope="col">OTHER_REMARKS3</th>
				      <th scope="col">NOTICE_OF_ACTION</th>
				      <th scope="col">OTHER_NOTICE_OF_ACTION</th>
				      <th scope="col">YEAR_ADDED</th>
				      <th scope="col">EVALUATION_OF_FACTS</th>
				      <th scope="col">TOTAL</th>
				      <th scope="col">TOTAL_IN_WORDS</th>
				      <th scope="col">APPLICATION_STATUS</th>
				    </tr>
		    	</thead>	
		    	<tbody>   	
			        <c:forEach items="${reportItems}" var="reportItems" varStatus="count">
			            <tr>		
			            	<th scope="row"><c:out value="${count.count}"/></th>	
			                <td><c:out value="${reportItems.transactionNumber}" /></td>
			                <td><c:out value="${reportItems.businessName}" /></td>
			                <td><c:out value="${reportItems.taxPayerName}" /></td>
			                <td><c:out value="${reportItems.noOfEmployeesMale}" /></td>
			                <td><c:out value="${reportItems.noOfEmployeesFemale}" /></td>
			                <td><c:out value="${reportItems.sex}" /></td>
			                <td><c:out value="${reportItems.rightOverLand}" /></td>
			                <td><c:out value="${reportItems.landRate}" /></td>
			                <td><c:out value="${reportItems.capitalization}" /></td>
			                <td><c:out value="${reportItems.address}" /></td>
			                <td><c:out value="${reportItems.barangay}" /></td>
			                <td><c:out value="${reportItems.district}" /></td>
			                <td><c:out value="${reportItems.businessActivityMainTile1}" /></td>
			                <td><c:out value="${reportItems.businessActivitySubTile1}" /></td>
			                <td><c:out value="${reportItems.businessActivityMainTile2}" /></td>
			                <td><c:out value="${reportItems.businessActivitySubTile2}" /></td>
			                <td><c:out value="${reportItems.businessActivityMainTile3}" /></td>
			                <td><c:out value="${reportItems.businessActivitySubTile3}" /></td>
			                <td><c:out value="${reportItems.businessActivityMainTile4}" /></td>
			                <td><c:out value="${reportItems.businessActivitySubTile4}" /></td>
			                <td><c:out value="${reportItems.businessActivityMainTile5}" /></td>
			                <td><c:out value="${reportItems.businessActivitySubTile5}" /></td>
			                <td><c:out value="${reportItems.mpNo}" /></td>
			          		<td><c:out value="${reportItems.cpdoNo}" /></td>
			          		<td><c:out value="${reportItems.dateApplied}" /></td>
			          		<td><c:out value="${reportItems.dateValid}" /></td>
			          		<td><c:out value="${reportItems.dateIssued}" /></td>
			          		<td><c:out value="${reportItems.validUntil}" /></td>
			          		<td><c:out value="${reportItems.status}" /></td>
			          		<td><c:out value="${reportItems.verifier}" /></td>
			          		<td><c:out value="${reportItems.mpNo}" /></td>
			          		<td><c:out value="${reportItems.lotArea}" /></td>
			          		<td><c:out value="${reportItems.businessArea}" /></td>
			          		<td><c:out value="${reportItems.mpNo}" /></td>
			          		<td><c:out value="${reportItems.pisc}" /></td>
			          		<td><c:out value="${reportItems.zoningClass}" /></td>
			          		<td><c:out value="${reportItems.evaluation}" /></td>
			          		<td><c:out value="${reportItems.zoningOfficial}" /></td>
			          		<td><c:out value="${reportItems.remarks}" /></td>
			          		<td><c:out value="${reportItems.remarks2}" /></td>
			          		<td><c:out value="${reportItems.remarks3}" /></td>
			          		<td><c:out value="${reportItems.otherRemarks}" /></td>
			          		<td><c:out value="${reportItems.otherRemarks2}" /></td>
			          		<td><c:out value="${reportItems.otherRemarks3}" /></td>
			          		<td><c:out value="${reportItems.noticeOfAction}" /></td>
			          		<td><c:out value="${reportItems.otherNoticeOfAction}" /></td>
			          		<td><c:out value="${reportItems.yearAdded}" /></td>
			          		<td><c:out value="${reportItems.evaluationOfFacts}" /></td>
			          		<td><c:out value="${reportItems.total}" /></td>
			          		<td><c:out value="${reportItems.totalInWords}" /></td>
			          		<td><c:out value="${reportItems.applicationStatus}" /></td>
			            </tr>
			        </c:forEach>
		        </tbody>
		    </table>
		</div>
	</div>
</div>
<script defer src="${pageContext.request.contextPath}/CSS/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/DataTables-1.10.18/js/dataTables.buttons.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/htmlButtons/1.5.2/buttons.html5.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/htmlButtons/1.5.2/buttons.print.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/htmlButtons/1.5.2/buttons.colVis.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/table/tableToDocuments.js"></script>
</body>
</html>