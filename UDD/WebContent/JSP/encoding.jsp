<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Encoding</title>
</head>
<body>
<jsp:include page="navbar.jsp" />
<div class="container-fluid p-5">
	<div class="row">
		<div class="col">
	        <!--Form with header-->
	        <form class="validatedForm" id="form" action="receiving" method="post" id="encodingForm">
	            <div class="card border-info rounded-0">
	         		<div class="card-header p-0">
	                    <div class="bg-primary text-white text-center py-2">
	                        <h2><i class="fas fa-keyboard"></i>Encoding</h2>	
	                        <p>Encode new application to generate transaction number</p>                        
	                    </div>
	                </div>
	                <div class="card-body p-3">
	                	<div class="form-row">
	                    	<div class="form-group col-md-4">	
		                    	<label for="businessName">Business Name / Trade Name</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="businessName" id="businessName">   
		                        </div>	                                           
		                    </div>	                	                    
		                    <div class="form-group col-md-4">	
		                    	<label for="taxPayerName">Taxpayer Name</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="taxPayerName" id="taxPayerName">   
		                        </div>	                                           
		                    </div>	
		                    <div class="form-group col-md-2">	
		                    	<label for="mpNo">MP No.</label><br>       
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="mpNo" id="mpNo" required>   
		                        </div>	                                        
		                    </div>
		                    <div class="form-group col-md-2">	
		                    	<label for="capitalization">Capitalization</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="capitalization" id="capitalization">   
		                        </div>	                                           
		                    </div>			                  	                                 		                         	       
	                    </div>
	                    <div class="form-row">                 
		                    <div class="form-group col-md-3">	
		                    	<label for="noOfEmployees">No of Employees</label><br>  		                  	  
		                    	<div class="input-group mb-2">
		                    		<label for="noOfEmployeesMale">Male</label>      	                          	                           
									<input type="text" class="form-control" name="noOfEmployeesMale" id="noOfEmployeesMale">  
									<label for="noOfEmployeesMale">Female</label>      	                          	                           
									<input type="text" class="form-control" name="noOfEmployeesFemale" id="noOfEmployeesFemale">   
		                        </div>	                                           
		                    </div>	
		                    <div class="form-group col-md-2">	
		                    	<label for="sex">Sex</label><br>          
		                    	<div class="input-group mb-2">
		                    		<select name="sex" id="sex" class="form-control">
		                    			<option selected disabled>-----Select Sex-----</option>
		                    			<option value="Male">Male</option>
		                    			<option value="Female">Female</option>
		                    		</select>	                          	                           
		                        </div>	                                           
		                    </div>  
		                    <div class="form-group col-md-2">	
		                    	<label for="rightOverLand">Right Over Land</label><br>    
		                    	<select class="form-control" name="rightOverLand" id="rightOverLand">
		                    		<option selected disabled value="">-----Choose-----</option>
		                    		<option value="LEASE">LEASE</option>
		                    		<option value="OWNER">OWNER</option>
		                    	</select>      	                                                               
		                    </div>
		                    <div class="form-group col-md-2">	
		                    	<label for="landRate">Land Rate</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="landRate" id="landRate">   
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-md-2">
	                    		<label for="pisc">Business Area</label><br>          
		                    	<div class="input-group mb-2">			                 
			                    	<input type="text" class="form-control" name="businessArea" id="businessArea">		                    			                    	                          	                           								   
		                        </div>	 
	                    	</div>	                    		  			                          			                        		                         	       
	                    </div>
	                    <div class="form-row">	              	                                   	                    
	                    	 <div class="form-group col-md-6">	
		                    	<label for="address">Address</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="address" id="address">   
		                        </div>	                                           
		                    </div>	  			                 	
		                    <div class="form-group col-md-2">	
		                    	<label for="district">District</label><br>          		                    
		                        <select class="form-control" name="district" id="district">
		                        	<option selected disabled>Select District</option>
		                        	<option value="1">1</option>
		                        	<option value="2">2</option>
		                        	<option value="3">3</option>
		                        	<option value="4">4</option>
		                        	<option value="5">5</option>
		                        	<option value="6">6</option>
		                        </select>	                                           
		                    </div>
		                    <div class="form-group col-md-4">	
		                    	<label for="barangay">Barangay</label><br>
		                    	<data> 
			                    	<select class="form-control" name="barangay" id="barangay">
			                    	<c:forEach items="${barangayList}" var="list">         
				                    	<option value="<c:out value="${list.barangay}" />"><c:out value="${list.barangay}" /></option>
			                        </c:forEach>
			                        </select>
		                        </data>                                          
		                    </div>	                 			                        		                         	       
	                    </div>
	                    <div class="form-row">	                    		          			                           	             	                            	  			                                      		                     	       
	                    </div>		  
	                    <div class="form-row">
	                    	<div class="form-group col-md-6">	
		                    	<label for="businessActivityMainTile1">Business Activity Maintile1</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="businessActivityMainTile1" id="businessActivityMainTile1">   
		                        </div>	                                           
		                    </div>  
		                    <div class="form-group col-md-6">	
		                    	<label for="businessActivitySubTile1">Business Activity Subtile1</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="businessActivitySubTile1" id="businessActivitySubTile1">   
		                        </div>	                                           
		                    </div>   	 		                            	  			                                      		                     	       
	                    </div>	                         	  	                    
	                </div>
	                <div class="text-center pl-4 pr-4 pb-4 pt-1">
	                	<button type="submit" id="btnSave" value="Save" class="btn btn-primary btn-block rounded-0 py-2"><span><i class="fas fa-save"></i>Save</span></button>
	                </div>			                
	            </div>
	        </form>
	    </div>
	</div>
</div>
<!-- Scripts -->
<script defer src="${pageContext.request.contextPath}/JS/save.js"></script>	
<script defer src="${pageContext.request.contextPath}/JS/pages/barangayDropdown.js"></script>	
</body>
</html>