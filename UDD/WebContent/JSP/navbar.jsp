<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/fontawesome-free-5.5.0-web/css/all.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/background.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/shadow.css">
<!-- scripts -->
<script defer src="${pageContext.request.contextPath}/JS/jquery-3.2.1/jquery-3.2.1.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/js/bootstrap.bundle.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/js/bootstrap.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/bootbox/bootbox.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/fontawesome-free-5.5.0-web/js/all.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/activeLink.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand"><img src="${pageContext.request.contextPath}/CSS/Images/logo/Q.C._Logo.png" style="width:100px;"></a>
  <a class="navbar-brand text-uppercase font-weight-bold" href=""><h3>Locational Clearance<br>Tracking System</h3></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
  	<div class="btn-group ml-auto">
  		<c:if test="${(sessionScope.administrator != null)||(sessionScope.receiving != null)}">
  			<button type="button" class="btn btn-primary" onclick="location.href='${pageContext.request.contextPath}/JSP/receiving.jsp';">RECEIVING</button>
		</c:if>
		<c:if test="${(sessionScope.administrator != null)||(sessionScope.evaluation != null)}">
			<button type="button" class="btn btn-primary" onclick="location.href='${pageContext.request.contextPath}/JSP/evaluation.jsp';">EVALUATION</button>
		</c:if>
		<c:if test="${(sessionScope.administrator != null)||(sessionScope.printing != null)}">
			<button type="button" class="btn btn-primary" onclick="location.href='${pageContext.request.contextPath}/JSP/lcFormSearch.jsp';">LC FORM</button>
		</c:if>
		<c:if test="${(sessionScope.administrator != null)||(sessionScope.releasing != null)}">
			<button type="button" class="btn btn-primary" onclick="location.href='${pageContext.request.contextPath}/JSP/releasing.jsp';">RELEASING</button>
		</c:if>	
		<c:if test="${(sessionScope.administrator != null)||(sessionScope.printing != null)}">
			<button type="button" class="btn btn-primary" onclick="location.href='${pageContext.request.contextPath}/JSP/reports.jsp';">REPORTS</button>
		</c:if>	
		<button type="button" class="btn btn-primary" onclick="location.href='${pageContext.request.contextPath}/JSP/cameraQr.jsp';">CAMERAQR</button>	
		<button type="button" class="btn btn-primary" onclick="location.href='${pageContext.request.contextPath}/JSP/help.jsp';">HELP</button>
		<div class="btn-group">
			<button type="button" class="btn btn-secondary dropdown-toggle text-uppercase" data-toggle="dropdown">
	    		Hi <i class="fas fa-user-circle"></i><c:out value="${sessionScope.nameOfUser}" />!
	    	</button>
		    <div class="dropdown-menu">
			    <c:if test="${sessionScope.administrator != null}">
			    	<a class="dropdown-item" href="${pageContext.request.contextPath}/JSP/manageAccounts?action=view">Manage User Accounts</a>
			    	<a class="dropdown-item" href="${pageContext.request.contextPath}/JSP/uploadExcel.jsp">Upload Excel</a>
			    </c:if>		 
			    <c:choose>
			    	<c:when test="${sessionScope.receiving != null }">
			    		<a class="dropdown-item">Section : Receiving</a>
			    	</c:when>
			    	<c:when test="${sessionScope.evaluation != null }">
			    		<a class="dropdown-item">Section : Evaluation</a>
			    	</c:when>
			    	<c:when test="${sessionScope.printing != null }">
			    		<a class="dropdown-item">Section : Printing</a>
			    	</c:when>
			    	<c:when test="${sessionScope.releasing != null }">
			    		<a class="dropdown-item">Section : Releasing</a>
			    	</c:when>
			    	<c:when test="${sessionScope.finalEvaluation != null }">
			    		<a class="dropdown-item">Section : Final Evaluation</a>
			    	</c:when>
			    </c:choose>
		    	<a class="dropdown-item" href="${pageContext.request.contextPath}/logout">Logout</a>
		    </div>
		</div>
	</div>    
  </div>
</nav>


</body>
</html>