<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Releasing Searched</title>
</head>
<body>
<jsp:include page="navbar.jsp" />

<div class="container p-5">
	<div class="row">
		<c:if test="${empty releasingRecords}">
			<div class="col-md-3"></div>
			<div class="col-md-6" id="mpNotFound">
				 <div class="card border-danger rounded-0 text-center">
				 	<div class="card-header p-0">
			 		  	<div class="bg-danger text-white text-center py-2">
	                        <h2><i class="fas fa-exclamation-triangle text-warning"></i>Warning!<i class="fas fa-exclamation-triangle text-warning"></i></h2>		                                          
	                    </div>
				 	</div>
				 	<div class="card-body p-3">
	                	<div class="text-center py-2">
	                        <h1 class="text-danger">TRANSACTION NUMBER NOT FOUND!</h1>	                                                          
	                    </div>
	                    <div class="text-center py-2">
	                        Click <a href="${pageContext.request.contextPath}/JSP/releasing.jsp">here</a> to go back	                                                          
	                    </div>
	                </div>
				 </div>
        	</div>  
        	<div class="col-md-3"></div>     	
        </c:if>
  		<div class="col">
	        <c:forEach items="${releasingRecords}" var="items">
	        	<form>			       
		        	<div class="card border-info rounded-0">
		        		<div class="card-header p-0">
		                    <div class="bg-primary text-white text-center py-2">
		                        <h2><i class="fas fa-edit"></i>Releasing Records</h2>	                      	          
		                    </div>                 
			        	</div>
				       	<div class="card-body p-3">       		 
				       		<div class="form-row">
				       			<div class="form-group col-sm-6">	
			                    	<label for="mpNo">MP Number</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="mpNo" id="mpNo" value="<c:out value="${items.mpNumber}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>	
			                    <div class="form-group col-sm-6">	
			                    	<label for="transactionNo">Transaction Number</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="transactionNo" id="transactionNo" value="<c:out value="${items.transactionNumber}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>	
				       		</div>
				       		<div class="form-row">
				       			<div class="form-group col-sm-12">	
			                    	<label for="businessName">Business Name</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="businessName" id="businessName" value="<c:out value="${items.businessName}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>			                   
				       		</div>	 
				       		<div class="form-row">
				       			<div class="form-group col-sm-12">	
			                    	<label for="taxPayerName">Taxpayer Name</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="taxPayerName" id="taxPayerName" value="<c:out value="${items.taxPayerName}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>			                   
				       		</div>	 
				       		<div class="form-row">
				       			<div class="form-group col-sm-6">	
			                    	<label for="amount">Amount</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="amount" id="amount" value="<c:out value="${items.amount}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>	
			                    <div class="form-group col-sm-6">	
			                    	<label for="sealNumber">Seal Number</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="sealNumber" id="sealNumber" value="<c:out value="${items.sealNumber}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>	
				       		</div>	
				       		<div class="form-row">
				       			<div class="form-group col-sm-4">	
			                    	<label for="dateEncoded">Date Encoded</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="dateEncoded" id="dateEncoded" value="<c:out value="${items.dateEncoded}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>				         
			                    <div class="form-group col-sm-7">	
			                    	<label for="receiptNumber">Receipt Number</label><br>          
			                    	<div class="input-group mb-2">	           		  	                           									
										<input type="text" class="form-control" name="receiptNumber" id="receiptNumber" value="<c:out value="${items.receiptNumber}"/>" readonly>   	                       	                        	              
			                        </div>	                                           
			                    </div>				                    
				       		</div>	                 		       		
				       	</div>
			       	</div>
		       	</form>	
			</c:forEach>
		</div>
	</div>
</div>
<!-- Scripts -->
<script defer src="${pageContext.request.contextPath}/JS/bounce.js"></script>
</body>
</html>