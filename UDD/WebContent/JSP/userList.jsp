<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User Lists</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/DataTables-1.10.18/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/searchForMp.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/background.css">
</head>
<body>
<jsp:include page="navbar.jsp" />
<div class="container p-5 text-center">
	<div class="row bg-light">
		<div class="col table-responsive">
			<table class="table table-bordered table-paginate" style="width:100%"">
				<caption>Locational Clearance</caption>
				<thead class="thead-dark">
				    <tr>
				      <th scope="col">#</th>
				      <th scope="col">USERNAME</th>
				      <th scope="col">FULLNAME</th>
				      <th scope="col">USER LEVEL</th>
				      <th scope="col">SECTION</th>
				      <th scope="col">STATUS</th>
				      <th scope="col"></th>
				      <th scope="col"></th>
				    </tr>
			    </thead>
			    <tbody>
					<c:forEach items="${userItems}" var="items" varStatus="count">				
					    <tr class="clickable-row">
					      <th scope="row"><c:out value="${count.count}"/></th>					    
					      <td class="col"><c:out value="${items.username}"/></td>
					      <td class="col"><c:out value="${items.fullname}"/></td>
					      <td class="col"><c:out value="${items.userLevel}"/></td>
					      <td class="col"><c:out value="${items.section}"/></td>
					      <td class="col"><c:out value="${items.status}"/></td>	
					      <td><a class="btn btn-primary" href="${pageContext.request.contextPath}/JSP/manageAccounts?action=edit&id=<c:out value="${items.id}"/>">Edit</a></td>
                    	  <td><a class="btn btn-danger delete_alert" href="${pageContext.request.contextPath}/JSP/manageAccounts?action=delete&id=<c:out value="${items.id}"/>">Delete</a></td>	
					    </tr>
					</c:forEach>
				</tbody>					 
			</table>
		</div>
	</div>
</div>

<!-- scripts -->
<script defer src="${pageContext.request.contextPath}/CSS/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/table/tablePaginate.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/delete.js"></script>
</body>
</html>