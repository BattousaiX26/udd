<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Reports</title>
</head>
<body>
<jsp:include page="navbar.jsp" />
<div class="jumbotron">
   	<h1 class="display-5">Reports Generation</h1>
 	<p class="lead">Generate report based on selected action</p>
</div>
<div class="container p-2">
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="card mb-4 border-info rounded">
				<div class="card-header p-0">
	                   <div class="bg-primary text-white text-center py-2">
	                       <h2><i class="fas fa-file-alt"></i>Reports</h2>	                                          
	                   </div>
	            </div>
	            <div class="card-body text-center">
	            	<form id="reportsForm" class="px-4 py-3" action="${pageContext.request.contextPath}/JSP/reports" method="get">		    	
					    <div class="input-group input-group-md">	
					    	<input type="text" class="form-control" name="inputCriteria" id="inputCriteria" placeholder="Enter first two digit of MP Number (18-) / year (2019)" disabled>
					    </div><br>
					    <div class="input-group input-group-md">							 		    	
					      	<select name="action" id="action" class="form-control" required>
					      		<option value="" disabled selected>------Select Action-----</option>
					      		<option value="generateNewBusiness" >Generate report for new business</option>
					      		<option value="generateOldBusiness" >Generate report for old business</option>
					      		<option value="generateFromMpDigit" >Generate report from MP number digit</option>
					      		<option value="generateByYearAdded" >Generate report based on year added</option>
					      	</select>		      	
					    </div><br>	
						<button id="btnGenerate" type="submit" class="btn btn-primary btn-md btn-block"><i class="far fa-file-alt"></i> Generate</button>				
		  	    	</form>           
	            </div>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
</div>
<!-- Scripts -->
<script defer src="${pageContext.request.contextPath}/JS/report.js"></script>
</body>
</html>