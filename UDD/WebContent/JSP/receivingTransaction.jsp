<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Receiving Transaction</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/background.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/shadow.css">
<style>
@media print {
     body {
     	font: 8pt Monaco, monospace;
     }
	 #button-print {
        display:none
    }
    body{
        background-image: none;
    }
    #back{
    	display:none
    }
    #top, #bottom {
	    position: fixed;
	    left: 0;
	    right: 0;
	    height: 50%;
	}
	#top {
	    top: 0;
	    border-bottom: 3px solid; margin-bottom:10px;
	}	
	#bottom {
	    bottom: 0;
	}
}
.arial-font{
	font-family: Arial, Helvetica, sans-serif;
}
.times-font{	
    font-family: "Times New Roman", Times, serif;
}
</style>
</head>
<body>
<c:if test="${empty generatedItems}">
<div class="container p-5">
	<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6" id="mpNotFound">
				 <div class="card border-danger rounded-0 text-center">
				 	<div class="card-header p-0">
			 		  	<div class="bg-danger text-white text-center py-2">
	                        <h2><i class="fas fa-exclamation-triangle text-warning"></i>Warning!<i class="fas fa-exclamation-triangle text-warning"></i></h2>		                                          
	                    </div>
				 	</div>
				 	<div class="card-body p-3">
	                	<div class="text-center py-2">
	                        <h1 class="text-danger">MP NUMBER NOT FOUND!</h1>	
	                        <h5>Click <a href="${pageContext.request.contextPath}/JSP/receiving.jsp">here</a> to search again</h5>                                          
	                    </div>
	                </div>
				 </div>
        	</div>  	
	</div>
</div>
</c:if>
<c:if test="${not empty generatedItems}">
<div class="container p-5">
	<div class="row p-3 bg-white" id="top">
		<div class="col-md-12 text-right" id="clientHeader">
	    	<span class="font-weight-bold arial-font">CLIENT'S COPY</span>
	    </div>     
		<div class="col-md-12" id="areaToPrint">
			<c:forEach items="${generatedItems}" var="items">
				<div class="text-center">	
					<img src="${pageContext.request.contextPath}/CSS/Images/logo/Q.C._Logo.png" style="width:150px;"></img>
				</div>
				<table style="width:100%;">	
					<tbody>
						<tr>
						 	<td class="border border-dark w-25"><span class="h4 times-font">Transaction<br> Number:</span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h1 arial-font font-weight-bold"><c:out value="${items.transactionNumber}"/></span><td>						
				 		</tr>
				 		<tr>
				 			<td class="border border-dark w-25"><span class="h4 times-font">MP <br>Number:</span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h4 arial-font font-weight-bold"><c:out value="${items.mpNo}"/></span></td>
				 		</tr>	
				 		<tr>
				 			<td class="border border-dark w-25"><span class="h4 times-font">Business <br>Name:</span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h4 arial-font font-weight-bold"><c:out value="${items.businessName}"/></span></td>
				 		</tr>
				 		<tr>
				 			<td class="border border-dark w-25"><span class="h4 times-font">Date <br>Applied: </span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h4 arial-font font-weight-bold"><c:out value="${items.dateApplied}"/></span></td>
				 		</tr>
					</tbody>
				</table>							 	
				<br>
				<table style="width:100%;">	
					<tbody>
						<tr>
							<td class="text-center"><span class="font-weight-bold h4">Reminders</span></td>
						</tr>
						<tr>
							<td><span class="h5">1. Only REGISTERED/RIGHTFUL OWNER or his DESIGNATED REPRESENTATIVE with notarized and legally valid authorization will be entertained.</span></td>
						</tr>
						<tr>
							<td><span class="h5">2. Present this claim stub together with your I.D. during transaction and I.D. of owner.</span></td>
						</tr>
						<tr>
							<td><span class="h5">3. FOLLOW-UP AND FIXING BY EMPLOYEES OF THE QUEZON CITY GOVERNMENT IS PROHIBITED.</span></td>
						</tr>
						<tr>
							<td><span class="h5">4. Please follow up 3-5 working days. Tel No. 988-4242 LOC. 1005.</span></td>
						</tr>
					</tbody>
				</table>
			</c:forEach>
		</div>
		<div class="col-md-12 text-right p-3" id="nameOfReceiver">
			<span class="h5">Received by: <c:out value="${sessionScope.nameOfUser}" /></span>	
		</div>
	</div>
	<div class="row p-3 bg-white" id="bottom">
		<div class="col-md-12 text-right" id="clientHeader">
	    	<span class="font-weight-bold arial-font">OFFICE'S COPY</span>
	    </div>     
		<div class="col-md-12" id="areaToPrint">
			<c:forEach items="${generatedItems}" var="items">	
				<div class="text-center">	
					<img src="${pageContext.request.contextPath}/CSS/Images/logo/Q.C._Logo.png" style="width:150px;"></img>
				</div>
				<table style="width:100%;">	
					<tbody>
						<tr>
						 	<td class="border border-dark w-25"><span class="h5 times-font">Transaction<br> Number:</span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h1 arial-font font-weight-bold"><c:out value="${items.transactionNumber}"/></span><td>						
				 		</tr>
				 		<tr>
				 			<td class="border border-dark w-25"><span class="h5 times-font">MP <br>Number:</span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h5 arial-font font-weight-bold"><c:out value="${items.mpNo}"/></span></td>
				 		</tr>	
				 		<tr>
				 			<td class="border border-dark w-25"><span class="h5 times-font">Business <br>Name:</span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h5 arial-font font-weight-bold"><c:out value="${items.businessName}"/></span></td>
				 		</tr>
				 		<tr>
				 			<td class="border border-dark w-25"><span class="h5 times-font">Date <br>Applied: </span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h5 arial-font font-weight-bold"><c:out value="${items.dateApplied}"/></span></td>
				 		</tr>
				 		<tr>
				 			<td class="border border-dark w-25"><span class="h5 times-font">CPDO <br>Number: </span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h5 arial-font font-weight-bold"><c:out value="${items.cpdoNo}"/></span></td>
				 		</tr>
				 		<tr>
				 			<td class="border border-dark w-25"><span class="h5 times-font">Zoning <br>Class: </span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h5 arial-font font-weight-bold"><c:out value="${items.zoningClass}"/></span></td>
				 		</tr>
				 		<tr>
				 			<td class="border border-dark w-25"><span class="h5 times-font">Verifier:</span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h5 arial-font font-weight-bold"><c:out value="${items.verifier}"/></span></td>
				 		</tr>
				 		<tr>
				 			<td class="border border-dark w-25"><span class="h5 times-font">Status:</span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h5 arial-font font-weight-bold"><c:out value="${items.status}"/></span></td>
				 		</tr>
				 		<tr>
				 			<td class="border border-dark w-25"><span class="h5 times-font">Remarks:</span></td>
						 	<td class="border border-dark w-75 text-center"><span class="h5 arial-font font-weight-bold"><c:out value="${items.remarks}"/></span></td>
				 		</tr>
				 		
					</tbody>
				</table>
				<br>					 	
			</c:forEach>
		</div>
		<div class="col-md-12 text-right p-3" id="nameOfReceiver">
			<span class="h5">Received by: <c:out value="${sessionScope.nameOfUser}" /></span>	
		</div>
		<div class="col-md-12 text-center">
			<span id="back"> Click <a href="${pageContext.request.contextPath}/JSP/receiving.jsp">here</a> to go back</span>
			<button type="button" class="btn btn-primary btn-block button-print" id="button-print"><i class="fas fa-print"></i>Print</button>
		</div>
	</div>
</div>
</c:if>
<!-- Scripts -->
<script defer src="${pageContext.request.contextPath}/JS/jquery-3.2.1/jquery-3.2.1.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/js/bootstrap.bundle.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/js/bootstrap.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/bounce.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/printForm.js"></script>
</body>
</html>