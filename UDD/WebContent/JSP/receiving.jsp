<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Receiving</title>
</head>
<body>
<jsp:include page="navbar.jsp" />
<div class="jumbotron">
    <h1 class="display-5">Receiving</h1>
    <p class="lead">Encode new application or Search for an existing record</p>
</div>

<div class="container-fluid p-2">
	<div class="row">
		<div class="col-md-6">
	        <div class="card mb-4 border-info rounded">
	        	<div class="card-header p-0">
                    <div class="bg-primary text-white text-center py-2">
                        <h2><i class="fas fa-search"></i> Locational Entry</h2>	
                        <p>Type MP-Number and choose desired action</p>                        
                    </div>
	            </div>
	            <div class="card-body text-center">
	                <form action="${pageContext.request.contextPath}/JSP/manageMp" method="get">
						<div class="form-group">
							<div class="input-group mb-2">					 
			                   <div class="input-group-prepend">
			                       <div class="input-group-text"><i class="fas fa-search text-primary"></i></div>
			                   </div>	                  
				               <input type="text" class="form-control" id="mpNumber" name="mpNumber" placeholder="Search MP Number" required>		           		               	                       		
				            </div>  
				            <select name="action" class="form-control" style="width:auto" required>
				            	<option selected disabled value="">Choose Action</option>
			               		<option value="search">Retrieve records</option>
			               		<option value="reprint">Reprint Transaction Number</option>
				            </select>
				            <div class="p-2">
				            	<input type="submit" class="btn btn-primary btn-block" value="Search">
				            </div>
						</div>
					</form>                
	            </div>
	        </div>
	    </div>
	    <div class="col-md-6"> 
	        <div class="card mb-4 border-info rounded">
	        	<div class="card-header p-0">
                    <div class="bg-primary text-white text-center py-2">
                        <h2><i class="fas fa-keyboard"></i> Encoding</h2>	
                        <p>Click the button below to encode new application</p>                        
                    </div>
	            </div>
	            <div class="card-body text-center">	                             
	                <div class="p-2">
	                	<a href="${pageContext.request.contextPath}/JSP/encoding.jsp" class="card-link btn btn-primary">Click here</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>


<!-- Scripts -->
</body>
</html>