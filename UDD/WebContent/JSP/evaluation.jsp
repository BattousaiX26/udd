<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Evaluation</title>
</head>
<body>
<c:if test="${(sessionScope.evaluation == null)&&(sessionScope.printing == null)&&(sessionScope.finalEvaluation == null)}">
   <c:redirect url="/JSP/receiving.jsp"></c:redirect>
</c:if>
<jsp:include page="navbar.jsp" />
<div class="jumbotron">
    <h1 class="display-5">Evaluation of Business Application</h1>
    <p class="lead">Search Transaction Number to evaluate Business Application</p>
</div>
<div class="container p-2">
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="card mb-4 border-info rounded">
				<div class="card-header p-0">
	                   <div class="bg-primary text-white text-center py-2">
	                       <h2><i class="fas fa-search"></i>Evaluation</h2>	                                          
	                   </div>
	            </div>
	            <div class="card-body text-center">
	                <form action="${pageContext.request.contextPath}/JSP/evaluate" method="get">
						<div class="form-group">
							<div class="input-group mb-2">					 
			                   <div class="input-group-prepend">
			                       <div class="input-group-text"><i class="fas fa-search text-primary"></i></div>
			                   </div>	                  
				               <input type="text" class="form-control" id="transactionNo" name="transactionNo" placeholder="Search Transaction Number" required>		           		               	                       		
				            </div>			            	  			            
				            <div class="p-2">
				            	<input type="submit" class="btn btn-primary btn-block" value="Search">
				            	<input name="action" id="action" value="searchTransaction" hidden>
				            </div>
						</div>
					</form>                
	            </div>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
</div>
</body>
</html>