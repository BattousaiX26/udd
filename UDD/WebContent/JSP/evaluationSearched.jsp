<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Evaluation Searched</title>
</head>
<body>
<jsp:include page="navbar.jsp" />
<div class="container-fluid p-5">
	<div class="row">
		<c:if test="${empty itemsTobeEvaluated}">
			<div class="col-md-3"></div>
			<div class="col-md-6" id="mpNotFound">
				 <div class="card border-danger rounded-0 text-center">
				 	<div class="card-header p-0">
			 		  	<div class="bg-danger text-white text-center py-2">
	                        <h2><i class="fas fa-exclamation-triangle text-warning"></i>Warning!<i class="fas fa-exclamation-triangle text-warning"></i></h2>		                                          
	                    </div>
				 	</div>
				 	<div class="card-body p-3">
	                	<div class="text-center py-2">
	                        <h1 class="text-danger">TRANSACTION NUMBER NOT FOUND!</h1>	
	                        <h5>Click <a href="${pageContext.request.contextPath}/JSP/evaluation.jsp">here</a> to search again</h5>                                          
	                    </div>
	                </div>
				 </div>
        	</div>  
        	<div class="col-md-3"></div>     	
        </c:if>
		<div class="col">
	        <!--Form with header-->
	        <c:forEach items="${itemsTobeEvaluated}" var="items" varStatus="count">	
	        <form class="validatedForm" id="form">
	            <div class="card border-info rounded-0">
	         		<div class="card-header p-0">
	                    <div class="bg-primary text-white text-center py-2">
	                        <h2><i class="fas fa-edit"></i>Receiving</h2>	
	                        <p class="m-0 font-italic">Edit fields if necessary</p> 	  
	                        <a href="#bottom" id="hulk" class="btn btn-danger"><i class="fas fa-arrow-circle-down"></i>Go to the bottom of the page</a>    
	                    </div>                 
	                </div>
	                <div class="card-body p-3">
	                <div class="form-row">
	                		<input type="text" class="form-control" name="id" value="<c:out value="${items.id}"/>" hidden><!-- id field -->
	                    	<div class="form-group col-sm-6">	
		                    	<label for="businessName">Business Name / Trade Name</label><br>          
		                    	<div class="input-group mb-2">	   
		                    		<c:choose>          
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">             	                           									
											<input type="text" class="form-control" name="businessName" id="businessName" value="<c:out value="${items.businessName}"/>">   
			                        	</c:when>
			                        	<c:when test="${sessionScope.evaluation==null}">             	                           									
											<input type="text" class="form-control" name="businessName" id="businessName" value="<c:out value="${items.businessName}"/>" readonly>   
			                        	</c:when>
		                        	</c:choose>
		                        </div>	                                           
		                    </div>	
		                    <div class="form-group col-sm-6">	
		                    	<label for="taxPayerName">Taxpayer Name</label><br>          
		                    	<div class="input-group mb-2">	   
		                    	    <c:choose>          
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">                 	                           
											<input type="text" class="form-control" name="taxPayerName" id="taxPayerName" value="<c:out value="${items.taxPayerName}"/>" >   
		                       			</c:when>
		                       			<c:when test="${sessionScope.evaluation==null}">
		                       				<input type="text" class="form-control" name="taxPayerName" id="taxPayerName" value="<c:out value="${items.taxPayerName}"/>" readonly> 
		                       			</c:when>
		                       		</c:choose>
		                        </div>	                                           
		                    </div>	            			                  	                                 		                         	       
	                    </div>
	                    <div class="form-row">		                    
		                    <div class="form-group col-sm-2">	
		                    	<label for="noOfEmployees">No of Employees</label><br>  		                  	  
		                    	<div class="input-group mb-2">
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
				                    		<label for="noOfEmployeesMale">Male</label>      	                          	                           
											<input type="text" class="form-control" name="noOfEmployeesMale" id="noOfEmployeesMale" value="<c:out value="${items.noOfEmployeesMale}"/>">  
											<label for="noOfEmployeesFemale">Female</label>      	                          	                           
											<input type="text" class="form-control" name="noOfEmployeesFemale" id="noOfEmployeesFemale" value="<c:out value="${items.noOfEmployeesFemale}"/>">   
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<label for="noOfEmployeesMale">Male</label>      	                          	                           
											<input type="text" class="form-control" name="noOfEmployeesMale" id="noOfEmployeesMale" value="<c:out value="${items.noOfEmployeesMale}"/>" readonly>  
											<label for="noOfEmployeesFemale">Female</label>      	                          	                           
											<input type="text" class="form-control" name="noOfEmployeesFemale" id="noOfEmployeesFemale" value="<c:out value="${items.noOfEmployeesFemale}"/>" readonly>   
			                    		</c:when>
			                    	</c:choose>	                    	
		                        </div>	                                           
		                    </div>	
		                    <div class="form-group col-sm-2">	
		                    	<label for="sex">Sex</label><br>          
		                    	<div class="input-group mb-2">
		                    		<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">			                    			
				                    		<select name="sex" id="sex" class="form-control">
				                    			<option selected value="<c:out value="${items.sex}"/>"><c:out value="${items.sex}"/></option>
				                    			<option disabled>-----Select Sex-----</option>
				                    			<option value="Male">Male</option>
				                    			<option value="Female">Female</option>
				                    		</select>	         
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<select name="sex" id="sex" class="form-control">
				                    			<option disabled selected value="<c:out value="${items.sex}"/>"><c:out value="${items.sex}"/></option>
				                    			<option disabled>-----Select Sex-----</option>
				                    			<option disabled value="Male">Male</option>
				                    			<option disabled value="Female">Female</option>
				                    		</select>	
			                    		</c:when>
		                    		</c:choose>		                    		                 	                           
		                        </div>	                                           
		                    </div>  
		                    <div class="form-group col-sm-2">	
		                    	<label for="rightOverLand">Right Over Land</label><br>          
		                    	<div class="input-group mb-2">	      
		                    		<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    			<select class="form-control" name="rightOverLand" id="rightOverLand">
												<option selected value="<c:out value="${items.rightOverLand}"/>"><c:out value="${items.rightOverLand}"/></option>
					                    		<option disabled value="">-----Choose-----</option>
					                    		<option value="LEASE">LEASE</option>
					                    		<option value="OWNER">OWNER</option>
		                    				</select>      
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<select class="form-control" name="rightOverLand" id="rightOverLand">
												<option disabled selected value="<c:out value="${items.rightOverLand}"/>"><c:out value="${items.rightOverLand}"/></option>
					                    		<option disabled value="">-----Choose-----</option>
					                    		<option disabled value="LEASE">LEASE</option>
					                    		<option disabled value="OWNER">OWNER</option>
		                    				</select>   
			                    		</c:when>
		                    		</c:choose>		                    		                    	                           								
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-sm-3">	
		                    	<label for="landRate">Land Rate</label><br>          
		                    	<div class="input-group mb-2">	                          	                           						 
									<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    			<input type="text" class="form-control" name="landRate" id="landRate" value="<c:out value="${items.landRate}"/>" >  
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<input type="text" class="form-control" name="landRate" id="landRate" value="<c:out value="${items.landRate}"/>" readonly>  
			                    		</c:when>
		                    		</c:choose>							
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-sm-3">	
		                    	<label for="capitalization">Capitalization</label><br>          
		                    	<div class="input-group mb-2">
		                    		<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    			<input type="text" class="form-control" name="capitalization" id="capitalization" value="<c:out value="${items.capitalization}"/>" >   
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<input type="text" class="form-control" name="capitalization" id="capitalization" value="<c:out value="${items.capitalization}"/>" readonly>   
			                    		</c:when>
		                    		</c:choose>	                    			                          	                           									
		                        </div>	                                           
		                    </div>			  			                          			                        		                         	       
	                    </div>
	                    <div class="form-row">	                		                  	           
		                    <div class="form-group col-sm-6">	
		                    	<label for="address">Address</label><br>          
		                    	<div class="input-group mb-2">	
		                    		<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    			<input type="text" class="form-control" name="address" id="address" value="<c:out value="${items.address}"/>" >   
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<input type="text" class="form-control" name="address" id="address" value="<c:out value="${items.address}"/>" readonly>   
			                    		</c:when>
		                    		</c:choose>		                    		                          	                           									
		                        </div>	                                           
		                    </div> 	 
		                    <div class="form-group col-sm-2">	
		                    	<label for="district">District</label><br>          
		                    	<div class="input-group mb-2">	  
		                    		<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
				                    		<select class="form-control" name="district" id="district">
												<option selected value="<c:out value="${items.district}"/>"><c:out value="${items.district}"/></option>
												<option disabled>Select District</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
											</select>  
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<select class="form-control" name="district" id="district">
												<option disabled selected value="<c:out value="${items.district}"/>"><c:out value="${items.district}"/></option>
												<option disabled>Select District</option>
												<option disabled value="1">1</option>
												<option disabled value="2">2</option>
												<option disabled value="3">3</option>
												<option disabled value="4">4</option>
												<option disabled value="5">5</option>
												<option disabled value="6">6</option>
											</select>  
			                    		</c:when>
		                    		</c:choose>                    		                        	                           																	
		                        </div>	                                           
		                    </div>	
		                    <div class="form-group col-sm-4">	
		                    	<label for="barangay">Barangay</label><br>          
		                    	<div class="input-group mb-2">	
			                    	<c:choose>
			                    		<c:when test="${sessionScope.evaluation!=null}">
				                    		<select class="form-control" name="barangay" id="barangay">
												<option selected value="<c:out value="${items.barangay}"/>"><c:out value="${items.barangay}"/></option>
											</select> 
											<c:forEach items="${barangayList}" var="list">         
							                    <option value="<c:out value="${list.barangay}" />"><c:out value="${list.barangay}" /></option>
						                    </c:forEach> 
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<select class="form-control" name="barangay" id="barangay">
												<option disabled selected value="<c:out value="${items.barangay}"/>"><c:out value="${items.barangay}"/></option>
											</select> 
			                    		</c:when>
			                    	</c:choose>                    	                          	                           							
									
		                        </div>	                                           
		                    </div>                  			                        		                         	       
	                    </div>
	                    <div class="form-row">  		                    		                		                           	             	                            	  			                                      		                     	       
	                    </div>		  
	                    <div class="form-row">
	                    	<div class="form-group col-sm-6">	
		                    	<label for="businessActivityMainTile1">Business Activity Maintile1</label><br>          
		                    	<div class="input-group mb-2">	
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    			<input type="text" class="form-control" name="businessActivityMainTile1" id="businessActivityMainTile1" value="<c:out value="${items.businessActivityMainTile1}"/>" >   
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<input type="text" class="form-control" name="businessActivityMainTile1" id="businessActivityMainTile1" value="<c:out value="${items.businessActivityMainTile1}"/>" readonly>   
			                    		</c:when>
			                    	</c:choose>		                 	                          	                           								
		                        </div>	                                           
		                    </div>  
		                    <div class="form-group col-sm-6">	
		                    	<label for="businessActivitySubTile1">Business Activity Subtile1</label><br>          
		                    	<div class="input-group mb-2">	    
		                    		<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    			<input type="text" class="form-control" name="businessActivitySubTile1" id="businessActivitySubTile1" value="<c:out value="${items.businessActivitySubTile1}"/>" >   
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<input type="text" class="form-control" name="businessActivitySubTile1" id="businessActivitySubTile1" value="<c:out value="${items.businessActivitySubTile1}"/>" readonly>   
			                    		</c:when>
		                    		</c:choose>	                    		                      	                           									
		                        </div>	                                           
		                    </div>   	 		                            	  			                                      		                     	       
	                    </div>		                   	                    	                	            	                    
	                </div>
	                <div class="card-header p-0">
	                    <div class="bg-primary text-white text-center py-2">
	                        <h2><i class="fas fa-edit"></i>Evaluation</h2>
	                        <p class="m-0 font-italic">Edit fields for evaluation</p>
	                        <a href="#top" id ="backToTopBtn" class="btn btn-danger"><i class="fas fa-arrow-circle-up"></i>Go to the top of the page</a><br>   
	                    </div>
	                </div>
	                <div class="card-body p-3">
	                    <!--Body-->	   
	                    <div class="form-row">
	                    	<div class="form-group col-sm-2">	
		                    	<label for="mpNo">Transaction Number</label><br>       
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="transactionNo" id="transactionNo" value="<c:out value="${items.transactionNumber}"/>" readonly>   
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-sm-2">	
		                    	<label for="evaluation">Evaluation</label><br>          
		                    	<div class="input-group mb-2">	
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
				                    		<select class="form-control inputField" id="evaluation" name="evaluation">
					                    		<option selected value="<c:out value="${items.evaluation}"/>"><c:out value="${items.evaluation}"/></option>
				                            	<option disabled>-----Choose Evaluation-----</option>
				                            	<option value="PERMITTED">PERMITTED</option>	
				                            	<option value="NOT PERMITTED">NOT PERMITTED</option>	
				                            	<option value="FOR COMPLIANCE">FOR COMPLIANCE</option>
				                            	<option value="FOR INSPECTION">FOR INSPECTION</option>
		                            		</select>	   
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<select class="form-control inputField" id="evaluation" name="evaluation">
					                    		<option disabled selected value="<c:out value="${items.evaluation}"/>"><c:out value="${items.evaluation}"/></option>
				                            	<option disabled>-----Choose Evaluation-----</option>
				                            	<option disabled value="PERMITTED">PERMITTED</option>	
				                            	<option disabled value="NOT PERMITTED">NOT PERMITTED</option>	
				                            	<option disabled value="FOR COMPLIANCE">FOR COMPLIANCE</option>
				                            	<option disabled value="FOR INSPECTION">FOR INSPECTION</option>
		                            		</select>	   
			                    		</c:when>
			                    	</c:choose>		                    	                          	                           								
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-sm-2">	
		                    	<label for="mpNo">MP No.</label><br>       
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="mpNo" id="mpNo" value="<c:out value="${items.mpNo}"/>" readonly>   
		                        </div>	                                           
		                    </div>	  
		                    <div class="form-group col-sm-2">	
		                    	<label for="cpdoNo">CPDO No.</label><br>  
		                    	<div class="input-group mb-2">	      
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    			<input type="text" class="form-control" name="cpdoNo" id="cpdoNo" value="<c:out value="${items.cpdoNo}"/>">
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<input type="text" class="form-control" name="cpdoNo" id="cpdoNo" value="<c:out value="${items.cpdoNo}"/>" readonly>
			                    		</c:when>
			                    	</c:choose>	                    	                    	                           									   
		                        </div>	                                           
		                    </div>		
		                    <div class="form-group col-sm-2">	
		                    	<label for="dateApplied">Date Applied(year/month/day)</label><br>        
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="dateApplied" id="dateApplied" value="<c:out value="${items.dateApplied}"/>" readonly>   
		                        </div>	                                           
		                    </div>		
		                    <div class="form-group col-sm-2">	
		                    	<label for="dateValid">Date Valid</label><br>  
		                    	<c:choose>
		                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    		<select class="form-control inputField" id="dateValid" name="dateValid" style="width:100%;">
				                    		<option selected value="<c:out value="${items.dateValid}"/>"><c:out value="${items.dateValid}"/></option>
			                            	<option disabled>-----Choose validity-----</option>
			                            	<option value="1">1</option>
			                            	<option value="3">3</option>
		                            	</select>	      
		                    		</c:when>
		                    		<c:when test="${sessionScope.evaluation==null}">
		                    			<select class="form-control inputField" id="dateValid" name="dateValid" style="width:100%;">
				                    		<option disabled selected value="<c:out value="${items.dateValid}"/>"><c:out value="${items.dateValid}"/></option>
			                            	<option disabled>-----Choose validity-----</option>
			                            	<option disabled value="1">1</option>
			                            	<option disabled value="3">3</option>
		                            	</select>	      
		                    		</c:when>
		                    	</c:choose>		                    	      		                    	                                     
		                    </div>	    	     
	                    </div>                      	 
	                    <div class="form-row">                    	                     		
	                    	<div class="form-group col-sm-3">	
		                    	<label for="evaluationOfFacts">Evaluation of Facts</label><br>   
		                    	<c:choose>
		                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    		<select class="form-control inputField" id="evaluationOfFacts" name="evaluationOfFacts" style="width:100%;">
				                    		<option selected value="<c:out value="${items.evaluationOfFacts}"/>"><c:out value="${items.evaluationOfFacts}"/></option>
			                            	<option disabled>-----Choose Evaluation of Facts-----</option>
			                            	<option value="BUSINESS ACTIVITY IS PERMISSIBLE IN THE ZONE">BUSINESS ACTIVITY IS PERMISSIBLE IN THE ZONE</option>
			                            	<option value="BUSINESS ACTIVITY IS CONDITIONAL USE IN THE ZONE">BUSINESS ACTIVITY IS CONDITIONAL USE IN THE ZONE</option>
			                            	<option value="BUSINESS ACTIVITY IS NOT PERMISSIBLE IN THE ZONE">BUSINESS ACTIVITY IS NOT PERMISSIBLE IN THE ZONE</option>
		                            	</select>	 
		                    		</c:when>
		                    		<c:when test="${sessionScope.evaluation==null}">
		                    			<select class="form-control inputField" id="evaluationOfFacts" name="evaluationOfFacts" style="width:100%;">
				                    		<option disabled selected value="<c:out value="${items.evaluationOfFacts}"/>"><c:out value="${items.evaluationOfFacts}"/></option>
			                            	<option disabled>-----Choose Evaluation of Facts-----</option>
			                            	<option disabled value="BUSINESS ACTIVITY IS PERMISSIBLE IN THE ZONE">BUSINESS ACTIVITY IS PERMISSIBLE IN THE ZONE</option>
			                            	<option disabled value="BUSINESS ACTIVITY IS CONDITIONAL USE IN THE ZONE">BUSINESS ACTIVITY IS CONDITIONAL USE IN THE ZONE</option>
			                            	<option value="BUSINESS ACTIVITY IS NOT PERMISSIBLE IN THE ZONE">BUSINESS ACTIVITY IS NOT PERMISSIBLE IN THE ZONE</option>
		                            	</select>	 
		                    		</c:when>
		                    	</c:choose>		                    	    		                    			                                                                          
		                    </div>	
		                    <div class="form-group col-sm-2">	
		                    	<label for="dateIssued">Date Issued(year/month/day)</label><br>        
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="dateIssued" id="dateIssued" value="<c:out value="${items.dateIssued}"/>" readonly>   
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-sm-2">	
		                    	<label for="validUntil">Valid Until(year/month/day)</label><br>        
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="validUntil" id="validUntil" value="<c:out value="${items.validUntil}"/>" readonly>   
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-sm-3">	
		                    	<label for="tctNo">TCT No:</label><br>        
		                    	<div class="input-group mb-2">	
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    			<input type="text" class="form-control" name="tctNo" id="tctNo" value="<c:out value="${items.tctNo}"/>">  
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<input type="text" class="form-control" name="tctNo" id="tctNo" value="<c:out value="${items.tctNo}"/>" readonly>  
			                    		</c:when>
			                    	</c:choose>	                    	                          	                           								 
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-sm-2">	
		                    	<label for="status">Status</label><br>   
		                    	<c:choose>
		                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
		                    			<select class="form-control inputField" id="status" name="status">
				                    		<option selected value="<c:out value="${items.status}"/>"><c:out value="${items.status}"/></option>
			                            	<option disabled>-----Choose Status-----</option>
			                            	<option value="APPROVED">APPROVED</option>
			                            	<option value="DISAPPROVED">DISAPPROVED</option>
			                            	<option value="FOR COMPLIANCE">FOR COMPLIANCE</option>
	                            		</select>
		                    		</c:when>
		                    		<c:when test="${sessionScope.evaluation==null}">
		                    			<select class="form-control inputField" id="status" name="status">
				                    		<option disabled selected value="<c:out value="${items.status}"/>"><c:out value="${items.status}"/></option>
			                            	<option disabled>-----Choose Status-----</option>
			                            	<option disabled value="APPROVED">APPROVED</option>
			                            	<option disabled value="DISAPPROVED">DISAPPROVED</option>
			                            	<option disabled value="FOR COMPLIANCE">FOR COMPLIANCE</option>
	                            		</select>
		                    		</c:when>
		                    	</c:choose>		                    	      		                    	                                          
		                    </div>		                	 				     
	                    </div>
	                    <div class="form-row">	                  		                 
		                    <div class="form-group col-sm-3">	
		                    	<label for="verifier">Verifier</label><br>          
		                    	<div class="input-group mb-2">			                	                     	                           
									<select class="form-control" name="verifier" id="verifier"> 
										<c:if test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">     											
											<option selected value="<c:out value="${sessionScope.nameOfUser}"/>"><c:out value="${sessionScope.nameOfUser}"/></option>
										</c:if>
										<option value="<c:out value="${items.verifier}"/>"><c:out value="${items.verifier}"/></option>
									</select>  		                       	
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-sm-2">	
		                    	<label for="lotArea">Lot Area</label><br>          
		                    	<div class="input-group mb-2">	 
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    			<input type="text" class="form-control" name="lotArea" id="lotArea" value="<c:out value="${items.lotArea}"/>">  
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<input type="text" class="form-control" name="lotArea" id="lotArea" value="<c:out value="${items.lotArea}"/>" readonly>  
			                    		</c:when>
			                    	</c:choose>	                    	                         	                           									 
		                        </div>	                                           
		                    </div>	    
		                    <div class="form-group col-sm-2">
	                    		<label for="pisc">Business Area</label><br>          
		                    	<div class="input-group mb-2">	
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    			<input type="text" class="form-control" name="businessArea" id="businessArea" value="<c:out value="${items.businessArea}"/>">
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<input type="text" class="form-control" name="businessArea" id="businessArea" value="<c:out value="${items.businessArea}"/>" readonly>
			                    		</c:when>
			                    	</c:choose>		                    	                          	                           								   
		                        </div>	 
	                    	</div>
	                    	<div class="form-group col-sm-2">	
		                    	<label for="pisc">PISC</label><br>          
		                    	<div class="input-group mb-2">	
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    			<input type="text" class="form-control" name="pisc" id="pisc" value="<c:out value="${items.pisc}"/>">   
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<input type="text" class="form-control" name="pisc" id="pisc" value="<c:out value="${items.pisc}"/>" readonly>   
			                    		</c:when>
			                    	</c:choose>		                    	                          	                           									
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-sm-3">	
		                    	<label for="zoningClass">Zoning Class</label><br>         
		                    	<c:choose>
		                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    		<select class="form-control inputField" id="zoningClass" name="zoningClass">
				                    		<option selected value="<c:out value="${items.zoningClass}"/>"><c:out value="${items.zoningClass}"/></option>
			                            	<option disabled>-----Choose Zoning Class-----</option>
			                            	<option value="LOW DENSITY RESIDENTIAL ZONE (R-1)">LOW DENSITY RESIDENTIAL ZONE (R-1)</option>	
			                            	<option value="LOW DENSITY RESIDENTIAL SUB-ZONE (R-1-A)">LOW DENSITY RESIDENTIAL SUB-ZONE (R-1-A)</option>
			                            	<option value="MEDIUM DENSITY RESIDENTIAL ZONE (R-2)">MEDIUM DENSITY RESIDENTIAL ZONE (R-2)</option>                            	
			                            	<option value="MEDIUM DENSITY RESIDENTIAL SUB-ZONE (R-2-A)">MEDIUM DENSITY RESIDENTIAL SUB-ZONE (R-2-A)</option>
			                            	<option value="HIGH DENSITY RESIDENTIAL ZONE (R-3)">HIGH DENSITY RESIDENTIAL ZONE (R-3)</option> 
			                            	<option value="MINOR COMMERCIAL ZONE (C-1)">MINOR COMMERCIAL ZONE (C-1)</option> 
			                            	<option value="MAJOR COMMERCIAL ZONE (C-2)">MAJOR COMMERCIAL ZONE (C-2)</option> 
			                            	<option value="METROPOLITAN COMMERCIAL ZONE (C-3)">METROPOLITAN COMMERCIAL ZONE (C-3)</option> 
			                            	<option value="LIGHT INTENSITY INDUSTRIAL ZONE (I-1)">LIGHT INTENSITY INDUSTRIAL ZONE (I-1)</option> 
			                            	<option value="MEDIUM INTENSITY INDUSTRIAL ZONE (I-2)">MEDIUM INTENSITY INDUSTRIAL ZONE (I-2)</option> 
			                            	<option value="SPECIAL URBAN DEVELOPMENT ZONE (SUDZ)">SPECIAL URBAN DEVELOPMENT ZONE (SUDZ)</option> 
			                            	<option value="INSTITUTIONAL (INST)">INSTITUTIONAL (INST)</option> 
			                            	<option value="SOCIALIZED HOUSING ZONE (SHZ)">SOCIALIZED HOUSING ZONE (SHZ)</option> 
			                            	<option value="TRANSPORT AND UTILITY ZONE (TRU)">TRANSPORT AND UTILITY ZONE (TRU)</option>
			                            	<option value="PARK AND RECREATIONAL (P&R)">PARK AND RECREATIONAL (P&R)</option>  
			                            	<option value="CEMETERY (CEM)">CEMETERY (CEM)</option>  
		                            	</select>	    	
		                    		</c:when>
		                    		<c:when test="${sessionScope.evaluation==null}">
		                    			<select class="form-control inputField" id="zoningClass" name="zoningClass">
				                    		<option disabled selected value="<c:out value="${items.zoningClass}"/>"><c:out value="${items.zoningClass}"/></option>
			                            	<option disabled>-----Choose Zoning Class-----</option>
			                            	<option disabled value="LOW DENSITY RESIDENTIAL ZONE (R-1)">LOW DENSITY RESIDENTIAL ZONE (R-1)</option>	
			                            	<option disabled value="LOW DENSITY RESIDENTIAL SUB-ZONE (R-1-A)">LOW DENSITY RESIDENTIAL SUB-ZONE (R-1-A)</option>
			                            	<option disabled value="MEDIUM DENSITY RESIDENTIAL ZONE (R-2)">MEDIUM DENSITY RESIDENTIAL ZONE (R-2)</option>                            	
			                            	<option disabled value="MEDIUM DENSITY RESIDENTIAL SUB-ZONE (R-2-A)">MEDIUM DENSITY RESIDENTIAL SUB-ZONE (R-2-A)</option>
			                            	<option disabled value="HIGH DENSITY RESIDENTIAL ZONE (R-3)">HIGH DENSITY RESIDENTIAL ZONE (R-3)</option> 
			                            	<option disabled value="MINOR COMMERCIAL ZONE (C-1)">MINOR COMMERCIAL ZONE (C-1)</option> 
			                            	<option disabled value="MAJOR COMMERCIAL ZONE (C-2)">MAJOR COMMERCIAL ZONE (C-2)</option> 
			                            	<option disabled value="LIGHT INTENSITY INDUSTRIAL ZONE (I-1)">LIGHT INTENSITY INDUSTRIAL ZONE (I-1)</option> 
			                            	<option disabled value="MEDIUM INTENSITY INDUSTRIAL ZONE (I-2)">MEDIUM INTENSITY INDUSTRIAL ZONE (I-2)</option> 
			                            	<option disabled value="SPECIAL URBAN DEVELOPMENT ZONE (SUDZ)">SPECIAL URBAN DEVELOPMENT ZONE (SUDZ)</option> 
			                            	<option disabled value="INSTITUTIONAL (INST)">INSTITUTIONAL (INST)</option> 
			                            	<option disabled value="SOCIALIZED HOUSING ZONE (SHZ)">SOCIALIZED HOUSING ZONE (SHZ)</option> 
			                            	<option disabled value="TRANSPORT AND UTILITY ZONE (TRU)">TRANSPORT AND UTILITY ZONE (TRU)</option>
			                            	<option disabled value="PARK AND RECREATIONAL (P&R)">PARK AND RECREATIONAL (P&R)</option>  
			                            	<option disabled value="CEMETERY (CEM)">CEMETERY (CEM)</option>  
		                            	</select>	    
		                    		</c:when>
		                    	</c:choose>		                    	 		                    	                                           
		                    </div>			              		                  	      	 	 	                      	      	       
	                    </div>	  	                           
	                    <div class="form-row">	                    	
		                    <div class="form-group col-sm-6">	
		                    	<label for="remarks">Remarks</label><br>   
		                    	<div class="input-group mb-2">	
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
			                    			<select class="form-control inputField" id="remarks" name="remarks">
					                    		<option selected value="<c:out value="${items.remarks}"/>"><c:out value="${items.remarks}"/></option>	                    		
				                            	<option disabled>----------Choose Remarks----------</option>
				                            	<option value="To submit Barangay Council Resolution with in 30 days from the date of issuance, non compliance revokes this clearance.">To submit Barangay Council Resolution with in 30 days from the date of issuance, non compliance revokes this clearance.</option>	
				                            	<option value="With Neighbors Consent allowing business operation.">With Neighbors Consent allowing business operation.</option>
				                            	<option value="With HOA Clearance allowing business operation.">With HOA Clearance allowing business operation.</option>
				                            	<option value="With Barangay Council Resolution No. 525, S-2018.">With Barangay Council Resolution No. 525, S-2018.</option>
				                            	<option value="No storage, selling and display of products at business site.">No storage, selling and display of products at business site.</option>
				                            	<option value="Submitted certification from Social Finance Housing Corp.">Submitted certification from Social Finance Housing Corp.</option>
				                            	<option value="With LC for Building (LC# C-14-17072) Interior Renovation; 7-11.">With LC for Building (LC# C-14-17072) Interior Renovation; 7-11.</option>
				                            	<option value="To submit barangay council resolution with in 30 days from the date of issuance, non compliance revokes this clearance.">To submit barangay council resolution with in 30 days from the date of issuance, non compliance revokes this clearance.</option>
				                            	<option value="No depository of trucks at business site">No depository of trucks at business site</option>
				                            	<option value="No storage, selling and display of products at business site.">No storage, selling and display of products at business site.</option>
				                            	<option value="Must be sound proofed as per City Ordinance No. SP1153, S-2002">Must be sound proofed as per City Ordinance No. SP1153, S-2002</option>
				                            	<option value="Not to utilize street/sidewalk for business operation">Not to utilize street/sidewalk for business operation</option>
				                            	<option value="With resolution no. SP-5165 S-2011 authorizing the issuance of a special use permit.">With resolution no. SP-5165 S-2011 authorizing the issuance of a special use permit.</option>
				                            	<option value="With special permit resolution no. SP-7137, S-2017 & bgy. resolution no. 120, S-17.">With special permit resolution no. SP-7137, S-2017 & bgy. resolution no. 120, S-17.</option>
				                            	<option value="With HOA clearance allowing business activity">With HOA clearance allowing business activity</option>
				                            	<option value="With neighbors consent allowing business activity">With neighbors consent allowing business activity</option>
				                            	<option value="With neighbors consent dated July 2015">With Neighbors Consent dated July 2015</option>
				                            	<option value="With HOA Clearance dated July 2015">With HOA Clearance dated July 2015</option>
				                            	<option value="With bldg. permit for commercial building/LC# C-16-18794 dated MAY 27,2016">With bldg. permit for commercial building/LC# C-16-18794 dated MAY 27,2016</option>
				                            	<option value="no storage, selling and display of products at business site.">no storage, selling and display of products at business site.</option>
				                            	<option value="Submitted proof of lot ownership from NGCEDP & neighbor's consent dated August 2015">Submitted proof of lot ownership from NGCEDP & neighbor's consent dated August 2015</option>
				                            	<option value="Subject to submission of lot ownership/lot award one year from date of issuance">Subject to submission of lot ownership/lot award one year from date of issuance</option>
				                            	<option value="With condominium certificate allowing business activity">With condominium certificate allowing business activity</option>
				                            	<option value="With cert. of occupancy for 4-storey comm. bldg. no. co-01404440 dated sept.5,2003">With cert. of occupancy for 4-storey comm. bldg. no. co-01404440 dated sept.5,2003</option>
				                            	<option value="Submitted affidavit of undertaking (use of property)">Submitted affidavit of undertaking (use of property)</option>
				                            	<option value="Not to utilize street/sidewalk for business operation">Not to utilize street/sidewalk for business operation</option>
				                            	<option value="Maginhawa Art and Food Hub">Maginhawa Art and Food Hub</option>
				                            	<option value="With cert. of occupancy as 5 storey commercial bldg. co. no. 01403514 dated 2/5/03.">With cert. of occupancy as 5 storey commercial bldg. co. no. 01403514 dated 2/5/03.</option>
				                            	<option value="With bldg permit for three storey w/ office bp # BP160901474 dated 09/19/2016.">With bldg permit for three storey w/ office bp # BP160901474 dated 09/19/2016.</option>
				                            	<option value="With certificate of occupancy #95-01-00 022 dated feb. 10, 1995.">With certificate of occupancy #95-01-00 022 dated feb. 10, 1995.</option>
				                            	<option value="With special use permit resolution no. sp-4188,s-2008 & bgy. resolution no. 013, s-08">With special use permit resolution no. sp-4188,s-2008 & bgy. resolution no. 013, s-08</option>
				                            	<option value="Not to exceed sixteen classrooms">Not to exceed sixteen classrooms</option>
				                            	<option value="With locational clearance for building as commercial lc no. C-16-19493.">With locational clearance for building as commercial lc no. C-16-19493.</option>
				                            	<option value="Must be soundproofed as per City Ordinance SP-1153, S-2002.">Must be soundproofed as per City Ordinance SP-1153, S-2002.</option>	
				                            	<option value="Not more than 20 units of LPG tanks.">Not more than 20 units of LPG tanks.</option>
				                            	<option value="Not to exceed sixteen ( 16 ) classrooms.">Not to exceed sixteen ( 16 ) classrooms.</option>
				                            	<option value="Not to utilize street / sidewalk for business operation.">Not to utilize street / sidewalk for business operation.</option>
				                            	<option value="Does not meet the minimum area requirement of 100 sq.m. as per Quezon City Ordinance SP-918, S-2000 as amended">Does not meet the minimum area requirement of 100 sq.m. as per Quezon City Ordinance SP-918, S-2000 as amended</option>
				                            	<option value="Does not meet the minimun area of 30 sq.m. as per Quezon City Ordinance SP-1711, S-2006.">Does not meet the minimun area of 30 sq.m. as per Quezon City Ordinance SP-1711, S-2006.</option>
				                            	<option value="Project activity is not permissible in the zone as per Executive Order 620-A & 620">Project activity is not permissible in the zone as per Executive Order 620-A & 620</option>
				                            	<option value="Area is within the jurisdiction of Housing & Urban Development Coordinating Council ( HUDCC )">Area is within the jurisdiction of Housing & Urban Development Coordinating Council ( HUDCC )</option>
				                            	<option value="Project activity is not permissible in the zone as per City Ordinance SP-918, S-2000 as amended">Project activity is not permissible in the zone as per City Ordinance SP-918, S-2000 as amended</option>
				                            	<option value="To submit certificate of Program Award from HUDCC / Proof of Ownership Within90 Days upon receipt Non-compliance revokes this certificate">To submit certificate of Program Award from HUDCC / Proof of Ownership Within90 Days upon receipt Non-compliance revokes this certificate</option>
				                            	<option value="To submit authorization from the City Council for Zoning Administrator to issue a Special Use Permit as per QC Zoning Ordinance SP-918, S-2000 as amended.">To submit authorization from the City Council for Zoning Administrator to issue a Special Use Permit as per QC Zoning Ordinance SP-918, S-2000 as amended.</option>
				                            	<option value="To submit certificate of Program Award from NGCHP/Proof of Land Ownership within 30 days upon receipt. Non-Compliance revokes this certificate.">To submit certificate of Program Award from NGCHP/Proof of Land Ownership within 30 days upon receipt. Non-Compliance revokes this certificate.</option>
				                            	<option value="To submit certificate of Pollution clearance from EPWMD within 45 days upon receipt of this clearance. Non-Compliance revokes this certificate.">To submit certificate of Pollution clearance from EPWMD within 45 days upon receipt of this clearance. Non-Compliance revokes this certificate.</option>
				                            	<option value="New  Business would require lay out plan for the approval of the Zoning Officials.">New  Business would require lay out plan for the approval of the Zoning Officials.</option>
				                            	<option value="To submit lay out plan of the building.">To submit lay out plan of the building.</option>
				                            	<option value="No actual training shall be conducted at business site.">No actual training shall be conducted at business site.</option>
				                            	<option value="OTHERS">OTHERS</option>
	                            			</select>	   
			                    		</c:when>		                    
			                    	</c:choose>		                    	                          	                           								
		                        </div>	        
		                	</div>  
		                	<div class="form-group col-sm-6">
		                		<label for="otherRemarks">Other Remarks</label>
		                		<input name="otherRemarks" id="otherRemarks" type="text" class="form-control" value="<c:out value="${items.otherRemarks}"/>" readonly>
		                	</div>	                    					            		                                             	                    
	                	</div>
	                	<!-- Remarks 2 -->
	                	<div class="form-row">	                    	
		                    <div class="form-group col-sm-6">	
		                    	<label for="remarks2">Remarks 2</label><br>   
		                    	<div class="input-group mb-2">	     
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
				                    		<select class="form-control inputField" id="remarks2" name="remarks2">
					                    		<option selected value="<c:out value="${items.remarks2}"/>"><c:out value="${items.remarks2}"/></option>	                    		
				                            	<option disabled>----------Choose Remarks----------</option>
				                            	<option value="To submit Barangay Council Resolution with in 30 days from the date of issuance, non compliance revokes this clearance.">To submit Barangay Council Resolution with in 30 days from the date of issuance, non compliance revokes this clearance.</option>	
				                            	<option value="With Neighbors Consent allowing business operation.">With Neighbors Consent allowing business operation.</option>
				                            	<option value="With HOA Clearance allowing business operation.">With HOA Clearance allowing business operation.</option>
				                            	<option value="With Barangay Council Resolution No. 525, S-2018.">With Barangay Council Resolution No. 525, S-2018.</option>
				                            	<option value="No storage, selling and display of products at business site.">No storage, selling and display of products at business site.</option>
				                            	<option value="Submitted certification from Social Finance Housing Corp.">Submitted certification from Social Finance Housing Corp.</option>
				                            	<option value="With LC for Building (LC# C-14-17072) Interior Renovation; 7-11.">With LC for Building (LC# C-14-17072) Interior Renovation; 7-11.</option>
				                            	<option value="To submit barangay council resolution with in 30 days from the date of issuance, non compliance revokes this clearance.">To submit barangay council resolution with in 30 days from the date of issuance, non compliance revokes this clearance.</option>
				                            	<option value="No depository of trucks at business site">No depository of trucks at business site</option>
				                            	<option value="No storage, selling and display of products at business site.">No storage, selling and display of products at business site.</option>
				                            	<option value="Must be sound proofed as per City Ordinance No. SP1153, S-2002">Must be sound proofed as per City Ordinance No. SP1153, S-2002</option>
				                            	<option value="Not to utilize street/sidewalk for business operation">Not to utilize street/sidewalk for business operation</option>
				                            	<option value="With resolution no. SP-5165 S-2011 authorizing the issuance of a special use permit.">With resolution no. SP-5165 S-2011 authorizing the issuance of a special use permit.</option>
				                            	<option value="With special permit resolution no. SP-7137, S-2017 & bgy. resolution no. 120, S-17.">With special permit resolution no. SP-7137, S-2017 & bgy. resolution no. 120, S-17.</option>
				                            	<option value="With HOA clearance allowing business activity">With HOA clearance allowing business activity</option>
				                            	<option value="With neighbors consent allowing business activity">With neighbors consent allowing business activity</option>
				                            	<option value="With neighbors consent dated July 2015">With Neighbors Consent dated July 2015</option>
				                            	<option value="With HOA Clearance dated July 2015">With HOA Clearance dated July 2015</option>
				                            	<option value="With bldg. permit for commercial building/LC# C-16-18794 dated MAY 27,2016">With bldg. permit for commercial building/LC# C-16-18794 dated MAY 27,2016</option>
				                            	<option value="no storage, selling and display of products at business site.">no storage, selling and display of products at business site.</option>
				                            	<option value="Submitted proof of lot ownership from NGCEDP & neighbor's consent dated August 2015">Submitted proof of lot ownership from NGCEDP & neighbor's consent dated August 2015</option>
				                            	<option value="Subject to submission of lot ownership/lot award one year from date of issuance">Subject to submission of lot ownership/lot award one year from date of issuance</option>
				                            	<option value="With condominium certificate allowing business activity">With condominium certificate allowing business activity</option>
				                            	<option value="With cert. of occupancy for 4-storey comm. bldg. no. co-01404440 dated sept.5,2003">With cert. of occupancy for 4-storey comm. bldg. no. co-01404440 dated sept.5,2003</option>
				                            	<option value="Submitted affidavit of undertaking (use of property)">Submitted affidavit of undertaking (use of property)</option>
				                            	<option value="Not to utilize street/sidewalk for business operation">Not to utilize street/sidewalk for business operation</option>
				                            	<option value="Maginhawa Art and Food Hub">Maginhawa Art and Food Hub</option>
				                            	<option value="With cert. of occupancy as 5 storey commercial bldg. co. no. 01403514 dated 2/5/03.">With cert. of occupancy as 5 storey commercial bldg. co. no. 01403514 dated 2/5/03.</option>
				                            	<option value="With bldg permit for three storey w/ office bp # BP160901474 dated 09/19/2016.">With bldg permit for three storey w/ office bp # BP160901474 dated 09/19/2016.</option>
				                            	<option value="With certificate of occupancy #95-01-00 022 dated feb. 10, 1995.">With certificate of occupancy #95-01-00 022 dated feb. 10, 1995.</option>
				                            	<option value="With special use permit resolution no. sp-4188,s-2008 & bgy. resolution no. 013, s-08">With special use permit resolution no. sp-4188,s-2008 & bgy. resolution no. 013, s-08</option>
				                            	<option value="Not to exceed sixteen classrooms">Not to exceed sixteen classrooms</option>
				                            	<option value="With locational clearance for building as commercial lc no. C-16-19493.">With locational clearance for building as commercial lc no. C-16-19493.</option>
				                            	<option value="Must be soundproofed as per City Ordinance SP-1153, S-2002.">Must be soundproofed as per City Ordinance SP-1153, S-2002.</option>	
				                            	<option value="Not more than 20 units of LPG tanks.">Not more than 20 units of LPG tanks.</option>
				                            	<option value="Not to exceed sixteen ( 16 ) classrooms.">Not to exceed sixteen ( 16 ) classrooms.</option>
				                            	<option value="Not to utilize street / sidewalk for business operation.">Not to utilize street / sidewalk for business operation.</option>
				                            	<option value="Does not meet the minimum area requirement of 100 sq.m. as per Quezon City Ordinance SP-918, S-2000 as amended">Does not meet the minimum area requirement of 100 sq.m. as per Quezon City Ordinance SP-918, S-2000 as amended</option>
				                            	<option value="Does not meet the minimun area of 30 sq.m. as per Quezon City Ordinance SP-1711, S-2006.">Does not meet the minimun area of 30 sq.m. as per Quezon City Ordinance SP-1711, S-2006.</option>
				                            	<option value="Project activity is not permissible in the zone as per Executive Order 620-A & 620">Project activity is not permissible in the zone as per Executive Order 620-A & 620</option>
				                            	<option value="Area is within the jurisdiction of Housing & Urban Development Coordinating Council ( HUDCC )">Area is within the jurisdiction of Housing & Urban Development Coordinating Council ( HUDCC )</option>
				                            	<option value="Project activity is not permissible in the zone as per City Ordinance SP-918, S-2000 as amended">Project activity is not permissible in the zone as per City Ordinance SP-918, S-2000 as amended</option>
				                            	<option value="To submit certificate of Program Award from HUDCC / Proof of Ownership Within90 Days upon receipt Non-compliance revokes this certificate">To submit certificate of Program Award from HUDCC / Proof of Ownership Within90 Days upon receipt Non-compliance revokes this certificate</option>
				                            	<option value="To submit authorization from the City Council for Zoning Administrator to issue a Special Use Permit as per QC Zoning Ordinance SP-918, S-2000 as amended.">To submit authorization from the City Council for Zoning Administrator to issue a Special Use Permit as per QC Zoning Ordinance SP-918, S-2000 as amended.</option>
				                            	<option value="To submit certificate of Program Award from NGCHP/Proof of Land Ownership within 30 days upon receipt. Non-Compliance revokes this certificate.">To submit certificate of Program Award from NGCHP/Proof of Land Ownership within 30 days upon receipt. Non-Compliance revokes this certificate.</option>
				                            	<option value="To submit certificate of Pollution clearance from EPWMD within 45 days upon receipt of this clearance. Non-Compliance revokes this certificate.">To submit certificate of Pollution clearance from EPWMD within 45 days upon receipt of this clearance. Non-Compliance revokes this certificate.</option>
				                            	<option value="New  Business would require lay out plan for the approval of the Zoning Officials.">New  Business would require lay out plan for the approval of the Zoning Officials.</option>
				                            	<option value="To submit lay out plan of the building.">To submit lay out plan of the building.</option>
				                            	<option value="No actual training shall be conducted at business site.">No actual training shall be conducted at business site.</option>
				                            	<option value="OTHERS">OTHERS</option>
		                            		</select>	   
			                    		</c:when>			                    
			                    	</c:choose>	                    	                     	                           								
		                        </div>	        
		                	</div>  
		                	<div class="form-group col-sm-6">
		                		<label for="otherRemarks2">Other Remarks 2</label>
		                		<input name="otherRemarks2" id="otherRemarks2" type="text" class="form-control" value="<c:out value="${items.otherRemarks2}"/>" readonly>
		                	</div>	                    					            		                                             	                    
	                	</div>
	                	<!-- Remarks 3 -->
	                	<div class="form-row">	                    	
		                    <div class="form-group col-sm-6">	
		                    	<label for="remarks3">Remarks 3</label><br>   
		                    	<div class="input-group mb-2">	        
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
				                    		<select class="form-control inputField" id="remarks3" name="remarks3">
					                    		<option selected value="<c:out value="${items.remarks3}"/>"><c:out value="${items.remarks3}"/></option>	                    		
				                            	<option disabled>----------Choose Remarks----------</option>
				                            	<option value="To submit Barangay Council Resolution with in 30 days from the date of issuance, non compliance revokes this clearance.">To submit Barangay Council Resolution with in 30 days from the date of issuance, non compliance revokes this clearance.</option>	
				                            	<option value="With Neighbors Consent allowing business operation.">With Neighbors Consent allowing business operation.</option>
				                            	<option value="With HOA Clearance allowing business operation.">With HOA Clearance allowing business operation.</option>
				                            	<option value="With Barangay Council Resolution No. 525, S-2018.">With Barangay Council Resolution No. 525, S-2018.</option>
				                            	<option value="No storage, selling and display of products at business site.">No storage, selling and display of products at business site.</option>
				                            	<option value="Submitted certification from Social Finance Housing Corp.">Submitted certification from Social Finance Housing Corp.</option>
				                            	<option value="With LC for Building (LC# C-14-17072) Interior Renovation; 7-11.">With LC for Building (LC# C-14-17072) Interior Renovation; 7-11.</option>
				                            	<option value="To submit barangay council resolution with in 30 days from the date of issuance, non compliance revokes this clearance.">To submit barangay council resolution with in 30 days from the date of issuance, non compliance revokes this clearance.</option>
				                            	<option value="No depository of trucks at business site">No depository of trucks at business site</option>
				                            	<option value="No storage, selling and display of products at business site.">No storage, selling and display of products at business site.</option>
				                            	<option value="Must be sound proofed as per City Ordinance No. SP1153, S-2002">Must be sound proofed as per City Ordinance No. SP1153, S-2002</option>
				                            	<option value="Not to utilize street/sidewalk for business operation">Not to utilize street/sidewalk for business operation</option>
				                            	<option value="With resolution no. SP-5165 S-2011 authorizing the issuance of a special use permit.">With resolution no. SP-5165 S-2011 authorizing the issuance of a special use permit.</option>
				                            	<option value="With special permit resolution no. SP-7137, S-2017 & bgy. resolution no. 120, S-17.">With special permit resolution no. SP-7137, S-2017 & bgy. resolution no. 120, S-17.</option>
				                            	<option value="With HOA clearance allowing business activity">With HOA clearance allowing business activity</option>
				                            	<option value="With neighbors consent allowing business activity">With neighbors consent allowing business activity</option>
				                            	<option value="With neighbors consent dated July 2015">With Neighbors Consent dated July 2015</option>
				                            	<option value="With HOA Clearance dated July 2015">With HOA Clearance dated July 2015</option>
				                            	<option value="With bldg. permit for commercial building/LC# C-16-18794 dated MAY 27,2016">With bldg. permit for commercial building/LC# C-16-18794 dated MAY 27,2016</option>
				                            	<option value="no storage, selling and display of products at business site.">no storage, selling and display of products at business site.</option>
				                            	<option value="Submitted proof of lot ownership from NGCEDP & neighbor's consent dated August 2015">Submitted proof of lot ownership from NGCEDP & neighbor's consent dated August 2015</option>
				                            	<option value="Subject to submission of lot ownership/lot award one year from date of issuance">Subject to submission of lot ownership/lot award one year from date of issuance</option>
				                            	<option value="With condominium certificate allowing business activity">With condominium certificate allowing business activity</option>
				                            	<option value="With cert. of occupancy for 4-storey comm. bldg. no. co-01404440 dated sept.5,2003">With cert. of occupancy for 4-storey comm. bldg. no. co-01404440 dated sept.5,2003</option>
				                            	<option value="Submitted affidavit of undertaking (use of property)">Submitted affidavit of undertaking (use of property)</option>
				                            	<option value="Not to utilize street/sidewalk for business operation">Not to utilize street/sidewalk for business operation</option>
				                            	<option value="Maginhawa Art and Food Hub">Maginhawa Art and Food Hub</option>
				                            	<option value="With cert. of occupancy as 5 storey commercial bldg. co. no. 01403514 dated 2/5/03.">With cert. of occupancy as 5 storey commercial bldg. co. no. 01403514 dated 2/5/03.</option>
				                            	<option value="With bldg permit for three storey w/ office bp # BP160901474 dated 09/19/2016.">With bldg permit for three storey w/ office bp # BP160901474 dated 09/19/2016.</option>
				                            	<option value="With certificate of occupancy #95-01-00 022 dated feb. 10, 1995.">With certificate of occupancy #95-01-00 022 dated feb. 10, 1995.</option>
				                            	<option value="With special use permit resolution no. sp-4188,s-2008 & bgy. resolution no. 013, s-08">With special use permit resolution no. sp-4188,s-2008 & bgy. resolution no. 013, s-08</option>
				                            	<option value="Not to exceed sixteen classrooms">Not to exceed sixteen classrooms</option>
				                            	<option value="With locational clearance for building as commercial lc no. C-16-19493.">With locational clearance for building as commercial lc no. C-16-19493.</option>
				                            	<option value="Must be soundproofed as per City Ordinance SP-1153, S-2002.">Must be soundproofed as per City Ordinance SP-1153, S-2002.</option>	
				                            	<option value="Not more than 20 units of LPG tanks.">Not more than 20 units of LPG tanks.</option>
				                            	<option value="Not to exceed sixteen ( 16 ) classrooms.">Not to exceed sixteen ( 16 ) classrooms.</option>
				                            	<option value="Not to utilize street / sidewalk for business operation.">Not to utilize street / sidewalk for business operation.</option>
				                            	<option value="Does not meet the minimum area requirement of 100 sq.m. as per Quezon City Ordinance SP-918, S-2000 as amended">Does not meet the minimum area requirement of 100 sq.m. as per Quezon City Ordinance SP-918, S-2000 as amended</option>
				                            	<option value="Does not meet the minimun area of 30 sq.m. as per Quezon City Ordinance SP-1711, S-2006.">Does not meet the minimun area of 30 sq.m. as per Quezon City Ordinance SP-1711, S-2006.</option>
				                            	<option value="Project activity is not permissible in the zone as per Executive Order 620-A & 620">Project activity is not permissible in the zone as per Executive Order 620-A & 620</option>
				                            	<option value="Area is within the jurisdiction of Housing & Urban Development Coordinating Council ( HUDCC )">Area is within the jurisdiction of Housing & Urban Development Coordinating Council ( HUDCC )</option>
				                            	<option value="Project activity is not permissible in the zone as per City Ordinance SP-918, S-2000 as amended">Project activity is not permissible in the zone as per City Ordinance SP-918, S-2000 as amended</option>
				                            	<option value="To submit certificate of Program Award from HUDCC / Proof of Ownership Within90 Days upon receipt Non-compliance revokes this certificate">To submit certificate of Program Award from HUDCC / Proof of Ownership Within90 Days upon receipt Non-compliance revokes this certificate</option>
				                            	<option value="To submit authorization from the City Council for Zoning Administrator to issue a Special Use Permit as per QC Zoning Ordinance SP-918, S-2000 as amended.">To submit authorization from the City Council for Zoning Administrator to issue a Special Use Permit as per QC Zoning Ordinance SP-918, S-2000 as amended.</option>
				                            	<option value="To submit certificate of Program Award from NGCHP/Proof of Land Ownership within 30 days upon receipt. Non-Compliance revokes this certificate.">To submit certificate of Program Award from NGCHP/Proof of Land Ownership within 30 days upon receipt. Non-Compliance revokes this certificate.</option>
				                            	<option value="To submit certificate of Pollution clearance from EPWMD within 45 days upon receipt of this clearance. Non-Compliance revokes this certificate.">To submit certificate of Pollution clearance from EPWMD within 45 days upon receipt of this clearance. Non-Compliance revokes this certificate.</option>
				                            	<option value="New  Business would require lay out plan for the approval of the Zoning Officials.">New  Business would require lay out plan for the approval of the Zoning Officials.</option>
				                            	<option value="To submit lay out plan of the building.">To submit lay out plan of the building.</option>
				                            	<option value="No actual training shall be conducted at business site.">No actual training shall be conducted at business site.</option>
				                            	<option value="OTHERS">OTHERS</option>
		                            		</select>	   
			                    		</c:when>		                    		
			                    	</c:choose>                   	                  	                           									
		                        </div>	        
		                	</div>  
		                	<div class="form-group col-sm-6">
		                		<label for="otherRemarks3">Other Remarks 3</label>
		                		<input name="otherRemarks3" id="otherRemarks3" type="text" class="form-control" value="<c:out value="${items.otherRemarks3}"/>" readonly>
		                	</div>	                    					            		                                             	                    
	                	</div>
	                	<div class="form-row">	                    	
		                    <div class="form-group col-sm-6">	
		                    	<label for="remarks">Notice of Action Remarks</label><br>   
		                    	<div class="input-group mb-2">	 
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
				                    		<select class="form-control inputField" id="noticeOfAction" name="noticeOfAction">
					                    		<option selected value="<c:out value="${items.noticeOfAction}"/>"><c:out value="${items.noticeOfAction}"/></option>
				                            	<option disabled>----------Choose Remarks----------</option>
				                            	<option value="TO SUBMIT AFFIDAVIT OF UNDERTAKING (POSSESSION BY WAY OF RIGHTS)">TO SUBMIT AFFIDAVIT OF UNDERTAKING (POSSESSION BY WAY OF RIGHTS)</option>	
				                            	<option value="TO SUBMIT NEW LEASE CONTRACT">TO SUBMIT NEW LEASE CONTRACT</option>		                            	
				                            	<option value="TO SUBMIT PROOF OF LOT OWNERSHIP">TO SUBMIT PROOF OF LOT OWNERSHIP</option>
				                            	<option value="TO SUBMIT COMMENTS OF HOMEOWNERS ASSOCIATION/IMMEDIATE NEIGHBORS CONSENT AND BARANGAY COUNCIL RESOLUTION (ART. VI SEC. 49)">TO SUBMIT COMMENTS OF HOMEOWNERS ASSOCIATION/IMMEDIATE NEIGHBORS CONSENT AND BARANGAY COUNCIL RESOLUTION (ART. VI SEC. 49)</option>
				                            	<option value="TO SECURE COMMENTS FROM HOME OWNERS ASSOCIATION/LESSOR'S CONSENT">TO SECURE COMMENTS FROM HOME OWNERS ASSOCIATION/LESSOR'S CONSENT</option>
				                            	<option value="BUSINESS ACTIVITY IS NOT PERMISSIBLE IN THE ZONE">BUSINESS ACTIVITY IS NOT PERMISSIBLE IN THE ZONE</option>
				                            	<option value="TO SECURE CERTIFICATE OF NON CONFORMANCE FROM THE CITY COUNCIL">TO SECURE CERTIFICATE OF NON CONFORMANCE FROM THE CITY COUNCIL</option>
				                            	<option value="TO SUBMIT CERTIFICATE OF LOT AWARD / PROOF OF LOT OWNERSHIP">TO SUBMIT CERTIFICATE OF LOT AWARD / PROOF OF LOT OWNERSHIP</option>
				                            	<option value="TO SECURE COMMENTS FROM HOMEOWNERS ASSOCIATION/IMMEDIATE NEIGHBORS & PROOF OF LOT OWNERSHIP">TO SECURE COMMENTS FROM HOMEOWNERS ASSOCIATION/IMMEDIATE NEIGHBORS & PROOF OF LOT OWNERSHIP</option>
				                            	<option value="TO SUBMIT SPECIAL PERMIT ISSUED BY THE CITY COUNCIL">TO SUBMIT SPECIAL PERMIT ISSUED BY THE CITY COUNCIL</option>
				                            	<option value="TO SUBMIT BUILDING PERMIT  OF THE EXISTING STRUCTURE.">TO SUBMIT BUILDING PERMIT  OF THE EXISTING STRUCTURE.</option>
				                            	<option value="TO SUBMIT SPECIAL USE  PERMIT ISSUED BY THE CITY COUNCIL">TO SUBMIT SPECIAL USE  PERMIT ISSUED BY THE CITY COUNCIL</option>
				                            	<option value="TO SECURE COMMENTS FROM HOMEOWNERS ASSOCIATION/IMMEDIATE NEIGHBORS & BUILDING PERMIT OF THE EXISTING STRUCTURE.">TO SECURE COMMENTS FROM HOMEOWNERS ASSOCIATION/IMMEDIATE NEIGHBORS & BUILDING PERMIT OF THE EXISTING STRUCTURE.</option>
				                            	<option value="TO SUBMIT BARANGAY COUNCIL RESOLUTION">TO SUBMIT BARANGAY COUNCIL RESOLUTION</option>
				                            	<option value="TO SUBMIT AFFIDAVIT OF UNDERTAKING (USE OF PROPERTY) OR TRANSFER CERTIFICATE TITLE (TCT)">TO SUBMIT AFFIDAVIT OF UNDERTAKING (USE OF PROPERTY) OR TRANSFER CERTIFICATE TITLE (TCT)</option>
				                            	<option value="TO SUBMIT AFFIDAVIT OF UNDERTAKING (USE OF PROPERTY)">TO SUBMIT AFFIDAVIT OF UNDERTAKING (USE OF PROPERTY)</option>
				                            	<option value="TO SUBMIT TRANSFER CERT. OF TITLE (TCT)">TO SUBMIT TRANSFER CERT. OF TITLE (TCT)</option>
				                            	<option value="TO SUBMIT BUILDING PERMIT  OF THE EXISTING STRUCTURE & TRANSFER CERTIFICATE OF TITLE (TCT)">TO SUBMIT BUILDING PERMIT  OF THE EXISTING STRUCTURE & TRANSFER CERTIFICATE OF TITLE (TCT)</option>
				                            	<option value="TO SUBMIT BARANGAY CLEARANCE FOR BUSINESS ADDRESS">TO SUBMIT BARANGAY CLEARANCE FOR BUSINESS ADDRESS  </option>
				                            	<option value="TO SUBMIT MARKET FRANCHISE  ISSUED BY THE CITY COUNCIL">TO SUBMIT MARKET FRANCHISE  ISSUED BY THE CITY COUNCIL</option>
				                            	<option value="FOR COMPLIANCE WITH THE PROVISIONS OF THE QUEZON CITY ZONING ORDINANCE">FOR COMPLIANCE WITH THE PROVISIONS OF THE QUEZON CITY ZONING ORDINANCE</option>
				                            	<option value="TO SUBMIT AMENDMENT FORM OF BUSINESS PERMIT">TO SUBMIT AMENDMENT FORM OF BUSINESS PERMIT</option>
				                            	<option value="BUSINESS IS WITHIN A COMMERCIAL STRUCTURE">BUSINESS IS WITHIN A COMMERCIAL STRUCTURE</option>
				                            	<option value="DISAPPROVED">DISAPPROVED</option>
				                            	<option value="BUSINESS ACTIVITY IS CONDITIONAL USE IN R-3 ZONE">BUSINESS ACTIVITY IS CONDITIONAL USE IN R-3 ZONE</option>
				                            	<option value="TO SUBMIT COMMENTS OF HOMEOWNERS ASSOCIATION/IMMEDIATE NEIGHBORS CONSENT, BARANGAY COUNCIL RESOLUTION (ART. VI SEC. 49) & BUILDING PERMIT OF THE EXISTING STRUCTURE.">TO SUBMIT COMMENTS OF HOMEOWNERS ASSOCIATION/IMMEDIATE NEIGHBORS CONSENT, BARANGAY COUNCIL RESOLUTION (ART. VI SEC. 49) & BUILDING PERMIT OF THE EXISTING STRUCTURE.</option>
				                            	<option value="TO SUBMIT COMMENTS OF HOMEOWNERS ASSOCIATION/IMMEDIATE NEIGHBORS CONSENT, BARANGAY COUNCIL RESOLUTION (ART. VI SEC. 49) & PROOF OF LOT OWNERSHIP">TO SUBMIT COMMENTS OF HOMEOWNERS ASSOCIATION/IMMEDIATE NEIGHBORS CONSENT, BARANGAY COUNCIL RESOLUTION (ART. VI SEC. 49) & PROOF OF LOT OWNERSHIP</option>
				                            	<option value="OTHERS">OTHERS</option>
		                            		</select>	
			                    		</c:when>			                    		
		                    		</c:choose>		                  	                         	                           										   
		                        </div>	        
		                	</div>  
		                	<div class="form-group col-sm-6">
		                		<label for="otherRemarks">Other Notice of Action Remarks</label>
		                		<input name="otherNoticeOfAction" id="otherNoticeOfAction" type="text" class="form-control" value="<c:out value="${items.otherNoticeOfAction}"/>" readonly>
		                	</div>	                    					            		                                             	                                  		
	                	</div> 
	                	<div class="form-row">               	              	                    
		                    <div class="form-group col-sm-3">	
		                    	<label for="zoningOfficial">Zoning Official</label><br>          
		                    	<div class="input-group mb-2">	  
			                    	<c:choose>
			                    		<c:when test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">
				                    		<select class="form-control" name="zoningOfficial" id="zoningOfficial">
												<option selected value="<c:out value="${items.zoningOfficial}"/>"><c:out value="${items.zoningOfficial}"/></option>
												<option value="PEDRO P. RODRIGUEZ JR.">PEDRO P. RODRIGUEZ JR.</option>
											</select>   
			                    		</c:when>
			                    		<c:when test="${sessionScope.evaluation==null}">
			                    			<select class="form-control" name="zoningOfficial" id="zoningOfficial">
												<option disabled selected value="<c:out value="${items.zoningOfficial}"/>"><c:out value="${items.zoningOfficial}"/></option>
												<option disabled value="PEDRO P. RODRIGUEZ JR.">PEDRO P. RODRIGUEZ JR.</option>
											</select> 
			                    		</c:when>
			                    	</c:choose>		                    	                        	                           															
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-sm-3">
	                			<label for="encodedBy">Encoded by</label>
	                			<input name="encodedBy" id="encodedBy" type="text" class="form-control" value="<c:out value="${items.encodedBy}"/>" readonly>	                
	                		</div>	
	                		<div class="form-group col-sm-3">
	                			<label for="approvedBy">Approved by / Final Evaluator</label>
	                			<input name="approvedBy" id="approvedBy" type="text" class="form-control" value="<c:out value="${items.approvedBy}"/>" readonly>	                
	                		</div>	
	                		<c:if test="${sessionScope.finalEvaluation!=null}">
		                		<div class="form-group col-sm-3">
		                			<label for="approveForPrinting">Approve for printing</label>
		                			<select name="approveForPrinting" id="approveForPrinting" class="form-control">	   
		                				<c:choose>
		                					<c:when test="${items.approvedForPrinting!=null }">
		                						<option selected value="<c:out value="${items.approvedForPrinting}"/>"><c:out value="${items.approvedForPrinting}"/></option>
		                					</c:when>
		                					<c:when test="${items.approvedForPrinting==null }">
		                						<option selected disabled value="">-----Select Action-----</option>
		                					</c:when>
		                				</c:choose>       					                			
		                				<option value="YES">YES</option>
		                				<option value="NO">NO</option>
		                			</select>               				                
		                		</div>	
	                		</c:if>		 				 				                                          
	                    </div>	                              				                	                            	
	                </div>		
	                <c:if test="${(sessionScope.evaluation!=null)||(sessionScope.finalEvaluation!=null)}">                  	 	                              
		                <div class="text-center p-2">	 
		                	 <p class="text-center" id="saveStatus"></p>  
		                	<button type="button" id="btnSave" value="Save" class="btn btn-primary btn-block rounded-0 py-2"><span><i class="fas fa-save"></i>Save</span></button>  	              	               	
		                </div>
	                </c:if>			                
	            </div>       
	        </form>
	        </c:forEach>	        	               
	    </div>
	</div>
</div>

<!-- Scripts -->
<script defer src="${pageContext.request.contextPath}/JS/evaluation.js"></script>	
<script defer src="${pageContext.request.contextPath}/JS/pages/barangayDropdown.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/pages/readOnlyRemove.js"></script>	
<script defer src="${pageContext.request.contextPath}/JS/backToTop.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/backToBottom.js"></script>

</body>
</html>