<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- css -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/uploadExcel.css">
<title>Upload Excel</title>
</head>
<body>
<c:if test="${(sessionScope.administrator == null)}">
   <c:redirect url="/logout"></c:redirect>
</c:if>
<jsp:include page="navbar.jsp" />
<div class="container pt-5">
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<div class="card mb-4 border-info rounded">
				<div class="card-header p-0">
                   <div class="bg-primary text-white text-center py-2">
                       <h2><i class="fas fa-upload"></i>Upload Excel File</h2>	                                          
                   </div>
	            </div>
				<div class="form-group p-3" align="center">
					<form method="post" action="${pageContext.request.contextPath}/JSP/uploadExcel "enctype="multipart/form-data" id="upload-form">									 
				    	<input type="file" class="form-control-file" name="file" accept=".xlsx, .xls, .csv"/><br>
				    		<span id="upload-error" class="text-danger">${uploadError}</span>	
				    					  
				    	<button type="submit" id="btn-upload" class="btn btn-primary btn-block">Upload</button>	    			
				    </form>
				</div>
			</div>
		</div>	
		<div class="col-sm-2"></div>	
	</div>
</div>
<div class="modal"><!-- Place at bottom of page --></div>
</body>

<script defer src="${pageContext.request.contextPath}/JS/jquery.form.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/upload.js"></script>
</html>