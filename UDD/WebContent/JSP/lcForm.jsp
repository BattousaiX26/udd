<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>LC Form</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/background.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/shadow.css">
<style>
body {
    font-family: "Times New Roman", Times, serif;
}
.arial-font{
	font-family: Arial, Helvetica, sans-serif;
}
td span{
	display:block; text-align:center; margin:0 auto;
}
.underline {
	text-decoration: underline;	
}
.border-3 {
    border-width:3px !important;
}
@media print {
    body{
        background-image: none;
    }
    #back{
    	display:none
    }
}
.container-imagetext {
    background-image:url(${pageContext.request.contextPath}/CSS/Images/logo/qc_bg.png);
    background-position:center;
    background-repeat: no-repeat;
    width:100%;
    height:100%;
}
</style>
</head>
<body>

<div class="container bg-white" id="areaToPrint">
	<c:if test="${empty itemsTobeEvaluated}">
		<div class="row p-4">	
			<div class="col-md-3"></div>
			<div class="col-md-6" id="mpNotFound" >
				 <div class="card border-danger rounded-0 text-center">
				 	<div class="card-header p-0">
			 		  	<div class="bg-danger text-white text-center py-2">
		                       <h2><i class="fas fa-exclamation-triangle text-warning"></i>Warning!<i class="fas fa-exclamation-triangle text-warning"></i></h2>		                                          
		                </div>
				 	</div>
				 	<div class="card-body p-3">
		               	<div class="text-center py-2">
		                       <h1 class="text-danger">Transaction Number not found or is not yet approved, ask evaluator for assistance!</h1>	
		                       <h5>Click <a href="${pageContext.request.contextPath}/JSP/lcFormSearch.jsp">here</a> to search again</h5>                                          
		                   </div>
		               </div>
				 </div>
		     </div>  
		     <div class="col-md-3"></div>     	
		</div>
	</c:if>
	<c:forEach items="${itemsTobeEvaluated}" var="items" varStatus="count">	
		<div class="row bg-white p-2">
			<div class="col-sm-2">
				<img src="${pageContext.request.contextPath}/CSS/Images/logo/Q.C._Logo.png" style="width:130px"><br>
				<span>MP NO. <c:out value="${items.mpNo}"/></span>	<!-- Previous size width:130px; -->
			</div>
			<div class="col-sm-8 text-center">
				<p class="arial-font">Republic of the Philippines<br><span class="font-weight-bold arial-font">QUEZON CITY</span><br>
				<span class="h5 font-italic font-weight-bold arial-font">CITY PLANNING AND DEVELOPMENT OFFICE</span><br>
				<span class="h5 font-italic font-weight-bold arial-font">ZONING ADMINISTRATION UNIT</span>
				</p>
			</div>
			<div class="col-sm-2 text-right">
			</div>
		</div>
		<div class="row bg-white">
			<div class="col-sm-3">
			</div>
			<div class="col-sm-6 text-center">
				<p><span class="h3 arial-font">LOCATIONAL CLEARANCE</span><br>
				<span class="arial-font">(FOR BUSINESS PERMIT PURPOSES ONLY)</span><br>
				<span class="font-weight-bold h5 underline arial-font">LC NO. <c:out value="${items.cpdoNo}"/></span></p></div>
			<div class="col-sm-3"></div>
		</div>
		<div class="row bg-white p-2">
			<div class="col-sm-12">
				<table style="width:100%;">
					<tbody>
						<tr>
							<td class="w-50 border border-dark"><small>TRANSACTION NO.</small><br><span><c:out value="${items.transactionNumber}"/></span></td>
							<td class="w-25 border border-dark"><small>DATE ISSUED(year/month/day):</small><br><span><c:out value="${items.dateIssued}"/></span></td>
							<td class="w-25 border border-dark"><small>VALID UNTIL(year/month/day):</small><br><span><c:out value="${items.validUntil}"/></span></td>
						</tr>
					</tbody>
				</table>
				<table style="width:100%;">
					<tbody>
						<tr>
							<td class="w-50 border border-bottom-0 border-dark"><small>BUSINESS ACTIVITY</small><br><small><span class="font-weight-bold"><c:out value="${items.businessActivityMainTile1}"/></span></small></td>
							<td class="w-50 border border-dark"><small>BUSINESS NAME:</small><br><span class="font-weight-bold"><c:out value="${items.businessName}"/></span></td>						
						</tr>
						<tr>
							<td class="w-50 border border-top-0 border-dark"><small><span class="font-weight-bold"><c:out value="${items.businessActivitySubTile1}"/></span></small></td>
							<td class="w-50 border border-dark"><small>ADDRESS:</small><br><span class="font-weight-bold"><c:out value="${items.address}"/><br></span></td>
						</tr>
					</tbody>
				</table>
				<table style="width:100%;">	
					<tbody>
						<tr>
							<td class="w-50 border border-dark"><small>OWNER NAME:</small><br><span class="font-weight-bold"><c:out value="${items.taxPayerName}"/><br></span></td>
							<td class="w-25 border border-dark"><small>BARANGAY:</small><br><span class="font-weight-bold"><c:out value="${items.barangay}"/></span></td>	
							<td class="w-25 border border-dark"><small>DISTRICT:</small><br><c:if test="${ items.district != '0'}"><span class="font-weight-bold"><c:out value="${items.district}"/></span></c:if></td>																			
						</tr>
					</tbody>
				</table>
				<table style="width:100%;">	
					<tbody>
						<tr>
							<td class="w-50 border border-dark"><small>ZONE CLASSIFICATION:</small><br><span class="font-weight-bold"><c:out value="${items.zoningClass}"/><br></span></td>
							<td class="w-25 border border-dark"><small>LOT AREA:</small><br><span class="font-weight-bold"><c:if test="${items.lotArea !='0'}"><c:out value="${items.lotArea}"/></c:if></span></td>	
							<td class="w-25 border border-dark"><small>BUSINESS AREA:</small><br><c:if test="${items.businessArea != '0'}"><span class="font-weight-bold"><c:out value="${items.businessArea}"/></span></c:if></td>																		
						</tr>
					</tbody>
				</table>
				<table style="width:100%;">	
					<tbody>
						<tr>
							<td class="w-75 border border-dark"><small>EVALUATION OF FACTS:</small><br><span class="font-weight-bold"><c:out value="${items.evaluationOfFacts}"/><br></span></td>
							<td class="w-25 border border-dark"><small>TCT NO:</small><br><span class="font-weight-bold"><c:out value="${items.tctNo}"/></span></td>													
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row bg-white p-2">
			<div class="col-sm-12">
				<c:if test = "${items.remarks != 'OTHERS'}">
					<small><small><c:out value="${items.remarks}"></c:out></small></small><br>
				</c:if>
				<c:if test = "${items.remarks == 'OTHERS'}">
					<small><small><c:out value="${items.otherRemarks}"></c:out></small></small><br>
				</c:if>
				<c:if test = "${items.remarks2 != 'OTHERS'}">
					<small><small><c:out value="${items.remarks2}"></c:out></small></small><br>
				</c:if>
				<c:if test = "${items.remarks2 == 'OTHERS'}">
					<small><c:out value="${items.otherRemarks2}"></c:out></small><br>
				</c:if>
				<c:if test = "${items.remarks3 != 'OTHERS'}">
					<small><small><c:out value="${items.remarks3}"></c:out></small></small><br>
				</c:if>
				<c:if test = "${items.remarks3 == 'OTHERS'}">
					<small><small><c:out value="${items.otherRemarks3}"></c:out></small></small>
				</c:if>
			</div>
		</div>
		<div class="row bg-white p-2"></div>
		<div class="row bg-white p-2">
			<div class="col-sm-3 text-center">
				<img src="data:image/png;base64,${items.imageQrCode}" />
			</div>
			<div class="col-sm-4 text-left">
				<img src="${pageContext.request.contextPath}/CSS/Images/square.png" class="thumbnail border border-secondary" style="width:100px;"><br>
					<small><small>NOT VALID WITHOUT SEAL</small></small>
				</div>
			<div class="col-sm-5 text-center">
				<p>
					<span class="font-weight-bold h5 arial-font">PEDRO P. RODRIGUEZ, JR.</span><br>
					<small>City Planning & Development Officer</small><br>
					<small>Zoning Administrator</small>
				</p>
			</div>
		</div>
		<div class="row bg-white p-2">
			<div class="col-sm-1">
			</div>
			<div class="col-sm-10 text-justify container-imagetext">
				<h5 align="center" class="underline">CONDITIONS</h5>
					1. This certification is valid for a period of <span class="underline font-weight-bold"><c:out value="${items.dateValid}"/></span> year/s from the date of its issuance and the same is subject for renewal within three(3) months prior to its expiration
					<br>
					2. That the proponent cannot undertake any activity other than those applied for and as indicated in this clearance without prior approval from this office.
					<br>
					3. That any material falsehood or misrepresentation in the submitted documents made in connection with this certification shall render this Null and Void.
					<br>				
					4. That this office reserves the right to inspect and review said project/operation and should there be any violation found, to institute cancellation proceedings.
					<br>
					5. That no additional activity/floor area than indicated in this Clearance shall be made/utilized without prior approval from this office.
					<br>
					6. That the pertinent provisions of the Quezon City Comprehensive Zoning Ordinance relative to performance standards shall be complied with.
					<br>
					7. That this certificate does not exempt the owner from securing other government permits and clearances as required in the operation of the applied business.
					<br>
					8. That this certificate can be revoked in the interest of the general public.
					<br>
					9. That no advertising and business sign to be displayed or put for public view shall be extended beyond the property line of the proponent.
					<br>
					10. That any violation of these conditions will mean the suspension or cancellation/revocation of this certificate and legally and criminally punishable under Quezon City Zoning Ordinance.
					<br>
					11. <span class="font-weight-bold">This clearance shall not be construed as a certification of the Zoning Administrative Unit as to the ownership by the applicant of the parcel of land subject of the clearance</span>.
					<br>
					12. That all conditions stipulated herein from part of this decision and are<span class="font-weight-bold"> subject to monitoring and actual verification</span>.							
			</div>
			<div class="col-sm-1">
			</div>
		</div>
		<div class="row bg-white p-2">
			<div class="col-sm-4 text-center">
				<small class="text-capitalize">// <c:out value="${sessionScope.nameOfUser}"></c:out></small>
			</div>
			<div class="col-sm-4 text-center">
				<small class="font-weight-bold">---nothing follows---</small>
			</div>
			<div class="col-sm-4 text-center">
				<small>QCG-CPDO-ZAU-QP-F09-V.01</small>				
			</div>
		</div>
	</c:forEach>
	<div id ="back" class="text-center">
		<c:if test="${not empty itemsTobeEvaluated}">
			<span id="back"> Click <a href="${pageContext.request.contextPath}/JSP/lcFormSearch.jsp">here</a> to go back</span>
			<button type="button" class="btn btn-primary btn-block" id="button-print"><span><i class="fas fa-print"></i></span>Print</button>
		</c:if>
	</div>
</div>

<!-- scripts -->
<script defer src="${pageContext.request.contextPath}/JS/jquery-3.2.1/jquery-3.2.1.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/js/bootstrap.bundle.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/js/bootstrap.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/bounce.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/printForm.js"></script>
</body>
</html>