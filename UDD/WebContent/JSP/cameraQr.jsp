<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta charset="utf-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/cameraQr.css">
<title>Camera QR</title>
</head>
<body>
<jsp:include page="navbar.jsp" />
<div class="container bg-light">
	
		<div class="row">
			<div class="col-sm-5">
				<video autoplay="true" id="videoElement">
		     
		   		</video> 
			</div>
			<div class="col-sm-1">
			</div>	
			<div class="col-sm-6">
				<canvas id="canvasID" width="500" height="375" style="border: 1px solid black;"></canvas>  
	    	</div>
	    </div>
	    <div class="row">
	    	<div class="col-sm-4">
	    		
	    	</div>
	    	<div class="col-sm-4 p-3">  
	    		<button type="button" class="btn btn-primary btn-block btn-lg" id="btnCapture"><span><i class="fas fa-camera"></i></span> Take photo</button>	   
	    	</div>
	    	<div class="col-sm-4"> 	    	
	    	</div>  	
	    </div> 
	    <div class="row p-3">
	     	<div class="col-sm-12 p-3" > 
	     		<p style="display:none" id="content" class="alert alert-info"></p>
	     	</div>
	    </div>
</div>

<script defer src="${pageContext.request.contextPath}/JS/qrCam/cameraView.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/qrCam/cameraCapture.js"></script>
</body>
</html>