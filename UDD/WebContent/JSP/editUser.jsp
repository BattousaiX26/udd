<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Edit User</title>
</head>
<body>
<jsp:include page="navbar.jsp" />
<c:if test="${sessionScope.administrator == null}">
   <c:redirect url="/JSP/receiving.jsp"></c:redirect>
</c:if>
<div class="container p-5">
	<div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6">
			<form action="${pageContext.request.contextPath}/JSP/manageAccounts" method="post">
				<div class="card border-info rounded-0">
					<div class="card-header p-0">
	                    <div class="bg-primary text-white text-center py-2">
	                        <h2><i class="fa fa-user"></i>Manage Account</h2>
	                        <p class="m-0">Administrator can edit user information here!</p>
	                    </div>
	                </div>
	                <div class="card-body p-3">
		                <div class="form-group">
		                	<label for="username" class="font-weight-bold">Username:</label>		                   	       	               
		                	<div class="input-group mb-2">		                		
		                        <div class="input-group-prepend">
		                        	<div class="input-group-text"><i class="fa fa-user text-info"></i></div>
		                        </div>	
		                        <input type="text" class="form-control" name="id" value="<c:out value="${userDetails.id}"/>" hidden>	                       
		                        <input type="text" class="form-control" id="username" name="username" value="<c:out value="${userDetails.username}"/>" required>	                    		
		                    </div>                                   
		                </div>
		                <div class="form-group">
		                	<label for="fullname" class="font-weight-bold">Fullname:</label>		   
		                	<div class="input-group mb-2">		                		                	       		                        
		                        <div class="input-group-prepend">
		                        	<div class="input-group-text"><i class="fa fa-user text-info"></i></div>
		                        </div>	           
		                        <input type="text" class="form-control" id="fullname" name="fullname" value="<c:out value="${userDetails.fullname}"/>" required>	            		
		                    </div>                                   
		                </div>
		                <div class="form-group">
		                	<label for="userLevel" class="font-weight-bold">User Level:</label>		             
		                	<div class="input-group mb-2">		                 	       	                        
		                        <div class="input-group-prepend">
		                        	<div class="input-group-text"><i class="fas fa-user text-info"></i></div>
		                        </div>	
		                        <select name="userLevel" id="userLevel" class="form-control" required>
		                        	<option selected value="<c:out value="${userDetails.userLevel}"/>"><c:out value="${userDetails.userLevel}"/></option>
		                        	<option disabled>-----Select new level-----</option>
		                        	<option value="user">user</option>
	                            	<option value="administrator">administrator</option>
		                        </select>	                                    		
		                    </div>                                   
		                </div>
		                <div class="form-group">
		                    <label for="section" class="font-weight-bold">Section:</label>	
	                    	<div class="input-group mb-2">
		                 		<div class="input-group-prepend">
		                        	<div class="input-group-text"><i class="fas fa-user text-info"></i></div>
		                        </div>	
		                        <select name="section" id="section" class="form-control" required>
		                        	<option selected value="<c:out value="${userDetails.section}"/>"><c:out value="${userDetails.section}"/></option>
		                        	<option disabled>-----Select new section-----</option>
		                        	<option value="receiving">Receiving</option>
	                            	<option value="evaluation">Evaluation</option>
	                            	<option value="releasing">Releasing</option>
	                            	<option value="printing">Printing</option>
		                        </select>	                    	            	   	                                 
		                	</div>	
			            </div>
		                <div class="form-group">
		                	<label for="status" class="font-weight-bold">Status:</label>		   
		                	<div class="input-group mb-2">		                		                	       		                        
		                        <div class="input-group-prepend">
		                        	<div class="input-group-text"><i class="fa fa-user text-info"></i></div>
		                        </div>	           
		                        <select name="status" id="status" class="form-control" required>
		                        	<option selected value="<c:out value="${userDetails.status}"/>"><c:out value="${userDetails.status}"/></option>
		                        	<option disabled>-----Select Status-----</option>
		                        	<option value="pending">Disable</option>		                   
	                            	<option value="confirmed">Confirm</option>
		                        </select>	  	            		
		                    </div>                                   
		                </div>
	                    <div class="form-group">
							<c:if test="${not empty isSaved}">
							    <div class="alert alert-success" role="alert">
									Updated user info successfully! Click <a href="${pageContext.request.contextPath}/JSP/manageAccounts?action=view">Here</a> to edit more users!
								</div>
							</c:if>
							<c:if test="${not empty notSaved}">
							    <div class="alert alert-danger" role="alert">
									Unable to update user info!
								</div>
							</c:if>
                        </div> 
		                <div class="text-center">
		                	<input type="submit" class="btn btn-primary btn-block rounded-0 py-2" value="Update"/> 	  
		                </div>		                	 	  	    
	                </div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Scripts -->
<script defer src="${pageContext.request.contextPath}/JS/save.js"></script>
</body>
</html>