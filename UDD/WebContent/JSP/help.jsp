<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Help</title>
</head>
<body>
<jsp:include page="navbar.jsp" />

<div class="container p-5">
	<div class="row bg-light shadow border-info">
		<div class="col-sm-12 text-center">
			<h1>Frequently Asked Questions <i class="fas fa-question-circle"></i></h1>
		</div>
		<div class="col-sm-12 pt-3">
			<p><span class="font-weight-bold font-talic">Q. <span class="font-weight-bold font-italic text-danger">"Your connection is not private"</span> appears when I try to access the system, why is that?</span><br>
			   <span><span class="font-weight-bold">A.</span> This is normal because the web certificate used in the system is just self-signed(free). Look for advance and then click 
			    <span class="text-underline font-weight-bold">"Proceed to cpdogisapp (unsafe)"</span> to start using the system.</span>
			</p>
		</div>
		<div class="col-sm-12 pt-2">
			<p><span class="font-weight-bold font-talic">Q. What is the recommended Internet browser to use the system?</span><br>
			   <span><span class="font-weight-bold">A.</span> It is recommended to use Google Chrome as the default browser for the system.</span>
			</p>
		</div>
		<div class="col-sm-12 pt-2">
			<p><span class="font-weight-bold font-talic">Q. I already registered an account but I can't login, why is that?</span><br>
			   <span><span class="font-weight-bold">A.</span> Make sure that the username or password you entered match the one you used on registration. If you are really sure that the account details you have entered is correct,
			   then you need to have your account to be activated by your administrator.</span>
			</p>
		</div>
		<div class="col-sm-12 pt-2">
			<p><span class="font-weight-bold font-talic">Q. The layout/GUI is not appearing properly, is this a bug?</span><br>
			   <span><span class="font-weight-bold">A.</span> Our Locational Clearance Tracking System uses some of the latest components including <span class="font-weight-bold">Bootstrap 4 and HTML5</span> which older browsers may not display correctly. To solve this,
			   you need to download and install the latest version of Google Chrome.</span>
			</p>
		</div>
		<div class="col-sm-12 pt-2">
			<p><span class="font-weight-bold font-talic">Q. Can I use other browsers such as Internet Explorer, Mozilla Firefox, etc.?</span><br>
			   <span><span class="font-weight-bold">A.</span> To avoid problems, use Google Chrome because it was the browser used during testing period and components were fully tested on it.</span>
			</p>
		</div>
		<div class="col-sm-12 pt-2">
			<p><span class="font-weight-bold font-talic">Q. Web Camera is not working on CAMERAQR module?</span><br> 
			   <span><span class="font-weight-bold">A.</span> Make sure that you have entered <span class="font-weight-bold">https</span> instead of <span class="font-weight-bold">http</span> 
			   in the browser's url. This is very important because Google Chrome won't allow device viewing on non-secured connections.</span>
			</p>
		</div>
		<div class="col-sm-12 pt-2">
			<p><span class="font-weight-bold font-talic">Q. When I try to print LC Form/NOA Form, the printed form is blank and print preview is also displaying blank page. Why is that?</span><br> 
			   <span><span class="font-weight-bold">A.</span> Make sure that you are using the latest version of Google Chrome. If you are already on the latest version, try downloading
			   <span class="font-weight-bold">Google Chrome Canary</span> as alternative browser. Google Chrome update would sometimes messed up the scripts used in some sites hence the blank print preview.</span>
			</p>
		</div>
	</div>
</div>
</body>
</html>