<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Locational Entry</title>
</head>
<body>
<jsp:include page="navbar.jsp" />
<div class="container-fluid p-5">
	<div class="row">
		<div class="col">
	        <!--Form with header-->
	        <c:forEach items="${items}" var="items" varStatus="count">	
	        <form class="validatedForm" id="form" action="manageMp" method="post">
	            <div class="card border-info rounded-0">
	         		<div class="card-header p-0">
	                    <div class="bg-primary text-white text-center py-2">
	                        <h2><i class="fas fa-edit"></i>Receiving</h2>	
	                        <p class="m-0 font-italic">Save to Generate Transaction Number</p>    
	                        <a href="#bottom" id="hulk" class="btn btn-danger"><i class="fas fa-arrow-circle-down"></i>Go to the bottom of the page</a>                        
	                    </div>
	                </div>
	                <div class="card-body p-3">
	                <div class="form-row">
	                    	<div class="form-group col-sm-6">	
		                    	<label for="businessName">Business Name / Trade Name</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="businessName" id="businessName" value="<c:out value="${items.businessName}"/>" >   
		                        </div>	                                           
		                    </div>
		                   	<div class="form-group col-sm-6">	
		                    	<label for="taxPayerName">Taxpayer Name</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="taxPayerName" id="taxPayerName" value="<c:out value="${items.taxPayerName}"/>" >   
		                        </div>	                                           
		                    </div>              	                                 		                         	       
	                    </div>
	                    <div class="form-row">		               
		                    <div class="form-group col-sm-4">	
		                    	<label for="noOfEmployees">No of Employees</label><br>  		                  	  
		                    	<div class="input-group mb-2">
		                    		<label for="noOfEmployeesMale">Male</label>      	                          	                           
									<input type="text" class="form-control" name="noOfEmployeesMale" id="noOfEmployeesMale">  
									<label for="noOfEmployeesMale">Female</label>      	                          	                           
									<input type="text" class="form-control" name="noOfEmployeesFemale" id="noOfEmployeesFemale">   
		                        </div>	                                           
		                    </div>	
		                    <div class="form-group col-sm-2">	
		                    	<label for="sex">Sex</label><br>          
		                    	<div class="input-group mb-2">
		                    		<select name="sex" id="sex" class="form-control">
		                    			<option selected disabled>-----Select Sex-----</option>
		                    			<option value="Male">Male</option>
		                    			<option value="Female">Female</option>
		                    		</select>	                          	                           
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-sm-2">	
		                    	<label for="rightOverLand">Right Over Land</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<select class="form-control" name="rightOverLand" id="rightOverLand">
										<option selected value="<c:out value="${items.rightOverLand}"/>"><c:out value="${items.rightOverLand}"/></option>
			                    		<option disabled value="">-----Choose-----</option>
			                    		<option value="LEASE">LEASE</option>
			                    		<option value="OWNER">OWNER</option>
		                    		</select>      
		                        </div>	                                           
		                    </div>	 
		                    <div class="form-group col-sm-2">	
		                    	<label for="landRate">Land Rate</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="landRate" id="landRate" value="<c:out value="${items.landRate}"/>" >   
		                        </div>	                                           
		                    </div>
		                    <div class="form-group col-sm-2">	
		                    	<label for="capitalization">Capitalization</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="capitalization" id="capitalization" value="<c:out value="${items.capitalization}"/>" >   
		                        </div>	                                           
		                    </div> 	 	  			                          			                        		                         	       
	                    </div>
	                    <div class="form-row">		             	                  	                    
		                    <div class="form-group col-sm-6">	
		                    	<label for="address">Address</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="address" id="address" value="<c:out value="${items.address}"/>" >   
		                        </div>	                                           
		                    </div>	
		                    <div class="form-group col-sm-2">	
		                    	<label for="district">District</label><br>          
		                    	<div class="input-group mb-2">	                          	                           					
									<select class="form-control"name="district" id="district">
										<option selected value="<c:out value="${items.district}"/>"><c:out value="${items.district}"/></option>
										<option disabled> Select District</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
									</select>  
		                        </div>	                                           
		                    </div>	     	  
	                      	<div class="form-group col-sm-2">	
		                    	<label for="barangay">Barangay</label><br>          
		                    	<div class="input-group mb-2">	        
		                    		<input type="hidden" name="action" id="action" value="brgyDropdownLocational">                  	                           
									<select class="form-control" name="barangay" id="barangay">
										<option value="<c:out value="${items.barangay}"/>"><c:out value="${items.barangay}"/></option>
										<c:forEach items="${barangayList}" var="list">         
					                    	<option value="<c:out value="${list.barangay}" />"><c:out value="${list.barangay}" /></option>
				                        </c:forEach>
			                        </select> 
		                        </div>	                                           
		                    </div>	
		                    <div class="form-group col-sm-2">
	                    		<label for="pisc">Business Area</label><br>          
		                    	<div class="input-group mb-2">			                 
			                    	<input type="text" class="form-control" name="businessArea" id="businessArea" value="<c:out value="${items.lotArea }"/>"><!-- lot area because of term difference in UDD and ITDO -->		                    			                    	                          	                           								   
		                        </div>	 
	                    	</div>	     	                                  			                        		                         	       
	                    </div>	  
	                    <div class="form-row">
	                    	<div class="form-group col-sm-6">	
		                    	<label for="businessActivityMainTile1">Business Activity Maintile1</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="businessActivityMainTile1" id="businessActivityMainTile1" value="<c:out value="${items.businessActivityMainTile1}"/>" >   
		                        </div>	                                           
		                    </div>  
		                    <div class="form-group col-sm-6">	
		                    	<label for="businessActivitySubTile1">Business Activity Subtile1</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="businessActivitySubTile1" id="businessActivitySubTile1" value="<c:out value="${items.businessActivitySubTile1}"/>" >   
		                        </div>	                                           
		                    </div>   	 		                            	  			                                      		                     	       
	                    </div>		                                                                                  	                    
	                </div>	             	
	                <div class="card-header p-0">
	                    <div class="bg-primary text-white text-center py-2">
	                        <h2><i class="fas fa-edit"></i>Evaluation</h2>
	                        <p class="m-0 font-italic">Edit fields for evaluation</p>
	                        <a href="#top" id ="backToTopBtn" class="btn btn-danger"><i class="fas fa-arrow-circle-up"></i>Go to the top of the page</a><br>  
	                    </div>
	                </div>
	                <div class="card-body p-3">
	                    <!--Body-->	           
	                    <div class="form-row">
	                    	<div class="form-group col-sm-6">	
		                    	<label for="mpNo">MP No.</label><br>       
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="mpNo" id="mpNo" value="<c:out value="${items.mpNo}"/>" readonly>   
		                        </div>	                                           
		                    </div>	     
		                    <div class="form-group col-sm-6">	
		                    	<label for="cpdoNo">CPDO No.</label><br>  
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="cpdoNo" id="cpdoNo" value="<c:out value="${items.cpdoNo}"/>" readonly>   
		                        </div>	                                           
		                    </div>		                                                          	       
	                    </div>	 
	                    <div class="form-row">
	                    	<div class="form-group col-sm-4">	
		                    	<label for="dateApplied">Date Applied(year/month/day)</label><br>        
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="dateApplied" id="dateApplied" value="<c:out value="${items.dateApplied}"/>" readonly>   
		                        </div>	                                           
		                    </div>			
	                     	<div class="form-group col-sm-4">	
		                    	<label for="dateValid">Date Valid</label><br>        
		                    	<select class="form-control inputField" id="dateValid" name="dateValid" style="width:100%;" readonly>
		                    		<option></option>
	                            	<option selected disabled>-----Choose validity-----</option>
	                            	<option disabled value="1">1</option>
	                            	<option disabled value="3">3</option>
	                            </select>	                                           
		                    </div>		
	                    	<div class="form-group col-sm-4">	
		                    	<label for="evaluationOfFacts">Evaluation of Facts</label><br>       		                    	
		                        <select class="form-control inputField" id="evaluationOfFacts" name="evaluationOfFacts" style="width:100%;" readonly>
		                    		<option></option>
	                            	<option selected disabled>-----Choose Evaluation of Facts-----</option>
	                            	<option disabled value="business activity is permissble in the zone">business activity is permissble in the zone</option>
	                            	<option disabled value="business activity is conditional use in inst zone">business activity is conditional use in inst zone</option>
	                            </select>	                                                     
		                    </div>	     
	                    </div>
	                    <div class="form-row">	                  
		                    <div class="form-group col-sm-4">	
		                    	<label for="status">Status</label><br>         
		                    	<select class="form-control inputField" id="status" name="status" readonly>
		                    		<option selected value="<c:out value="${items.status}"/>"><c:out value="${items.status}"/></option>
	                            	<option disabled>-----Choose Status-----</option>
	                            	<option disabled value="APPROVED">APPROVED</option>
	                            	<option disabled value="DISAPPROVED">DISAPPROVED</option>
	                            	<option disabled value="FOR COMPLIANCE">FOR COMPLIANCE</option>
	                            </select>	                                          
		                    </div>		                
		                    <div class="form-group col-sm-4">	
		                    	<label for="zoningOfficial">Zoning Official</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="zoningOfficial" id="zoningOfficial" value="PEDRO P. RODRIGUEZ JR." readonly>   
		                        </div>	                                           
		                    </div>	 		                                  
		                    <div class="form-group col-sm-2">	
		                    	<label for="pisc">PISC</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="pisc" id="pisc" value="<c:out value="${items.pisc}"/>" readonly>   
		                        </div>	                                           
		                    </div>	    	      	 	 	                      	      	       
	                    </div>	  	       
	                    <div class="form-row">
		                    <div class="form-group col-sm-8">	
		                    	<label for="zoningClass">Zoning Class</label><br>          
		                    	<select class="form-control inputField" id="zoningClass" name="zoningClass" readonly>
		                    		<option selected value="<c:out value="${items.zoningClass}"/>"><c:out value="${items.zoningClass}"/></option>
	                            	<option disabled>-----Choose Zoning Class-----</option>
	                            	<option disabled value="LOW DENSITY RESIDENTIAL ZONE (R-1)">LOW DENSITY RESIDENTIAL ZONE (R-1)</option>	
	                            	<option disabled value="LOW DENSITY RESIDENTIAL SUB-ZONE (R-1-A)">LOW DENSITY RESIDENTIAL SUB-ZONE (R-1-A)</option>
	                            	<option disabled value="MEDIUM DENSITY RESIDENTIAL ZONE (R-2)">MEDIUM DENSITY RESIDENTIAL ZONE (R-2)</option>                            	
	                            	<option disabled value="MEDIUM DENSITY RESIDENTIAL SUB-ZONE (R-2-A)">MEDIUM DENSITY RESIDENTIAL SUB-ZONE (R-2-A)</option>
	                            	<option disabled value="HIGH DENSITY RESIDENTIAL ZONE (R-3)">HIGH DENSITY RESIDENTIAL ZONE (R-3)</option> 
	                            	<option disabled value="MINOR COMMERCIAL ZONE (C-1)">MINOR COMMERCIAL ZONE (C-1)</option> 
	                            	<option disabled value="MAJOR COMMERCIAL ZONE (C-2)">MAJOR COMMERCIAL ZONE (C-2)</option> 
	                            	<option disabled value="METROPOLITAN COMMERCIAL ZONE (C-3)">METROPOLITAN COMMERCIAL ZONE (C-3)</option> 
	                            	<option disabled value="LIGHT INTENSITY INDUSTRIAL ZONE (I-1)">LIGHT INTENSITY INDUSTRIAL ZONE (I-1)</option> 
	                            	<option disabled value="MEDIUM INTENSITY INDUSTRIAL ZONE (I-2)">MEDIUM INTENSITY INDUSTRIAL ZONE (I-2)</option> 
	                            	<option disabled value="SPECIAL URBAN DEVELOPMENT ZONE (SUDZ)">SPECIAL URBAN DEVELOPMENT ZONE (SUDZ)</option> 
	                            	<option disabled value="INSTITUTIONAL (INST)">INSTITUTIONAL (INST)</option> 
	                            	<option disabled value="SOCIALIZED HOUSING ZONE (SHZ)">SOCIALIZED HOUSING ZONE (SHZ)</option> 
	                            	<option disabled value="TRANSPORT AND UTILITY ZONE (TRU)">TRANSPORT AND UTILITY ZONE (TRU)</option>
	                            	<option disabled value="PARK AND RECREATIONAL (P&R)">PARK AND RECREATIONAL (P&R)</option>  
	                            	<option disabled value="CEMETERY (CEM)">CEMETERY (CEM)</option>  
	                            </select>	    	                                           
		                    </div>
		                    <div class="form-group col-sm-4">	
		                    	<label for="evaluation">Evaluation</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<select class="form-control inputField" id="evaluation" name="evaluation" readonly>
		                    		<option selected value="<c:out value="${items.evaluation}"/>"><c:out value="${items.evaluation}"/></option>
	                            	<option disabled>-----Choose Evaluation-----</option>
	                            	<option disabled value="PERMITTED">PERMITTED</option>	
	                            	<option disabled value="NOT PERMITTED">NOT PERMITTED</option>	
	                            	<option disabled value="FOR COMPLIANCE">FOR COMPLIANCE</option>
	                            	<option disabled value="FOR INSPECTION">FOR INSPECTION</option>
	                            </select>	   
		                        </div>	                                           
		                    </div>		                 		                  			                                          
	                    </div>	
		                <div class="form-row">
		                	<div class="form-group col-sm-4">
			                	<label for="verifier">Verifier</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="verifier" id="verifier" value="<c:out value="${items.verifier}"/>" readonly>   
		                        </div>	  
		                	</div>  
		                	<div class="form-group col-sm-8">
			                	<label for="verifier">Remarks</label><br>          
		                    	<div class="input-group mb-2">	                          	                           
									<input type="text" class="form-control" name="remarks" id="remarks" value="<c:out value="${items.remarks}"/>" readonly>   
		                        </div>	  
		                	</div>  
		                </div>          	                      
	                </div>	
                    <div class="text-center p-3">
                		<button type="submit" id="btnSave" value="Save" class="btn btn-primary btn-block rounded-0 py-2"><span><i class="fas fa-save"></i>Save</span></button>
                	</div>	         	                
	            </div>
	        </form>
	        </c:forEach>
	    </div>
	</div>
</div>

<!-- Scripts -->
<script defer src="${pageContext.request.contextPath}/JS/save.js"></script>	
<script defer src="${pageContext.request.contextPath}/JS/pages/barangayDropdown.js"></script>	
<script defer src="${pageContext.request.contextPath}/JS/backToTop.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/backToBottom.js"></script>	
</body>
</html>