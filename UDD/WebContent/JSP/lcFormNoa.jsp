<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>LC Form Noa</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/fontawesome-free-5.0.8/web-fonts-with-css/css/fontawesome-all.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/background.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/shadow.css">
<style>
body {
    font-family: "Times New Roman", Times, serif;
}
.arial-font{
	font-family: Arial, Helvetica, sans-serif;
}
td span{
	display:block; text-align:center; margin:0 auto;
}
.underline {
	text-decoration: underline;	
}
.border-3 {
    border-width:3px !important;
}
@media print {
    #back {
        display:none
    }
    body{
        background-image: none;
        margin-top: 3mm; 
	/*  margin-bottom: 10mm;  */
    }
	#top {
	    top: 0;
	    margin-bottom:10px;
	}	
	#bottom {
		border-top: 3px solid; 
	    bottom: 0;
	}
}
.container-imagetext {
    background-image:url(${pageContext.request.contextPath}/CSS/Images/logo/qc_bg.png);
    background-position:center;
    background-repeat: no-repeat;
    width:100%;
    height:100%;
}
</style>
</head>
<body>
<c:if test="${empty itemsTobeEvaluated}">
	<div class="container">
		<div class="row p-4">	
			<div class="col-md-3"></div>
			<div class="col-md-6" id="mpNotFound" >
				 <div class="card border-danger rounded-0 text-center">
				 	<div class="card-header p-0">
			 		  	<div class="bg-danger text-white text-center py-2">
		                       <h2><i class="fas fa-exclamation-triangle text-warning"></i>Warning!<i class="fas fa-exclamation-triangle text-warning"></i></h2>		                                          
		                   </div>
				 	</div>
				 	<div class="card-body p-3">
		               	<div class="text-center py-2">
		                       <h1 class="text-danger">Transaction Number not found or is not yet approved, ask evaluator for assistance! </h1>	
		                       <h5>Click <a href="${pageContext.request.contextPath}/JSP/lcFormSearch.jsp">here</a> to search again</h5>                                          
		                </div>
		         	</div>
				 </div>
		     </div>  
		     <div class="col-md-3"></div>     	
		</div>
	</div>
</c:if>
<c:forEach items="${itemsTobeEvaluated}" var="items" varStatus="count">	
<div class="container h-50 bg-white" id="areaToPrint">
		<div class="row bg-white pb-3 pl-3 pr-3 pt-1">
			<div class="col-sm-2">
				<img src="${pageContext.request.contextPath}/CSS/Images/logo/Q.C._Logo.png" style="width:130px"><br>
			</div>
			<div class="col-sm-8 text-center">
				<p class="arial-font">Republic of the Philippines<br><span class="font-weight-bold arial-font">QUEZON CITY</span><br>
				<span class="h4 font-italic font-weight-bold arial-font">CITY PLANNING AND DEVELOPMENT OFFICE</span><br>
				<span class="h4 font-italic font-weight-bold arial-font">ZONING ADMINISTRATION UNIT</span>
				</p>
			</div>
		</div>
		<div class="row bg-white p-3">
			<div class="col-sm-4">
				<span class="font-weight-bold arial-font">MP NO. </span> <span class="underline"><c:out value="${items.mpNo}"/></span>	<!-- Previous size width:130px; -->
			</div>
			<div class="col-sm-4 text-center">
				<span class="h5 arial-font font-weight-bold">NOTICE / ADVICE OF ACTION</span><br>
			</div>
		</div>
		<div class="row bg-white pl-3 pr-3 pb-2 pt-2">
			<div class="col-sm-4">
				<span class="font-weight-bold arial-font">DATE: </span><span class="underline"><c:out value="${items.dateIssued}"/></span>
			</div>
			<div class="col-sm-4 text-center"></div>
			<div class="col-sm-4 text-center">
				<span class="font-weight-bold arial-font">TRANSACTION NO. </span> <span class="underline"><c:out value="${items.transactionNumber}"/></span>
			</div>
		</div>
		<div class="row bg-white pb-3 pr-3 pl-3 pt-1">
			<div class="col-sm-12">
				<table style="width:100%;">
					<tbody>
						<tr>
							<td class="w-50 border border-dark"><small>BUSINESS ACTIVITY</small><br>
								<small><span class="font-weight-bold"><c:out value="${items.businessActivityMainTile1}"/><br>
									<c:out value="${items.businessActivitySubTile1}"/>
								</span></small></td>
							<td class="w-50 border border-dark"><small>BUSINESS NAME:</small><br>
								<small><span class="font-weight-bold">
									<c:out value="${items.businessName}"/>
								</span></small>
							</td>						
						</tr>
					</tbody>
				</table>
				<table style="width:100%;">
					<tbody>
						<tr>
							<td class="w-50 border border-dark border-top-0"><small>ADDRESS:</small><br>
								<small><span class="font-weight-bold"><c:out value="${items.address}"/><br></span></small>
							</td>
							<td class="w-25 border border-dark border-top-0"><small>BARANGAY:</small><br>
								<small><span class="font-weight-bold"><c:out value="${items.barangay}"/></span></small>
							</td>	
							<td class="w-25 border border-dark border-top-0"><small>DISTRICT:</small><br>
								<c:if test="${ items.district != '0'}">
									<small><span class="font-weight-bold"><c:out value="${items.district}"/></span></small>
								</c:if>
							</td>						
						</tr>
					</tbody>
				</table>
				<table style="width:100%;">	
					<tbody>
						<tr>
							<td class="w-75 border border-dark border-top-0"><small>ZONE CLASSIFICATION:</small><br>
								<small><span class="font-weight-bold"><c:out value="${items.zoningClass}"/><br></span></small>
							</td>
							<td class="w-25 border border-dark border-top-0"><small>BUSINESS AREA:</small><br>
								<c:if test="${items.businessArea != '0'}">
									<small><span class="font-weight-bold"><c:out value="${items.businessArea}"/></span></small>
								</c:if>
							</td>																		
						</tr>
					</tbody>
				</table>
				<table style="width:100%;">	
					<tbody>
						<tr>
							<td class="w-75 border border-dark border-top-0"><small>EVALUATION OF FACTS:</small><br>
								<small><span class="font-weight-bold"><c:out value="${items.evaluationOfFacts}"/><br></span></small>
							</td>													
						</tr>
						<tr>
							<td class="w-75 border border-dark border-top-0"><small>Remarks:</small><br>
								<small><span class="font-weight-bold"><c:out value="${items.noticeOfAction}"/><br></span></small>
							</td>													
						</tr>	
					</tbody>
				</table>
			</div>
		</div>
		<div class="row bg-white pt-5 pr-2"></div>
		<div class="row bg-white pl-2 pr-2 pb-2">
			<div class="col-sm-5 text-center">
			</div>
			<div class="col-sm-2 text-center">
			</div>
			<div class="col-sm-5 text-center">
				<p>
					<span class="font-weight-bold h5 arial-font">PEDRO P. RODRIGUEZ, JR.</span><br>
					<small>City Planning & Development Officer</small><br>
					<small>Zoning Administrator</small>
				</p>
			</div>
		</div>
		<div class="row bg-white p-1">
			<div class="col-sm-4 text-center">
				<small class="text-capitalize">// <c:out value="${sessionScope.nameOfUser}"></c:out></small>
			</div>
			<div class="col-sm-4 text-center">
			</div>
			<div class="col-sm-4 text-center">
				<small>QCG-CPDO-ZAU-QP-F09-V.01</small>				
			</div>
		</div>
</div>
<div class="container bg-white h-50">		
	<!-------------------------------------------------------------- bottom of form ------------------------------------------------------------------------------------->	
	<div id="bottom" class="row bg-white"></div>
	<div class="row bg-white pb-3 pl-3 pr-3 pt-2">
		<div class="col-sm-2">
			<img src="${pageContext.request.contextPath}/CSS/Images/logo/Q.C._Logo.png" style="width:130px"><br>
		</div>
		<div class="col-sm-8 text-center">
			<p class="arial-font">Republic of the Philippines<br><span class="font-weight-bold arial-font">QUEZON CITY</span><br>
			<span class="h4 font-italic font-weight-bold arial-font">CITY PLANNING AND DEVELOPMENT OFFICE</span><br>
			<span class="h4 font-italic font-weight-bold arial-font">ZONING ADMINISTRATION UNIT</span>
			</p>
		</div>
		<div class="col-sm-2">			
		</div>
	</div>
	<div class="row bg-white p-3">
		<div class="col-sm-4">
			<span class="font-weight-bold arial-font">MP NO. </span> <span class="underline"><c:out value="${items.mpNo}"/></span>	<!-- Previous size width:130px; -->
		</div>
		<div class="col-sm-4 text-center">
			<span class="h5 arial-font font-weight-bold">NOTICE / ADVICE OF ACTION</span><br>
		</div>
	</div>
	<div class="row bg-white pl-3 pr-3 pb-2 pt-2">
		<div class="col-sm-4">
			<span class="font-weight-bold arial-font">DATE: </span><span class="underline"><c:out value="${items.dateIssued}"/></span>
		</div>
		<div class="col-sm-4 text-center">
		</div>
		<div class="col-sm-4 text-center">
			<span class="font-weight-bold arial-font">TRANSACTION NO. </span> <span class="underline"><c:out value="${items.transactionNumber}"/></span>
		</div>
	</div>
	<div class="row bg-white pb-3 pr-3 pl-3 pt-1">
		<div class="col-sm-12">
			<table style="width:100%;">
				<tbody>
					<tr>
						<td class="w-50 border border-dark">
							<small>BUSINESS ACTIVITY</small><br>
							<small><span class="font-weight-bold"><c:out value="${items.businessActivityMainTile1}"/><br><c:out value="${items.businessActivitySubTile1}"/></span></small>
						</td>
						<td class="w-50 border border-dark"><small>BUSINESS NAME:</small><br>
							<small><span class="font-weight-bold"><c:out value="${items.businessName}"/></span></small>
						</td>						
					</tr>
				</tbody>
			</table>
			<table style="width:100%;">
				<tbody>
					<tr>
						<td class="w-50 border border-dark border-top-0">
							<small>ADDRESS:</small><br>
							<small><span class="font-weight-bold"><c:out value="${items.address}"/><br></span></small>
						</td>
						<td class="w-25 border border-dark border-top-0">
							<small>BARANGAY:</small><br>
							<small><span class="font-weight-bold"><c:out value="${items.barangay}"/></span></small>
						</td>	
						<td class="w-25 border border-dark border-top-0">
							<small>DISTRICT:</small><br>
							<c:if test="${ items.district != '0'}">
								<span class="font-weight-bold"><c:out value="${items.district}"/></span>
							</c:if>
						</td>						
					</tr>
				</tbody>
			</table>
			<table style="width:100%;">	
				<tbody>
					<tr>
						<td class="w-75 border border-dark border-top-0">
							<small>ZONE CLASSIFICATION:</small><br>
							<small><span class="font-weight-bold"><c:out value="${items.zoningClass}"/><br></span></small>
						</td>
						<td class="w-25 border border-dark border-top-0">
							<small>BUSINESS AREA:</small><br>
							<c:if test="${items.businessArea != '0'}">
								<small><span class="font-weight-bold"><c:out value="${items.businessArea}"/></span></small>
							</c:if>
						</td>																		
					</tr>
				</tbody>
			</table>
			<table style="width:100%;">	
				<tbody>
					<tr>
						<td class="border border-dark border-top-0">
							<small>EVALUATION OF FACTS:</small><br>
							<small><span class="font-weight-bold"><c:out value="${items.evaluationOfFacts}"/><br></span></small>
						</td>													
					</tr>
					<tr>
						<td class="w-75 border border-dark border-top-0">
							<small>Remarks:</small><br>
							<small><span class="font-weight-bold"><c:out value="${items.noticeOfAction}"/><br></span></small>
						</td>													
					</tr>	
				</tbody>
			</table>
		</div>
	</div>
	<div class="row bg-white pt-5 pr-2"></div>
	<div class="row bg-white pl-2 pr-2 pb-2">
		<div class="col-sm-5 text-center">
		</div>
		<div class="col-sm-2 text-center">
		</div>
		<div class="col-sm-5 text-center">
			<p>
				<span class="font-weight-bold h5 arial-font">PEDRO P. RODRIGUEZ, JR.</span><br>
				<small>City Planning & Development Officer</small><br>
				<small>Zoning Administrator</small>
			</p>
		</div>
	</div>
	<div class="row bg-white p-1">
		<div class="col-sm-4 text-center">
			<small class="text-capitalize">// <c:out value="${sessionScope.nameOfUser}"></c:out></small>
		</div>
		<div class="col-sm-4 text-center">
		</div>
		<div class="col-sm-4 text-center">
			<small>QCG-CPDO-ZAU-QP-F09-V.01</small>				
		</div>
	</div>
	<div class="row bg-white text-center" id="back">
		<c:if test="${not empty itemsTobeEvaluated}">	
			<div class="col text-center">	
				<span id="back"> Click <a href="${pageContext.request.contextPath}/JSP/lcFormSearch.jsp">here</a> to go back</span>			
				<button type="button" class="btn btn-primary btn-block" id="button-print"><span><i class="fas fa-print"></i></span>Print</button>
			</div>
		</c:if>
	</div>
</div>
</c:forEach>
<!-- scripts -->
<script defer src="${pageContext.request.contextPath}/JS/jquery-3.2.1/jquery-3.2.1.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/jquery-ui-1.12.1/jquery-ui.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/js/bootstrap.bundle.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/js/bootstrap.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/bounce.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/printForm.js"></script>
</body>
</html>