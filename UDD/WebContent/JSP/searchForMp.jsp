<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Search for MP</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/DataTables-1.10.18/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/MainPages/searchForMp.css">
</head>
<body>
<jsp:include page="navbar.jsp" />
<div class="container-fluid fill p-5 text-center">
	<div class="row bg-light one-hundred-height">
		<div class="col table-responsive">
			<table class="table table-bordered table-paginate table-hover" style="width:100%">
				<caption>Click row to edit</caption>
				<thead class="thead-dark">
				    <tr>
				      <th scope="col" hidden>#</th>
				      <th scope="col">#</th>
				      <th scope="col">CPDO NO</th>
				      <th scope="col">DATE APPLIED</th>
				      <th scope="col">MP NO</th>
				      <th scope="col">STATUS</th>
				      <th scope="col">DATE POSTED</th>
				      <th scope="col">TIME POSTED</th>
				      <th scope="col">ZONING CLASS</th>
				      <th scope="col">EVALUATION</th>
				      <th scope="col">VERIFIER</th>
				      <th scope="col">ZONING OFFICIAL</th>
				      <th scope="col">DATE PROCESSED</th>
				      <th scope="col">TIME PROCESSED</th>
				      <th scope="col">REMARKS</th>
				      <th scope="col">LOT AREA</th>
				      <th scope="col">ENCODED BY</th>
				      <th scope="col">PISC</th>
				      <th scope="col">RIGHT OVER LAND</th>
				      <th scope="col">LANDRATE</th>
				      <th scope="col">BUSINESS NAME</th>
				      <th scope="col">TAXPAYERS NAME</th>
				      <th scope="col">ADDRESS</th>
				      <th scope="col">NO OF EMPLOYEES</th>
				      <th scope="col">CAPITALIZATION</th>
				      <th scope="col">BUSINESS ACTIVITY MAINTILE1</th>
				      <th scope="col">BUSINESS ACTIVITY SUBTILE1</th>
				      <th scope="col">BUSINESS ACTIVITY MAINTILE2</th>
				      <th scope="col">BUSINESS ACTIVITY SUBTILE2</th>
				      <th scope="col">BUSINESS ACTIVITY MAINTILE3</th>
				      <th scope="col">BUSINESS ACTIVITY SUBTILE3</th>
				      <th scope="col">BUSINESS ACTIVITY MAINTILE4</th>
				      <th scope="col">BUSINESS ACTIVITY SUBTILE4</th>
				      <th scope="col">BUSINESS ACTIVITY MAINTILE5</th>
				      <th scope="col">BUSINESS ACTIVITY SUBTILE5</th>
				    </tr>
			    </thead>
			    <tbody>
					<c:forEach items="${searchedItems}" var="searched" varStatus="count">				
					    <tr class="clickable-row" data-href="manageMp?action=getTableRow&id=${searched.id}">
					      <td hidden><input type="hidden" value="${searched.id}" name="id"></td>
					      <th scope="row"><c:out value="${count.count}"/></th>
					      <td class="col-sm"><c:out value="${searched.cpdoNo}"/></td>
					      <td class="col-sm"><c:out value="${searched.dateApplied}"/></td>
					      <td class="col-sm"><c:out value="${searched.mpNo}"/></td>
					      <td class="col-sm"><c:out value="${searched.status}"/></td>
					      <td class="col-sm"><c:out value="${searched.datePosted}"/></td>
					      <td class="col-sm"><c:out value="${searched.timePosted}"/></td>
					      <td class="col-sm"><c:out value="${searched.zoningClass}"/></td>
					      <td class="col-sm"><c:out value="${searched.evaluation}"/></td>
					      <td class="col-sm"><c:out value="${searched.verifier}"/></td>
					      <td class="col-sm"><c:out value="${searched.zoningOfficial}"/></td>
					      <td class="col-sm"><c:out value="${searched.dateProcessed}"/></td>
					      <td class="col-sm"><c:out value="${searched.timeProcessed}"/></td>
					      <td class="col-sm"><c:out value="${searched.remarks}"/></td>
					      <td class="col-sm"><c:out value="${searched.lotArea}"/></td>
					      <td class="col-sm"><c:out value="${searched.encodedBy}"/></td>
					      <td class="col-sm"><c:out value="${searched.pisc}"/></td>
					      <td class="col-sm"><c:out value="${searched.rightOverLand}"/></td>
					      <td class="col-sm"><c:out value="${searched.landRate}"/></td>
					      <td class="col-sm"><c:out value="${searched.businessName}"/></td>
					      <td class="col-sm"><c:out value="${searched.taxPayerName}"/></td>
					      <td class="col-sm"><c:out value="${searched.address}"/></td>
					      <td class="col-sm"><c:out value="${searched.noOfEmployees}"/></td>
					      <td class="col-sm"><c:out value="${searched.capitalization}"/></td>
					      <td class="col-sm"><c:out value="${searched.businessActivityMainTile1}"/></td>
					      <td class="col-sm"><c:out value="${searched.businessActivitySubTile1}"/></td>
					      <td class="col-sm"><c:out value="${searched.businessActivityMainTile2}"/></td>
					      <td class="col-sm"><c:out value="${searched.businessActivitySubTile2}"/></td>
					      <td class="col-sm"><c:out value="${searched.businessActivityMainTile3}"/></td>
					      <td class="col-sm"><c:out value="${searched.businessActivitySubTile3}"/></td>
					      <td class="col-sm"><c:out value="${searched.businessActivityMainTile4}"/></td>
					      <td class="col-sm"><c:out value="${searched.businessActivitySubTile4}"/></td>
					      <td class="col-sm"><c:out value="${searched.businessActivityMainTile5}"/></td>
					      <td class="col-sm"><c:out value="${searched.businessActivitySubTile5}"/></td>
					    </tr>
					</c:forEach>
				</tbody>					 
			</table>
		</div>
	</div>
</div>
<!-- scripts -->
<script defer src="${pageContext.request.contextPath}/CSS/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/DataTables-1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/table/tablePaginate.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/table/tableClickable.js"></script>
</body>
</html>