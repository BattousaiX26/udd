<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style>
body {
    font-family: "Times New Roman", Times, serif;
}
.arial-font{
	font-family: Arial, Helvetica, sans-serif;
}
.underline {
	text-decoration: underline;	
}
.col-bordered:after {
  content: "";
  display: block;
  border-bottom: 1px solid #000000;
  margin: 0 15px;
}
@media print{
	body{
		background:none;
	}
	#button-print{
		display:none;
	}
}
</style>
<title>Order of Payment</title>
</head>
<body>
<jsp:include page="navbar.jsp" />
<div class="container p-3 w-50 h-50">
	<c:if test="${empty releasingRecords}">
		<div class="row p-4">	
			<div class="col-md-3"></div>
			<div class="col-md-6" id="mpNotFound" >
				 <div class="card border-danger rounded-0 text-center">
				 	<div class="card-header p-0">
			 		  	<div class="bg-danger text-white text-center py-2">
		                       <h2><i class="fas fa-exclamation-triangle text-warning"></i>Warning!<i class="fas fa-exclamation-triangle text-warning"></i></h2>		                                          
		                </div>
				 	</div>
				 	<div class="card-body p-3">
		               	<div class="text-center py-2">
		                       <h1 class="text-danger">TRANSACTION NUMBER NOT FOUND!</h1>	
		                       <h5>Click <a href="${pageContext.request.contextPath}/JSP/releasing.jsp">here</a> to search again</h5>                                          
		                   </div>
		               </div>
				 </div>
		     </div>  
		     <div class="col-md-3"></div>     	
		</div>
	</c:if>
	<c:forEach items="${releasingRecords}" var="items" varStatus="count">	
		<div class="row bg-light pt-2">
			<div class="col-sm-6 text-center"></div>
			<div class="col-sm-3 text-center"></div>
			<div class="col-sm-3 text-right border-dark border-bottom">
				<small>Transaction No. <c:out value="${items.transactionNumber }"/></small>
			</div>
		</div>
		<div class="row bg-light">
			<div class="col-sm-3 text-right">
				<img src="${pageContext.request.contextPath}/CSS/Images/logo/Q.C._Logo.png" style="width:130px"><br>				
			</div>
			<div class="col-sm-6 text-center">
				<p class="arial-font"><small>Republic of the Philippines</small><br><small class="arial-font">QUEZON CITY</small><br><small>Office of the Mayor</small><br>
					<small class="font-italic font-weight-bold arial-font pt-2">ZONING ADMINISTRATION UNIT</small><br>
					<small class="font-italic font-weight-bold arial-font underline">ORDER OF PAYMENT</small><br>
				</p>
			</div>
			<div class="col-sm-3">
			</div>
		</div>
		<div class="row bg-light">
			<div class="col-sm-12 ml-2">
				<small>To the City Treasurer:</small>
			</div>
		</div>
		<div class="row bg-light">
			<div class="col-sm-3 ml-2">
				<small>Please collect from Mr./Ms.</small>
			</div>
			<div class="col-sm-8 border-dark border-bottom">
				<small><c:out value="${items.taxPayerName}"/></small>
			</div>
		</div>
		<div class="row bg-light">
			<div class="col-sm-3 ml-2">
				<small>with address at</small>
			</div>
			<div class="col-sm-8 border-dark border-bottom">
				<small><c:out value="${items.address }"/>, <c:out value="${items.district }"/>, <c:out value="${items.barangay }"/></small>
			</div>
		</div>
		<div class="row bg-light pt-2">
			<div class="col-sm-11 ml-2">
				<small>The following fees indicated below is pursuant of ordinance SP-2502, S-2016 otherwise known as Comprehensive Zoning Ordinance of 2016</small>
			</div>
		</div>
		<div class="row bg-light pt-1">
			<div class="col-sm-7 ml-2">
				<small class="mr-2">FEES:</small>
				<div class="form-check form-check-inline">
		  			<input class="form-check-input" type="checkbox">
		  			<small class="form-check-label">RESIDENTIAL</small>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox">
					<small class="form-check-label">COMMERCIAL</small>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox">
					<small class="form-check-label">INDUSTRIAL</small>		
				</div>
				<div class="form-check form-check-inline ">
					<input class="form-check-input" type="checkbox">
					<small class="form-check-label">OTHERS</small>
				</div>
			</div>
			<div class="col-sm-2"></div>	
			<div class="col-sm-2 border-dark border-bottom"></div>	
		</div>
		<div class="row bg-light pt-1">
			<div class="col-sm-6 ml-2">
				<div class="form-check">
		  			<input class="form-check-input" type="checkbox">
		  			<small class="form-check-label">APPLICATION FEE</small>
				</div>
			</div>
			<div class="col-sm-3"></div>
			<div class="col-sm-2 text-center border-dark border-bottom"></div>
		</div>
		<div class="row bg-light pt-1">
			<div class="col-sm-6 ml-2">
				<div class="form-check">
		  			<input class="form-check-input" type="checkbox">
		  			<small class="form-check-label">VERIFICATION FEE</small>
				</div>
			</div>
			<div class="col-sm-3">		 		</div>
			<div class="col-sm-2 text-center border-dark border-bottom"></div>
		</div>
		<div class="row bg-light pt-1">
			<div class="col-sm-6 ml-2">
				<div class="form-check">
		  			<input class="form-check-input" type="checkbox">
		  			<small class="form-check-label">PROCESSING FEE</small>
				</div>
			</div>
			<div class="col-sm-3">		 		
			</div>
			<div class="col-sm-2 text-center">		
			</div>
		</div>
		<div class="row bg-light pt-1">
			<div class="col-sm-3 ml-2 text-right">
		  		<small>Total Floor Area of the Project</small>
			</div>
			<div class="col-sm-1 border-dark border-bottom text-left"></div>
			<div class="col-sm-2">	
				<small>sq.m X</small>
			</div>
			<div class="col-sm-2 border-dark border-bottom">
				<small>P</small>
			</div>
			<div class="col-sm-1 text-right"></div>
			<div class="col-sm-2 text-center border-dark border-bottom"></div>
		</div>
		<div class="row bg-light pt-1">
			<div class="col-sm-9 ml-2">
				<div class="form-check form-check-inline">
		  			<input class="form-check-input" type="checkbox">
		  			<small class="form-check-label">OTHERS</small>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox">
					<small class="form-check-label">Motion for Reconsideration</small>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="checkbox" checked>
					<small class="form-check-label">Certification - LC BUSINESS <c:out value="${items.dateValid }"/> YEAR(S)</small>		
				</div>
			</div>
			<div class="col-sm-2 text-center border-dark border-bottom"></div>
		</div>
		<div class="row bg-light pt-1">
			<div class="col-sm-2 ml-2">
				<small>Amount in words: </small>
			</div>
			<div class="col-sm-4 border-dark border-bottom">
				<small class="text-uppercase text-center font-weight-bold"><c:out value="${items.totalInWords }"/></small>
			</div>
			<div class="col-sm-3 text-right">
				<small>TOTAL</small>
			</div>
			<div class="col-sm-2 text-center border-dark border-bottom">
				<small>P </small><small class="font-weight-bold"><c:out value="${items.total }"></c:out></small>
			</div>
		</div>
		<div class="row bg-light p-2">
			<div class="col-sm-2"></div>
		</div>
		<div class="row bg-light pt-4">
			<div class="col-sm-3"></div>
			<div class="col-sm-3"></div>
			<div class="col-sm-2"></div>
			<div class="col-sm-4 text-center">
				<small class="font-weight-bold">PEDRO P. RODRIGUEZ, JR.</small><br>
				<small>City Planning and Development Officer</small><br>
				<small>Zoning Administrator</small>
			</div>
		</div>
		<div class="row bg-light pt-3">
			<div class="col-sm-3"></div>
			<div class="col-sm-3"></div>
			<div class="col-sm-2"></div>
			<div class="col-sm-4 text-center">
				<small>QCG-CPDO-ZAU-QP-F15-V.01</small>
			</div>
		</div>
		<div id="button-print" class="row bg-light p-2">
			<button type="button" class="btn btn-primary btn-block" id="button-print"><span><i class="fas fa-print"></i></span>Print</button>
		</div>
	</c:forEach>
</div>

<script defer src="${pageContext.request.contextPath}/JS/printForm.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/bounce.js"></script>
</body>
</html>