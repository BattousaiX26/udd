<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style>
*{
    padding: 0;
    margin: 0;
}

html {
  position: relative;
  min-height: 100%;
}
body {
  /* Margin bottom by footer height */
  margin-bottom: 250px;
}
.footer {
  position: absolute;
  bottom: 0;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 160px;
}
</style>
<title>Insert title here</title>
</head>
<body >
<footer class="text-white footer">
    <div class="container-fluid py-3 bg-primary ">
        <div class="row">
            <div class="col-md-5">        
                <h4 class="text-uppercase">City Planning Development Department <img src="${pageContext.request.contextPath}/CSS/Images/logo/qclogo1.png" style="width:90px;"></img></h4>                            
            </div>     
            <div class="col-md-4 text-center">
            	<span class="h4">1 Timothy 4:10 <i class="fas fa-bible"></i> (NIV)</span><br>
				That is why we labor and strive, because we have put our hope in the living God,<br> who is the Savior of all people, and especially of those who believe.
            </div>   
            <div class="col-md-3 text-right">
            	<h3 class="text-uppercase">Developed by PDIU <br></h3>
            	<h5>Powered by <img src="${pageContext.request.contextPath}/CSS/Images/logo/javaicon.jpg" width="35" height="35"> 	     
	            	<img src="${pageContext.request.contextPath}/CSS/Images/logo/jquery_logo.png" width="35" height="35">
	            	<img src="${pageContext.request.contextPath}/CSS/Images/logo/ajax_logo.jpg" width="35" height="35">
	            	<img src="${pageContext.request.contextPath}/CSS/Images/logo/bootstrap.jpg" width="35" height="35">
            	</h5>
            </div>          	
        </div>
        <div class="row">
        	<div class="col-md-8">
        		Quezon City Hall, Elliptical Road, Brgy. Central, Diliman, Quezon City <span><i class="fas fa-city"></i></span>
        		<br>+63 2 988-4242 <span><i class="fas fa-phone"></i></span>
        	</div>
        	<div class="col-md-4 small align-self-end text-right">�2018, BattousaiX26</div>
    	</div>
	</div>
</footer>

</body>
</html>