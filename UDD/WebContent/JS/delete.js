$(document).ready(function(){
    $('.delete_alert').on('click', function(event){
        var element = $(this);
        event.preventDefault();
        bootbox.confirm( {
        	message: "Do you want to delete this account? This cannot be undone.",
        	backdrop: true,
        	buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel',
                    className: 'btn-danger'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }                
            },
            callback: function(result){
	            if (result) {
	                 //include the href duplication link here?;
	                 var path = element.attr("href");                
	                 window.location = path;                                                     
	            }
            }
        });
    });
});