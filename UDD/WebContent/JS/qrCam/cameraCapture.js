var video = document.getElementById('videoElement'); 
var canvas = document.getElementById('canvasID'); 
var context = canvas.getContext('2d');
window.URL = window.URL || window.webkitURL; navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

$('#btnCapture').on('click', function(event){
	context.drawImage(video, 0, 0, canvas.width, canvas.height); 
	var imageData = canvas.toDataURL(); 

	$.ajax({
		type: "POST",
	    url: "/UDD/JSP/qrRead",
	    data: imageData,
	    dataType: "json",
	    success: function(data) {
	    	if(data!=null){
	    		console.log(data);
	    		document.getElementById('content').style.display = 'block';
	    		$("#content").append(data); 
	    	}
	    },error: function(data){
	    	bootbox.alert({
    			message: "Unable to read QR Code, QR image needs to be on the center of camera!",
    			backdrop: true,
    			callback: function() {
    				 window.location= "/UDD/JSP/cameraQr.jsp";
    			}
    		});
	    }
	});
});




