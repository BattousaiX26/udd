$(document).ready(function(){
	var $action = $('#action'), $inputCriteria = $('#inputCriteria');
	$action.change(function () {
	    if ($action.val() == 'generateFromMpDigit') {
	        $inputCriteria.removeAttr('disabled');
	    }else if($action.val() == 'generateByYearAdded') {
	        $inputCriteria.removeAttr('disabled');
	    }
	    else {
	        $inputCriteria.attr('disabled', 'disabled').val('');
	    }
	}).trigger('change'); // added trigger to calculate initial state
});
