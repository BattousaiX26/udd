$(document).ready(function(){
	 $("#mpNotFound").effect("bounce", { times:15 }, 5000);
});

$('#btnSave').on('click', function(event){
    var datastring = $("#form").serialize();
    event.preventDefault();
    bootbox.confirm( {
    	  message: "Are you sure you want to submit?",
    	  backdrop: true,
    	  buttons: {
              cancel: {
                  label: '<i class="fa fa-times"></i> Cancel',
                  className: 'btn-danger'
              },
              confirm: {
                  label: '<i class="fa fa-check"></i> Confirm'
              }
          },
    	  callback: function(result){
    		  if (result) {
    				$.ajax({
    				    type: "POST",
    				    url: "/UDD/JSP/evaluate",
    				    data: datastring,
    				    success: function(data) {
    			
    				    	if(data=='Saved'){
    				    		 $("#saveStatus").html('<p class="alert alert-success">Evaluation was saved successfully! Click <a href="/UDD/JSP/evaluation.jsp">here</a> to evaluate more records</p>');   
    				    	}else if(data=='NotSaved'){
    				    		 $("#saveStatus").html('<p class="alert alert-danger">Unable to save, contact PDIU!</p>');   
    				    	}
    				    },
    				    error: function(data){
    						bootbox.alert({
    							message: "Unable to reach server!",
    							backdrop: true,
    							callback: function() {
    								 window.location= "/UDD/JSP/evaluationSearched.jsp";
    							}
    						});
    				    }
    				});
    	      }
    	  }
    });
});