$(function () {
	    $(".button-print").click(function () {
	        var contents = $("#areaToPrint").html();
	        var contents2 = $("#clientHeader").html();
	        var frame1 = $('<iframe />');
	        var receiver = $("#nameOfReceiver").html();
	        frame1[0].name = "frame1";
	        frame1.css({ "position": "absolute", "top": "-1000px" });
	        $("body").append(frame1);
	        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
	        frameDoc.document.open();
	        //Create a new HTML document.
	        frameDoc.document.write('<html><head>');
	        frameDoc.document.write('</head><body>');
	        //Append the external CSS file.
	        //frameDoc.document.write('<link href="/UDD/CSS/bootstrap-4.0.0-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />');
	        frameDoc.document.write('<link href="/UDD/CSS/Printing/printing.css" rel="stylesheet" type="text/css" media="print"/> ');
	        //Append the DIV contents.
	        frameDoc.document.write(contents2);
	        frameDoc.document.write(contents);
	        frameDoc.document.write('<p align="center">Reminders</p>');
	        frameDoc.document.write('<p>1. Only REGISTERED/RIGHTFUL OWNER or his DESIGNATED REPRESENTATIVE with notarized and legally valid authorization will be entertained.</p>');
	        frameDoc.document.write('<p>2. Present this claim stub together with your I.D. during transaction and I.D. of owner.</p>');
	        frameDoc.document.write('<p>3. FOLLOW-UP AND FIXING BY EMPLOYEES OF THE QUEZON CITY GOVERNMENT IS PROHIBITED.</p>');
	        frameDoc.document.write('<p>4. Please follow up 3-5 working days<br>Tel No. 988-4242 LOC. 1005</p>');
	        frameDoc.document.write('<br></br>');
	        frameDoc.document.write('<p align="right">',receiver,'</p>');
	        frameDoc.document.write('</body></html>');
	        frameDoc.document.close();
	        setTimeout(function () {
	            window.frames["frame1"].focus();
	            window.frames["frame1"].print();
	            frame1.remove();
	        }, 1000);
	    });
	});