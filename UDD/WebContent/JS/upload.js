$body = $("body");

$('#btn-upload').on('click', function(event) {
	$body.addClass("loading");
        $('#upload-form').ajaxForm({
	        success: function(msg) {
	        	console.log(msg);
	        	$body.removeClass("loading");
	        	if(msg=='successful'){	     
	        		bootbox.alert("Excel file has been uploaded and saved successfully!");             		 
	        	}else if(msg=='failed'){
	        		$("#upload-error").text("Couldn't upload file!");
	        	} 	        	
	        },
        error: function(msg) {
        	console.log(msg);
            $("#upload-error").text("Server returned an unexpected response!");
        }
    });
});