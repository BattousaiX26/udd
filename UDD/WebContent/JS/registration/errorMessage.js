$(document).ready(function(){
    $('#btnRegister').on('click', function(event){
    	var err = '';
		$('.form-control').each(function(){
			if($(this).val() == ''){
				var name = $(this).attr('name');
				err += name+" is required <br>";
			}
		});
		bootbox.alert({
			message: err,
			backdrop: true,
			callback: function() {
				 window.location= "/UDD/registration.jsp";
			}
		});
    });
});
$('#btnRegister').on('click', function(event){
	
	var datastring = $("#form").serialize();//get all parameters from form
	
	if(datastring.indexOf('=&') > -1){
		event.preventDefault();
	}else{
		$.ajax({
		    type: "POST",
		    url: "register",
		    data: datastring,
		    success: function(data) {
	
		    	if(data=='userExisting'){
		    		 $("#registrationStatus").html('<div class="alert alert-danger">User already in use!</div>');   
		    	}else if(data=='registrationSuccessful'){
		    		 $("#registrationStatus").html('<div class="alert alert-success">Registered Successfully!</div>');   
		    	}else if(data == 'registrationFailed'){
		    		 $("#registrationStatus").html('<div class="alert alert-danger">Unable to Register! Please check the network!</div>'); 
		    	}
		    },
		    error: function(data){
				bootbox.alert({
					message: "Unable to reach server!",
					backdrop: true,
					callback: function() {
						 window.location= "/UDD/registration.jsp";
					}
				});
		    }
		});
	}
});
