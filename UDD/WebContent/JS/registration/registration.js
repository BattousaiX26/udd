function check() {
    if(document.getElementById('password').value === document.getElementById('confirm_password').value) {
        document.getElementById('message').innerHTML = "Passwords match!";
        var sheet = document.createElement('style')
        sheet.innerHTML = "div {color:green;}";
        document.body.appendChild(sheet);
    } else {
        document.getElementById('message').innerHTML = "Passwords don't match!";
        var sheet = document.createElement('style')
        sheet.innerHTML = "div {color:red;}";
        document.body.appendChild(sheet);
    }
}
$("#section").keyup(function(event) {
    if (event.keyCode === 13) {
        $("#btnRegister").click();
    }
});