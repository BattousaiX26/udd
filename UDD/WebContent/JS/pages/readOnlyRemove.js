$(document).ready(function(){

	 $('#remarks').on("change", function() {  

	     var remarks = $(this).val();
	     
	     if(remarks == "OTHERS"){
	         $("#otherRemarks").attr("readonly", false); 
	     }
	     else{ 
	         $("#otherRemarks").attr("readonly", true); 
	     }
   });
	 $('#remarks2').on("change", function() {  

	     var remarks = $(this).val();
	     
	     if(remarks == "OTHERS"){
	         $("#otherRemarks2").attr("readonly", false); 
	     }
	     else{ 
	         $("#otherRemarks2").attr("readonly", true); 
	     }
   });
	 $('#remarks3').on("change", function() {  

	     var remarks = $(this).val();
	     
	     if(remarks == "OTHERS"){
	         $("#otherRemarks3").attr("readonly", false); 
	     }
	     else{ 
	         $("#otherRemarks3").attr("readonly", true); 
	     }
   });
	 $('#noticeOfAction').on("change", function() {  

	     var noticeOfAction = $(this).val();
	     
	     if(noticeOfAction == "OTHERS"){
	         $("#otherNoticeOfAction").attr("readonly", false); 
	     }
	     else{ 
	         $("#otherNoticeOfAction").attr("readonly", true); 
	     }
   });
});