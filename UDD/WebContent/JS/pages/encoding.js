$('form').submit(function(e) {
    var currentForm = this;
    e.preventDefault();
    bootbox.confirm( {
    	  message: "Are you sure you want to submit?",
    	  backdrop: true,
    	  buttons: {
              cancel: {
                  label: '<i class="fa fa-times"></i> Cancel',
                  className: 'btn-danger'
              },
              confirm: {
                  label: '<i class="fa fa-check"></i> Confirm'
              }
          },
    	  callback: function(result){
    		  if (result) {
    	            currentForm.submit();
    	      }
    	  }
    });
});