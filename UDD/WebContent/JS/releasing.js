$('#btnSave').on('click', function(event){
    var datastring = $("#form").serialize();
    var sealNo = $('#sealNo').val();
    var receiptNo = $('#receiptNo').val();
    event.preventDefault();
    
    if ( sealNo == "" || receiptNo == ""){
    	bootbox.alert({
			message: "Please make sure that Seal Number and Receipt Number are filled up!",
			backdrop: true,
		});
    }else{
    	  bootbox.confirm( {
        	  message: "Are you sure you want to submit?",
        	  backdrop: true,
        	  buttons: {
                  cancel: {
                      label: '<i class="fa fa-times"></i> Cancel',
                      className: 'btn-danger'
                  },
                  confirm: {
                      label: '<i class="fa fa-check"></i> Confirm'
                  }
              },
        	  callback: function(result){
        		  if (result) {
        				$.ajax({
        				    type: "POST",
        				    url: "/UDD/JSP/releasing",
        				    data: datastring,
        				    success: function(data) {
        			
        				    	if(data=='Saved'){
        				    		 $("#saveStatus").html('<p class="alert alert-success">Saved successfully!</p>');   
        				    	}else if(data=='NotSaved'){
        				    		 $("#saveStatus").html('<p class="alert alert-danger">Unable to save, contact PDIU!</p>');   
        				    	}
        				    },
        				    error: function(data){
        						bootbox.alert({
        							message: "Unable to reach server!",
        							backdrop: true,
        							callback: function() {
        								 window.location= "/UDD/JSP/releasing.jsp";
        							}
        						});
        				    }
        				});
        	      }
        	  }
        });
    }
});