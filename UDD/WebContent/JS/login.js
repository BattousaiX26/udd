
$(function() {
	  $("#username").focus();
});

$("#password").keyup(function(event) {
    if (event.keyCode === 13) {
        $("#btnLogin").click();
    }
});

$body = $("body");

$('#btnLogin').on('click', function(event){
	
	$body.addClass("loading");
	var datastring = $("#loginForm").serialize();
	
	$.ajax({
	    type: "POST",
	    url: "login",
	    data: datastring,
	    dataType: "json",
	    success: function(data) {
	    	console.log(data);
	    		    
	    	if(data == 'receiving'){
	    		window.location = "JSP/receiving.jsp";
	    	}else if(data == 'evaluation'){
	    		window.location = "JSP/evaluation.jsp";
	    	}else if(data == 'releasing'){
	    		window.location = "JSP/releasing.jsp";
	    	}else if(data == 'printing'){
	    		window.location = "JSP/lcFormSearch.jsp";
	    	}else if(data == 'finalEvaluation'){
	    		window.location = "JSP/evaluation.jsp";
	    	}
	    	else if(data == 'loginFailed'){	 
	    		document.getElementById('loginAlert').style.display = 'block';
	    		$("#loginAlert").html('<p class="alert alert-danger">Invalid account or not yet activated!</p> ');
	    		$body.removeClass("loading"); 
	    	}    	
	    },error: function(data){
	    	console.log(data);
	    	bootbox.alert({
    			message: "Cannot connect to the server! Please contact PDIU!.",
    			backdrop: true,
    			callback: function() {
    				 window.location= "/UDD/login.jsp";
    			}
    		});
	    }
	});
});
