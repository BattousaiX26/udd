$(document).ready(function() {
    var table = $('.table-paginate').DataTable({
    	  dom: 
	    	  "<'row'<'col-sm-3'l><'col-sm-6 text-center'B><'col-sm-3'f>>" +
	    	  "<'row'<'col-sm-12'tr>>" +
	    	  "<'row'<'col-sm-5'i><'col-sm-7'p>>", 
          buttons: [
        	  {extend: 'copyHtml5', className: 'btn btn-primary btn-lg', text:'Copy to Clipboard <i class="fas fa-copy"></i>'},
        	  {extend: 'excelHtml5', className: 'btn btn-success btn-lg',  text:'Convert to Excel <i class="fas fa-file-excel"></i>'},
        	  {extend: 'csvHtml5', className: 'btn btn-info btn-lg', text:'Convert to CSV <i class="fas fa-file-csv"></i>'},
          ],                 	
    });
    table.buttons().container()
    	.appendTo( $('#example_wrapper .col-sm-6:eq(0) ', table.table().container() ) );
} );