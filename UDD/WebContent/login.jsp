<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- css -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/fontawesome-free-5.5.0-web/css/all.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/CSS/LoginCss/login.css">

<title>UDD Login</title>
</head>
<body>
<br>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-12 col-md-8 col-lg-6 pb-5">
			<form id="loginForm">
				<div class="card border-info rounded-0 shadow">
                	<div class="card-body p-3">
                	 	<img class="card-img-top w-50 p-1 mx-auto d-block" src="${pageContext.request.contextPath}/CSS/Images/logo/Q.C._Logo.png">
                		<h2 align="center" class="text-italic">LOCATIONAL CLEARANCE</h2>
                		<h3 align="center" class="text-italic">TRACKING SYSTEM</h3>
                		<div class="form-group">
	                        <div class="input-group mb-2">
	                            <div class="input-group-prepend">
	                                <div class="input-group-text"><i class="fa fa-user text-primary"></i></div>
	                            </div>
	                            <input type="text" class="form-control" id="username" name="username" placeholder="Username" required>
	                        </div>
                    	</div>
                    	<div class="form-group">
                        	<div class="input-group mb-2">
	                            <div class="input-group-prepend">
	                                <div class="input-group-text"><i class="fas fa-key text-primary"></i></div>
	                            </div>
                            	<input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                        	</div>
                    	</div>      
                    	<div class="text-center">
                    		<div id="loginAlert" style="display:none"></div>
                    		<a href="${pageContext.request.contextPath}/registration.jsp">Register Here!</a>
                    	</div>                 
                    	<div class="text-center p-1">
	                        <button type="button" value="Login" id="btnLogin" class="btn btn-primary btn-block rounded-0 py-2 border border-danger"><i class="fas fa-sign-in-alt"></i>Login</button>
	                    </div>
                	</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal"><!-- Place at bottom of page --></div>
<jsp:include page="footer.jsp" />
<!-- script -->
<script defer src="${pageContext.request.contextPath}/JS/jquery-3.2.1/jquery-3.2.1.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/js/bootstrap.bundle.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/bootstrap-4.0.0-dist/js/bootstrap.min.js"></script>
<script defer src="${pageContext.request.contextPath}/JS//bootbox/bootbox.min.js"></script>
<script defer src="${pageContext.request.contextPath}/CSS/fontawesome-free-5.5.0-web/js/all.js"></script>
<script defer src="${pageContext.request.contextPath}/JS/login.js"></script>
</body>
</html>