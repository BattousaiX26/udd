package com.filter.web.java;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class CacheFilter
 */
@WebFilter("/JSP/*")
public class CacheFilter implements Filter {

	@Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        if(session.getAttribute("nameOfUser")==null) {
        	response.sendRedirect(request.getContextPath() + "/login.jsp");
        }else {
        	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        	response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        	response.setHeader("Expires", "0"); // Proxies.
        	filterChain.doFilter(request, response);    
        }
    }

    @Override
    public void destroy() {
    }
}
