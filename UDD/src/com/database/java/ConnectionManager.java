package com.database.java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


public class ConnectionManager {
	private static String DB_CONNECTION = "jdbc:sqlserver://CPDOGISAPP;databaseName = UDD";
	//private static String DB_CONNECTION = "jdbc:sqlserver://CPDOGISAPP;databaseName = UDD_TEST";
	//private static String DB_CONNECTION = "jdbc:sqlserver://CPDOGISAPP;databaseName = UDD_OFFICIAL_DB";
	private static String DB_USER = "sa";
	private static String DB_PASSWORD = "development";
	final static Logger logger = LogManager.getLogger(ConnectionManager.class.getName());
	public Connection getConnection() throws SQLException {  	
		Connection con=null;
        try{  	      	          
        	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");  
            con=DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);  
        }catch(Exception e){
        	logger.fatal("Unable to get Database Connection using ConnectionManage" + e);
        }  
        return con;  
	}
}
