package com.entities.java;

public class Releasing {
	private String mpNumber;
	private String transactionNumber;
	private String businessName;
	private String taxPayerName;
	private String receiptNumber;
	private String amount;
	private String sealNumber;
	private int id;
	private String dateEncoded;
	private String applicationStatus;
	private String address;
	private int district;
	private String barangay;
	private String total;
	private String totalInWords;
	private String dateValid;
	
	public String getMpNumber() {
		return mpNumber;
	}
	public void setMpNumber(String mpNumber) {
		this.mpNumber = mpNumber;
	}
	public String getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getSealNumber() {
		return sealNumber;
	}
	public void setSealNumber(String sealNumber) {
		this.sealNumber = sealNumber;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDateEncoded() {
		return dateEncoded;
	}
	public void setDateEncoded(String dateEncoded) {
		this.dateEncoded = dateEncoded;
	}
	public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public String getTaxPayerName() {
		return taxPayerName;
	}
	public void setTaxPayerName(String taxPayerName) {
		this.taxPayerName = taxPayerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getDistrict() {
		return district;
	}
	public void setDistrict(int district) {
		this.district = district;
	}
	public String getBarangay() {
		return barangay;
	}
	public void setBarangay(String barangay) {
		this.barangay = barangay;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getTotalInWords() {
		return totalInWords;
	}
	public void setTotalInWords(String totalInWords) {
		this.totalInWords = totalInWords;
	}
	public String getDateValid() {
		return dateValid;
	}
	public void setDateValid(String dateValid) {
		this.dateValid = dateValid;
	}
}
