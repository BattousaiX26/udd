package com.entities.java;

public class LocationalClearance {
	private int id;
	private String cpdoNo;
	private String dateApplied;
	private String dateIssued;
	private String validUntil;
	private int dateValid;
	private int noOfEmployeesMale;
	private int noOfEmployeesFemale;
	private String mpNo;
	private String mpNoSecond;
	private String status;
	private String datePosted;
	private String timePosted;
	private String zoningClass;
	private String evaluation;
	private String verifier;
	private String zoningOfficial;
	private String dateProcessed;
	private String timeProcessed;
	private double lotArea;
	private double businessArea;
	private String encodedBy;
	private String pisc;
	private String rightOverLand;
	private String landRate;
	private String businessName;
	private String taxPayerName;
	private String address;
	private int noOfEmployees;
	private String capitalization;
	private String businessActivityMainTile1;
	private String businessActivitySubTile1;
	private String businessActivityMainTile2;
	private String businessActivitySubTile2;
	private String businessActivityMainTile3;
	private String businessActivitySubTile3;
	private String businessActivityMainTile4;
	private String businessActivitySubTile4;
	private String businessActivityMainTile5;
	private String businessActivitySubTile5;
	private String searchedItem;
	private String barangay;
	private int district;
	private String sex;
	private String transactionNumber;
	private String evaluationOfFacts;
	private String tctNo;
	private String remarks;
	private String otherRemarks;
	private String remarks2;
	private String otherRemarks2;
	private String remarks3;
	private String otherRemarks3;
	private String noticeOfAction;
	private String otherNoticeOfAction;
	private byte[] qrCode;
	private String imageQrCode;
	private double total;
	private String totalInWords;
	private String applicationStatus;
	private String yearAdded;
	private String approvedBy;
	private String approvedForPrinting;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCpdoNo() {
		return cpdoNo;
	}
	public void setCpdoNo(String cpdoNo) {
		this.cpdoNo = cpdoNo;
	}
	public String getDateApplied() {
		return dateApplied;
	}
	public void setDateApplied(String dateApplied) {
		this.dateApplied = dateApplied;
	}
	public String getDateIssued() {
		return dateIssued;
	}
	public void setDateIssued(String dateIssued) {
		this.dateIssued = dateIssued;
	}
	public String getValidUntil() {
		return validUntil;
	}
	public void setValidUntil(String validUntil) {
		this.validUntil = validUntil;
	}
	public String getMpNo() {
		return mpNo;
	}
	public void setMpNo(String mpNo) {
		this.mpNo = mpNo;
	}
	public String getMpNoSecond() {
		return mpNoSecond;
	}
	public void setMpNoSecond(String mpNoSecond) {
		this.mpNoSecond = mpNoSecond;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDatePosted() {
		return datePosted;
	}
	public void setDatePosted(String datePosted) {
		this.datePosted = datePosted;
	}
	public String getTimePosted() {
		return timePosted;
	}
	public void setTimePosted(String timePosted) {
		this.timePosted = timePosted;
	}
	public String getZoningClass() {
		return zoningClass;
	}
	public void setZoningClass(String zoningClass) {
		this.zoningClass = zoningClass;
	}
	public String getEvaluation() {
		return evaluation;
	}
	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}
	public String getVerifier() {
		return verifier;
	}
	public void setVerifier(String verifier) {
		this.verifier = verifier;
	}
	public String getZoningOfficial() {
		return zoningOfficial;
	}
	public void setZoningOfficial(String zoningOfficial) {
		this.zoningOfficial = zoningOfficial;
	}
	public String getDateProcessed() {
		return dateProcessed;
	}
	public void setDateProcessed(String dateProcessed) {
		this.dateProcessed = dateProcessed;
	}
	public String getTimeProcessed() {
		return timeProcessed;
	}
	public void setTimeProcessed(String timeProcessed) {
		this.timeProcessed = timeProcessed;
	}
	
	public double getLotArea() {
		return lotArea;
	}
	public void setLotArea(double lotArea) {
		this.lotArea = lotArea;
	}
	public String getEncodedBy() {
		return encodedBy;
	}
	public void setEncodedBy(String encodedBy) {
		this.encodedBy = encodedBy;
	}
	public String getPisc() {
		return pisc;
	}
	public void setPisc(String pisc) {
		this.pisc = pisc;
	}
	public String getRightOverLand() {
		return rightOverLand;
	}
	public void setRightOverLand(String rightOverLand) {
		this.rightOverLand = rightOverLand;
	}
	public String getLandRate() {
		return landRate;
	}
	public void setLandRate(String landRate) {
		this.landRate = landRate;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getTaxPayerName() {
		return taxPayerName;
	}
	public void setTaxPayerName(String taxPayerName) {
		this.taxPayerName = taxPayerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getNoOfEmployees() {
		return noOfEmployees;
	}
	public void setNoOfEmployees(int noOfEmployees) {
		this.noOfEmployees = noOfEmployees;
	}
	
	public String getCapitalization() {
		return capitalization;
	}
	public void setCapitalization(String capitalization) {
		this.capitalization = capitalization;
	}
	public String getBusinessActivityMainTile1() {
		return businessActivityMainTile1;
	}
	public void setBusinessActivityMainTile1(String businessActivityMainTile1) {
		this.businessActivityMainTile1 = businessActivityMainTile1;
	}
	public String getBusinessActivitySubTile1() {
		return businessActivitySubTile1;
	}
	public void setBusinessActivitySubTile1(String businessActivitySubTile1) {
		this.businessActivitySubTile1 = businessActivitySubTile1;
	}
	public String getBusinessActivityMainTile2() {
		return businessActivityMainTile2;
	}
	public void setBusinessActivityMainTile2(String businessActivityMainTile2) {
		this.businessActivityMainTile2 = businessActivityMainTile2;
	}
	public String getBusinessActivitySubTile2() {
		return businessActivitySubTile2;
	}
	public void setBusinessActivitySubTile2(String businessActivitySubTile2) {
		this.businessActivitySubTile2 = businessActivitySubTile2;
	}
	public String getBusinessActivityMainTile3() {
		return businessActivityMainTile3;
	}
	public void setBusinessActivityMainTile3(String businessActivityMainTile3) {
		this.businessActivityMainTile3 = businessActivityMainTile3;
	}
	public String getBusinessActivitySubTile3() {
		return businessActivitySubTile3;
	}
	public void setBusinessActivitySubTile3(String businessActivitySubTile3) {
		this.businessActivitySubTile3 = businessActivitySubTile3;
	}
	public String getBusinessActivityMainTile4() {
		return businessActivityMainTile4;
	}
	public void setBusinessActivityMainTile4(String businessActivityMainTile4) {
		this.businessActivityMainTile4 = businessActivityMainTile4;
	}
	public String getBusinessActivitySubTile4() {
		return businessActivitySubTile4;
	}
	public void setBusinessActivitySubTile4(String businessActivitySubTile4) {
		this.businessActivitySubTile4 = businessActivitySubTile4;
	}
	public String getBusinessActivityMainTile5() {
		return businessActivityMainTile5;
	}
	public void setBusinessActivityMainTile5(String businessActivityMainTile5) {
		this.businessActivityMainTile5 = businessActivityMainTile5;
	}
	public String getBusinessActivitySubTile5() {
		return businessActivitySubTile5;
	}
	public void setBusinessActivitySubTile5(String businessActivitySubTile5) {
		this.businessActivitySubTile5 = businessActivitySubTile5;
	}
	public String getSearchedItem() {
		return searchedItem;
	}
	public void setSearchedItem(String searchedItem) {
		this.searchedItem = searchedItem;
	}
	public int getDateValid() {
		return dateValid;
	}
	public void setDateValid(int dateValid) {
		this.dateValid = dateValid;
	}
	public int getNoOfEmployeesMale() {
		return noOfEmployeesMale;
	}
	public void setNoOfEmployeesMale(int noOfEmployeesMale) {
		this.noOfEmployeesMale = noOfEmployeesMale;
	}
	public int getNoOfEmployeesFemale() {
		return noOfEmployeesFemale;
	}
	public void setNoOfEmployeesFemale(int noOfEmployeesFemale) {
		this.noOfEmployeesFemale = noOfEmployeesFemale;
	}
	public String getBarangay() {
		return barangay;
	}
	public void setBarangay(String barangay) {
		this.barangay = barangay;
	}
	public int getDistrict() {
		return district;
	}
	public void setDistrict(int district) {
		this.district = district;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getOtherRemarks() {
		return otherRemarks;
	}
	public void setOtherRemarks(String otherRemarks) {
		this.otherRemarks = otherRemarks;
	}
	public String getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}
	public String getEvaluationOfFacts() {
		return evaluationOfFacts;
	}
	public void setEvaluationOfFacts(String evaluationOfFacts) {
		this.evaluationOfFacts = evaluationOfFacts;
	}
	public double getBusinessArea() {
		return businessArea;
	}
	public void setBusinessArea(double businessArea) {
		this.businessArea = businessArea;
	}
	public String getTctNo() {
		return tctNo;
	}
	public void setTctNo(String tctNo) {
		this.tctNo = tctNo;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getRemarks2() {
		return remarks2;
	}
	public void setRemarks2(String remarks2) {
		this.remarks2 = remarks2;
	}
	public String getOtherRemarks2() {
		return otherRemarks2;
	}
	public void setOtherRemarks2(String otherRemarks2) {
		this.otherRemarks2 = otherRemarks2;
	}
	public String getRemarks3() {
		return remarks3;
	}
	public void setRemarks3(String remarks3) {
		this.remarks3 = remarks3;
	}
	public String getOtherRemarks3() {
		return otherRemarks3;
	}
	public void setOtherRemarks3(String otherRemarks3) {
		this.otherRemarks3 = otherRemarks3;
	}
	public String getNoticeOfAction() {
		return noticeOfAction;
	}
	public void setNoticeOfAction(String noticeOfAction) {
		this.noticeOfAction = noticeOfAction;
	}
	public String getOtherNoticeOfAction() {
		return otherNoticeOfAction;
	}
	public void setOtherNoticeOfAction(String otherNoticeOfAction) {
		this.otherNoticeOfAction = otherNoticeOfAction;
	}
	public byte[] getQrCode() {
		return qrCode;
	}
	public void setQrCode(byte[] qrCode) {
		this.qrCode = qrCode;
	}
	public String getImageQrCode() {
		return imageQrCode;
	}
	public void setImageQrCode(String imageQrCode) {
		this.imageQrCode = imageQrCode;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getTotalInWords() {
		return totalInWords;
	}
	public void setTotalInWords(String totalInWords) {
		this.totalInWords = totalInWords;
	}
	public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public String getYearAdded() {
		return yearAdded;
	}
	public void setYearAdded(String yearAdded) {
		this.yearAdded = yearAdded;
	}
	public String getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}
	public String getApprovedForPrinting() {
		return approvedForPrinting;
	}
	public void setApprovedForPrinting(String approvedForPrinting) {
		this.approvedForPrinting = approvedForPrinting;
	}
}
