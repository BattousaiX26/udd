package com.controllers.java;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.dao.java.ReceivingDao;
import com.entities.java.LocationalClearance;
import com.utilities.java.MpUtilities;

/**
 * Servlet implementation class ReceivingController
 */
@WebServlet("/JSP/receiving")
public class ReceivingController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private LocationalClearance lc; 
    private ReceivingDao rcd;
    private static final Logger log = Logger.getLogger(ReceivingController.class);
  
    public ReceivingController() {
        super();
        lc = new LocationalClearance();
        rcd = new ReceivingDao();
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String encodedBy = session.getAttribute("nameOfUser").toString();lc.setEncodedBy(encodedBy);
		String businessName = request.getParameter("businessName");lc.setBusinessName(businessName);
		String mpNo = request.getParameter("mpNo");lc.setMpNo(mpNo);
		String taxPayerName = request.getParameter("taxPayerName");lc.setTaxPayerName(taxPayerName);
		String noOfEmployeesMale = request.getParameter("noOfEmployeesMale");
		int noOfEmpMale = 0;
		try {
			if(noOfEmployeesMale.isEmpty()) {
				lc.setNoOfEmployeesMale(noOfEmpMale);
			}else {
				noOfEmpMale = MpUtilities.convertToInteger(noOfEmployeesMale);lc.setNoOfEmployeesMale(noOfEmpMale);
			}
		} catch (Exception e) {
			log.error("Unable to convert input to integer on ReceivingController, variable noOfEmpMale \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}
		String noOfEmployeesFemale = request.getParameter("noOfEmployeesFemale");
		int noOfEmpFemale = 0;
		try {
			if(noOfEmployeesFemale.isEmpty()) {
				lc.setNoOfEmployeesMale(noOfEmpFemale);
			}else {
				noOfEmpFemale = MpUtilities.convertToInteger(noOfEmployeesFemale);lc.setNoOfEmployeesFemale(noOfEmpFemale);
			}
		} catch (Exception e) {
			log.error("Unable to convert input to integer on ReceivingController, variable noOfEmpFemale \n" + e.getCause());
			log.error("Cause " + e.getCause());
		}
		String sex = request.getParameter("sex");lc.setSex(sex);
		String rightOverLand = request.getParameter("rightOverLand");lc.setRightOverLand(rightOverLand);
		String landRate = request.getParameter("landRate");lc.setLandRate(landRate);
		String capitalization = request.getParameter("capitalization"); lc.setCapitalization(capitalization);	
		String businessArea = request.getParameter("businessArea");
		double bsArea = 0;
		try {
			if(businessArea != null) {
				bsArea = MpUtilities.convertToDouble(businessArea);lc.setBusinessArea(bsArea);
			}
		} catch (Exception e) {
			log.error("Unable to convert input to double on ReceivingController, variable bsArea \n" + e.getMessage());
			log.error("Cause " + e.getMessage());
		}
		String address = request.getParameter("address");lc.setAddress(address);
		String barangay = request.getParameter("barangay");lc.setBarangay(barangay);
		String district = request.getParameter("district");
		int dist = 0;
		try {
			if(district==null) {
				lc.setDistrict(dist);
			}else {
				dist = MpUtilities.convertToInteger(district);lc.setDistrict(dist);
			}	
		} catch (Exception e) {
			log.error("Unable to convert input to integer on ReceivingController, variable dist \n" + e);
			log.error("Cause " + e.getCause());
		}
		String businessActivityMainTile1 = request.getParameter("businessActivityMainTile1");lc.setBusinessActivityMainTile1(businessActivityMainTile1);
		String businessActivitySubTile1 = request.getParameter("businessActivitySubTile1");lc.setBusinessActivitySubTile1(businessActivitySubTile1);
		String businessActivityMainTile2 = request.getParameter("businessActivityMainTile2");lc.setBusinessActivityMainTile2(businessActivityMainTile2);
		String businessActivitySubTile2 = request.getParameter("businessActivitySubTile2");lc.setBusinessActivitySubTile2(businessActivitySubTile2);
		String businessActivityMainTile3 = request.getParameter("businessActivityMainTile3");lc.setBusinessActivityMainTile3(businessActivityMainTile3);
		String businessActivitySubTile3 = request.getParameter("businessActivitySubTile3");lc.setBusinessActivitySubTile3(businessActivitySubTile3);
		String businessActivityMainTile4 = request.getParameter("businessActivityMainTile4");lc.setBusinessActivityMainTile4(businessActivityMainTile4);
		String businessActivitySubTile4 = request.getParameter("businessActivitySubTile4");lc.setBusinessActivitySubTile4(businessActivitySubTile4);
		String businessActivityMainTile5 = request.getParameter("businessActivityMainTile5");lc.setBusinessActivityMainTile5(businessActivityMainTile5);
		String businessActivitySubTile5 = request.getParameter("businessActivitySubTile5");lc.setBusinessActivitySubTile5(businessActivitySubTile5);	
		try {
			List<LocationalClearance> generatedItems = rcd.saveToMasterRecords(lc);
			if(generatedItems!=null) {				
				session.setAttribute("generatedItems", generatedItems);			
				response.sendRedirect(request.getContextPath()+"/JSP/receivingTransaction.jsp");
			}
		} catch (Exception e) {
			log.error("Unable to fill generatedItems on ReceivingController \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}	
	}
}
