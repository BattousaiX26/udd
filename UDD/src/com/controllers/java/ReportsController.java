package com.controllers.java;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.dao.java.ReportsDao;
import com.entities.java.LocationalClearance;
import com.utilities.java.MpUtilities;

@WebServlet("/JSP/reports")
public class ReportsController extends HttpServlet {
	private static final long serialVersionUID = 1L;  
    private ReportsDao rd;
    private static final Logger log = Logger.getLogger(ReportsController.class);
    
    public ReportsController() {
        super();
        rd = new ReportsDao();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String inputCriteria = request.getParameter("inputCriteria");
		
		try {	
			String currentYear = MpUtilities.getCurrentYear();
			String reportCriteria = MpUtilities.getFirstTwofString(currentYear)+"-";
			List<LocationalClearance> reportItems = rd.generateReports(action,reportCriteria,inputCriteria);
			request.setAttribute("reportItems", reportItems);
			RequestDispatcher rd = request.getRequestDispatcher("/JSP/reportsSearched.jsp");rd.forward(request, response);
		} catch (Exception e) {
			log.error("Error on ReportsController \n" + e.getMessage());
			log.error("Cause" + e.getCause());
		}
	}
}
