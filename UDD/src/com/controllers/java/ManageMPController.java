package com.controllers.java;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.dao.java.ManageMpDao;
import com.dao.java.ReceivingDao;
import com.entities.java.LocationalClearance;
import com.utilities.java.MpUtilities;

/**
 * Servlet implementation class SearchMPController
 */
@WebServlet("/JSP/manageMp")
public class ManageMPController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ManageMpDao mpd;
	private LocationalClearance lc;
	private ReceivingDao rcd;
	private static final Logger log = Logger.getLogger(ManageMPController.class);
	
	public ManageMPController(){
		super();
		mpd = new ManageMpDao();
		lc = new LocationalClearance();
		rcd = new ReceivingDao();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String action=request.getParameter("action");
		String forward="";
		if(action.equalsIgnoreCase("search")) {
			String mpNumber = request.getParameter("mpNumber");		
			try {
				lc.setSearchedItem(mpNumber);
				List<LocationalClearance> searchedItems = mpd.search(lc);
				request.setAttribute("searchedItems", searchedItems);	
				forward = "/JSP/searchForMp.jsp";
				RequestDispatcher rd = request.getRequestDispatcher(forward);rd.forward(request, response);		
			} catch (Exception e) {
				log.error("Unable to retrieve List<LocationalClearance> searchedItems using search method of ManageMpDao \n" + e.getMessage());
				log.error("Cause " + e.getCause());
			}		
		}else if(action.equalsIgnoreCase("getTableRow")) {
			String id = request.getParameter("id");
			int sid = Integer.parseInt(id);
			
			try {
				lc.setId(sid);
				List<LocationalClearance> items = mpd.getTableValues(lc);
				request.setAttribute("items", items);
				forward = "/JSP/locationalEntry.jsp";
				RequestDispatcher rd = request.getRequestDispatcher(forward);rd.forward(request, response);	
			} catch (Exception e) {
				log.error("Unable to retrieve List<LocationalClearance> items using getTableValues method of ManageMpDao \n" + e.getMessage());
				log.error("Cause " + e.getCause());
			}
		}else if(action.equalsIgnoreCase("reprint")) {
			String mpNumber = request.getParameter("mpNumber");
			lc.setMpNo(mpNumber);
			
			try {
				List<LocationalClearance> generatedItems = rcd.getRecords(lc);
				if(generatedItems!=null) {				
					session.setAttribute("generatedItems", generatedItems);			
					response.sendRedirect(request.getContextPath()+"/JSP/receivingTransaction.jsp");
				}
			} catch (Exception e) {
				log.error("Unable to retrieve List<LocationalClearance> generatedItems using getRecords method of ManageMpDao used for reprinting \n" + e.getMessage());
				log.error("Cause " + e.getCause());
			}
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Receiving
		HttpSession session = request.getSession();
		String encodedBy = session.getAttribute("nameOfUser").toString();lc.setEncodedBy(encodedBy);
		String businessName = request.getParameter("businessName");lc.setBusinessName(businessName);
		String taxPayerName = request.getParameter("taxPayerName");lc.setTaxPayerName(taxPayerName);
		String noOfEmployeesMale = request.getParameter("noOfEmployeesMale");
		int noOfEmpMale = 0;
		try {
			if(noOfEmployeesMale.isEmpty()) {
				lc.setNoOfEmployeesMale(noOfEmpMale);
			}else {
				noOfEmpMale = MpUtilities.convertToInteger(noOfEmployeesMale);lc.setNoOfEmployeesMale(noOfEmpMale);
			}
		} catch (Exception e) {
			log.error("Unable to convert input to integer on ManageMpController, variable noOfEmployeesMale /n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}
		String noOfEmployeesFemale = request.getParameter("noOfEmployeesFemale");
		int noOfEmpFemale = 0;
		try {
			if(noOfEmployeesFemale.isEmpty()) {
				lc.setNoOfEmployeesMale(noOfEmpFemale);
			}else {
				noOfEmpFemale = MpUtilities.convertToInteger(noOfEmployeesFemale);lc.setNoOfEmployeesFemale(noOfEmpFemale);
			}
		} catch (Exception e) {
			log.error("Unable to convert input to Integer on ManageMpController, variable noOfEmployeesFemale /n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}
		String sex = request.getParameter("sex");lc.setSex(sex);
		String rightOverLand = request.getParameter("rightOverLand");lc.setRightOverLand(rightOverLand);
		String landRate = request.getParameter("landRate");lc.setLandRate(landRate);
		String capitalization = request.getParameter("capitalization");lc.setCapitalization(capitalization);	
		String address = request.getParameter("address");lc.setAddress(address);
		String barangay = request.getParameter("barangay");lc.setBarangay(barangay);
		String district = request.getParameter("district");
		int dist = 0;
		try {
			if(district.isEmpty()) {
				lc.setDistrict(dist);
			}else {
				dist = MpUtilities.convertToInteger(district);lc.setDistrict(dist);
			}	
		} catch (Exception e) {
			log.error("Unable to convert input to integer on ManageMpController, variable dist /n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}
		String businessArea = request.getParameter("businessArea");
		double bsArea = 0;
		try {
			if(businessArea != null) {
				bsArea = MpUtilities.convertToDouble(businessArea);lc.setBusinessArea(bsArea);
			}
		} catch (Exception e) {
			log.error("Unable to convert input to double on ManageMpController, variable bsArea /n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}
		String businessActivityMainTile1 = request.getParameter("businessActivityMainTile1");lc.setBusinessActivityMainTile1(businessActivityMainTile1);
		String businessActivitySubTile1 = request.getParameter("businessActivitySubTile1");lc.setBusinessActivitySubTile1(businessActivitySubTile1);
		// removed front end interface of maintile2-sub-2 to main5-sub5 on locational entry
		String businessActivityMainTile2 = request.getParameter("businessActivityMainTile2");lc.setBusinessActivityMainTile2(businessActivityMainTile2);
		String businessActivitySubTile2 = request.getParameter("businessActivitySubTile2");lc.setBusinessActivitySubTile2(businessActivitySubTile2);
		String businessActivityMainTile3 = request.getParameter("businessActivityMainTile3");lc.setBusinessActivityMainTile3(businessActivityMainTile3);
		String businessActivitySubTile3 = request.getParameter("businessActivitySubTile3");lc.setBusinessActivitySubTile3(businessActivitySubTile3);
		String businessActivityMainTile4 = request.getParameter("businessActivityMainTile4");lc.setBusinessActivityMainTile4(businessActivityMainTile4);
		String businessActivitySubTile4 = request.getParameter("businessActivitySubTile4");lc.setBusinessActivitySubTile4(businessActivitySubTile4);
		String businessActivityMainTile5 = request.getParameter("businessActivityMainTile5");lc.setBusinessActivityMainTile5(businessActivityMainTile5);
		String businessActivitySubTile5 = request.getParameter("businessActivitySubTile5");lc.setBusinessActivitySubTile5(businessActivitySubTile5);
		//Variables below are for evaluation part of the form on View
		String mpNo = request.getParameter("mpNo");lc.setMpNo(mpNo);
		String cpdoNo = request.getParameter("cpdoNo");lc.setCpdoNo(cpdoNo);
		String status = request.getParameter("status");lc.setStatus(status);
		String pisc = request.getParameter("pisc");lc.setPisc(pisc);
		String zoningClass = request.getParameter("zoningClass");lc.setZoningClass(zoningClass);
		String evaluation = request.getParameter("evaluation");lc.setEvaluation(evaluation);
		String zoningOfficial = request.getParameter("zoningOfficial");lc.setZoningOfficial(zoningOfficial);
		String remarks = request.getParameter("remarks");lc.setRemarks(remarks);
		String otherRemarks = request.getParameter("otherRemarks");lc.setOtherRemarks(otherRemarks);
		String verifier = request.getParameter("verifier");lc.setVerifier(verifier);
		try {
			List<LocationalClearance> generatedItems = mpd.saveToMasterRecords(lc);
			if(generatedItems!=null) {				
				session.setAttribute("generatedItems", generatedItems);			
				response.sendRedirect(request.getContextPath()+"/JSP/receivingTransaction.jsp");
			}
		} catch (Exception e) {
			log.error("Unable to fill up List<LocationalClearance> generatedItems on ManageMpController /n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}	
	}
}
