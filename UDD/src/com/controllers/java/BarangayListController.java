package com.controllers.java;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.entities.java.LocationalClearance;
import com.utilities.java.MpUtilities;
import com.utilities.java.OtherUtilities;

/**
 * Servlet implementation class BarangayListController
 */
@WebServlet("/JSP/barangayList")
public class BarangayListController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private LocationalClearance lc;   
    private OtherUtilities ou;
    private static final Logger log = Logger.getLogger(BarangayListController.class);
    
    public BarangayListController() {
        super();
        lc = new LocationalClearance();
        ou = new OtherUtilities();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String district = request.getParameter("district");	
		try {
			int intDistrict = 0;
			if(district!=null) {
				intDistrict = MpUtilities.convertToInteger(district);lc.setDistrict(intDistrict);
			}
			List<LocationalClearance> barangayList = ou.getBarangayList(lc);
			request.setAttribute("barangayList", barangayList);
			request.getRequestDispatcher("/JSP/encoding.jsp").forward(request, response);
		} catch (Exception e) {
			log.error("Error on BarangayListController message:" + e.getMessage());
			log.error("Error on BarangayListController cause:" + e.getCause());
		}		
	}
}
