package com.controllers.java;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.dao.java.EvaluationDao;
import com.entities.java.LocationalClearance;
import com.utilities.java.EnglishNumberToWords;
import com.utilities.java.MpUtilities;

/**
 * Servlet implementation class EvaluationController
 */
@WebServlet("/JSP/evaluate")
public class EvaluationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private LocationalClearance lc;
    private EvaluationDao ed;
    private static final Logger log = Logger.getLogger(EvaluationController.class);
    
    public EvaluationController() {
        super();
        lc = new LocationalClearance();
        ed = new EvaluationDao();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getParameter("action");
		String forward = "";
		String transactionNumber = request.getParameter("transactionNo");lc.setTransactionNumber(transactionNumber);
		String evaluation="";
		try {
			List<LocationalClearance> itemsTobeEvaluated = ed.getItemsToBeEvaluated(lc);
			           
            for (LocationalClearance lcGetter : itemsTobeEvaluated) {
                evaluation = lcGetter.getEvaluation();          
            }
			request.setAttribute("itemsTobeEvaluated", itemsTobeEvaluated);	
			
			if(action.equalsIgnoreCase("searchTransaction")) {	
				forward = "/JSP/evaluationSearched.jsp";
			}else if(action.equalsIgnoreCase("printLC")) {
				//module on printing, can be rearchitected by creating PrintingController
				if(evaluation.equalsIgnoreCase("permitted")) {
					forward = "/JSP/lcForm.jsp";
				}else {
					forward = "/JSP/lcFormNoa.jsp";
				}
			}
		} catch (Exception e) {
			log.error("Error on EvaluationController \n" + e.getMessage());
			log.error("Cause is " + e.getCause());
		}				
		RequestDispatcher rd = request.getRequestDispatcher(forward);rd.forward(request, response);	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String id = request.getParameter("id");
		try {
			int sid = MpUtilities.convertToInteger(id);	lc.setId(sid);
		} catch (Exception e) {
			log.error("Unable to convert id to integer using convertToInteger \n" + e.getMessage());
			log.error("Cause is " + e.getCause());
		}	
		String transactionNumber = request.getParameter("transactionNo");lc.setTransactionNumber(transactionNumber);
		String mpNo = request.getParameter("mpNo");lc.setMpNo(mpNo);
		String businessName = request.getParameter("businessName");lc.setBusinessName(businessName);
		String taxPayerName = request.getParameter("taxPayerName");lc.setTaxPayerName(taxPayerName);
		String noOfEmployeesMale = request.getParameter("noOfEmployeesMale");
		try {
			int noOfEmpMale = 0;
			if(noOfEmployeesMale!=null) {
				noOfEmpMale = MpUtilities.convertToInteger(noOfEmployeesMale);lc.setNoOfEmployeesMale(noOfEmpMale);
			}
		} catch (Exception e) {
			log.error("Unable to convert input to Integer on EvaluationController, variable noOfEmpMale \n" + e.getMessage());
			log.error("Cause is " + e.getCause());
		}
		String noOfEmployeesFemale = request.getParameter("noOfEmployeesFemale");
		try {
			int noOfEmpFemale = 0;
			if(noOfEmployeesFemale!=null) {
				noOfEmpFemale = MpUtilities.convertToInteger(noOfEmployeesFemale);lc.setNoOfEmployeesFemale(noOfEmpFemale);
			}
		} catch (Exception e) {
			log.error("Unable to convert input to Integer on EvaluationController, variable noOfEmpFemale \n" + e.getMessage());
			log.error("Cause is " + e.getCause());
		}
		String sex = request.getParameter("sex");lc.setSex(sex);
		String rightOverLand = request.getParameter("rightOverLand");lc.setRightOverLand(rightOverLand);
		String landRate = request.getParameter("landRate");lc.setLandRate(landRate);
		String capitalization = request.getParameter("capitalization");lc.setCapitalization(capitalization);
		String address = request.getParameter("address");lc.setAddress(address);
		String district = request.getParameter("district");
		try {
			int dist = 0;
			if(district.isEmpty()) {
				lc.setDistrict(dist);
			}else {
				dist = MpUtilities.convertToInteger(district);lc.setDistrict(dist);
			}
		} catch (Exception e) {
			log.error("Unable to convert input to Integer on EvaluationController, variable dist \n" + e.getMessage());
			log.error("Cause is " + e.getCause());
		}
		String barangay = request.getParameter("barangay");lc.setBarangay(barangay);
		String businessActivityMainTile1 = request.getParameter("businessActivityMainTile1");lc.setBusinessActivityMainTile1(businessActivityMainTile1);
		String businessActivitySubTile1 = request.getParameter("businessActivitySubTile1");lc.setBusinessActivitySubTile1(businessActivitySubTile1);
		//evaluation
		String evaluation = request.getParameter("evaluation");lc.setEvaluation(evaluation);
		String cpdoNo = request.getParameter("cpdoNo");lc.setCpdoNo(cpdoNo);
		String dateValid = request.getParameter("dateValid");
		try {
			int dteVld = 0;
			if(dateValid!=null) {
				dteVld = MpUtilities.convertToInteger(dateValid);lc.setDateValid(dteVld);
			}
		} catch (Exception e) {
			log.error("Unable to convert input to Integer on EvaluationController, variable dteVld \n" + e.getMessage());
			log.error("Cause is " + e.getCause());
		}
		try {
			double total = 0;
			String inWords = "";
			String toBeConvertedToWords="";
			long totalToLong = 0;
			if(dateValid.equals("1")) {			
				toBeConvertedToWords = "50.00";
				total = MpUtilities.convertToDouble(toBeConvertedToWords);lc.setTotal(total);
				totalToLong =  (long) total;
				inWords = EnglishNumberToWords.convert(totalToLong);
				lc.setTotalInWords(inWords);			
			}else if(dateValid.equals("3")) {	
				toBeConvertedToWords = "150.00";
				total = MpUtilities.convertToDouble(toBeConvertedToWords);lc.setTotal(total);
				totalToLong =  (long) total;
				inWords = EnglishNumberToWords.convert(totalToLong);
				lc.setTotalInWords(inWords);
			}
		} catch (Exception e) {
			log.error("Error on EvaluationController in converting to words \n" + e.getMessage());
			log.error("Cause is " + e.getCause());
		}

		String evaluationOfFacts = request.getParameter("evaluationOfFacts");lc.setEvaluationOfFacts(evaluationOfFacts);
		String dateIssued = request.getParameter("dateIssued");
		try {
			if(dateIssued.isEmpty()) {
				String dtIssued = MpUtilities.getCurrentDate();lc.setDateIssued(dtIssued);
			}else {
				lc.setDateIssued(dateIssued);
			}
		} catch (Exception e) {
			log.error("Unable to getCurrentDate on EvaluationController \n" + e.getMessage());
			log.error("Cause is " + e.getCause());
		}
		String tctNo = request.getParameter("tctNo");lc.setTctNo(tctNo);
		String status = request.getParameter("status");lc.setStatus(status);	
		String verifier = request.getParameter("verifier");lc.setVerifier(verifier);
		String lotArea = request.getParameter("lotArea");
		try {
			double ltArea = 0;
			if(lotArea.isEmpty()) {
				lc.setLotArea(ltArea);
			}else {
				ltArea = MpUtilities.convertToDouble(lotArea);lc.setLotArea(ltArea);
			}
		} catch (Exception e) {
			log.error("Unable to convert input to Double on EvaluationController, variable ltArea \n" + e.getMessage());
			log.error("Cause is " + e.getCause());
		}
		String businessArea = request.getParameter("businessArea");
		double bsArea = 0;
		try {
			if(businessArea != null) {
				bsArea = MpUtilities.convertToDouble(businessArea);lc.setBusinessArea(bsArea);
			}
		} catch (Exception e) {
			log.error("Unable to convert input to double on EvaluationController, variable bsArea \n" + e.getMessage());
			log.error("Cause is " + e.getCause());
		}
		String pisc = request.getParameter("pisc");lc.setPisc(pisc);
		String zoningClass = request.getParameter("zoningClass");lc.setZoningClass(zoningClass);
		String zoningOfficial = request.getParameter("zoningOfficial");lc.setZoningOfficial(zoningOfficial);
		String remarks = request.getParameter("remarks");lc.setRemarks(remarks);
		if(remarks.equalsIgnoreCase("others")) {
			String otherRemarks = request.getParameter("otherRemarks");lc.setOtherRemarks(otherRemarks);
		}	
		String remarks2 = request.getParameter("remarks2");lc.setRemarks2(remarks2);
		if(remarks2.equalsIgnoreCase("others")) {
			String otherRemarks2 = request.getParameter("otherRemarks2");lc.setOtherRemarks2(otherRemarks2);
		}
		String remarks3 = request.getParameter("remarks3");lc.setRemarks3(remarks3);
		if(remarks3.equalsIgnoreCase("others")) {
			String otherRemarks3 = request.getParameter("otherRemarks3");lc.setOtherRemarks3(otherRemarks3);
		}
		String noticeOfAction = request.getParameter("noticeOfAction");lc.setNoticeOfAction(noticeOfAction);
		if(noticeOfAction.equalsIgnoreCase("others")) {
			String otherNoticeOfAction = request.getParameter("otherNoticeOfAction");lc.setOtherNoticeOfAction(otherNoticeOfAction);
		}
		if(session.getAttribute("finalEvaluation")!=null) {
			String approvedBy  = session.getAttribute("nameOfUser").toString(); lc.setApprovedBy(approvedBy);
		}	
		String approvedForPrinting = request.getParameter("approveForPrinting");lc.setApprovedForPrinting(approvedForPrinting);
		try {
			boolean isSaved = ed.isEvaluationSaved(lc);
			
			if(isSaved == true) {
				response.getWriter().write("Saved");
			}else {
				response.getWriter().write("NotSaved");
			}
		} catch (Exception e) {
			log.error("Unable to save on EvaluationController \n" + e.getMessage());
			log.error("Cause is " + e.getCause());
		}
	}
}
