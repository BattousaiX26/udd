package com.controllers.java;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.dao.java.ReleasingDao;
import com.entities.java.LocationalClearance;
import com.entities.java.Releasing;

/**
 * Servlet implementation class ReleasingController
 */
@WebServlet("/JSP/releasing")
public class ReleasingController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private Releasing rls; 
    private ReleasingDao rda;
    private LocationalClearance lc;
    private static final Logger log = Logger.getLogger(ReleasingController.class);
    
    public ReleasingController() {
        super();
        rls = new Releasing();
        rda = new ReleasingDao();
        lc = new LocationalClearance();
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String transactionNo = request.getParameter("transactionNo");rls.setTransactionNumber(transactionNo);lc.setTransactionNumber(transactionNo);
    	String action=request.getParameter("action");
    	String forward="";
    	
    	try {
    		if(action.equalsIgnoreCase("getMasterRecords")) {
    			List<Releasing> releasingRecords = rda.getMasterRecords(rls);
    			forward = "/JSP/releasingEncoding.jsp";
    			request.setAttribute("releasingRecords", releasingRecords);
    		}else if(action.equalsIgnoreCase("getReleasingRecords")){
    			List<Releasing> releasingRecords = rda.getReleasingRecords(rls);
    			forward = "/JSP/releasingSearched.jsp";
    			request.setAttribute("releasingRecords", releasingRecords);
    		}else if(action.equalsIgnoreCase("checkStatus")) {
    			List<LocationalClearance> recordsWithStatus = rda.getItemsWithStatus(lc);
    			forward = "/JSP/applicationStatus.jsp";
    			request.setAttribute("recordsWithStatus", recordsWithStatus);
    		}else if(action.equalsIgnoreCase("generateOrderOfPayment")) {
    			List<Releasing> releasingRecords = rda.generateOrderOfPayment(rls);
    			forward = "/JSP/orderOfPayment.jsp";
    			request.setAttribute("releasingRecords", releasingRecords);
    		}
			RequestDispatcher rd = request.getRequestDispatcher(forward);rd.forward(request, response);
		} catch (Exception e) {
			log.error("Error on ReleasingController Get \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mpNo = request.getParameter("mpNo");rls.setMpNumber(mpNo);
		String transactionNo = request.getParameter("transactionNo");rls.setTransactionNumber(transactionNo);
		String businessName = request.getParameter("businessName");rls.setBusinessName(businessName);
		String taxPayerName = request.getParameter("taxPayerName");rls.setTaxPayerName(taxPayerName);
		String receiptNo = request.getParameter("receiptNo");rls.setReceiptNumber(receiptNo);
		String amount = request.getParameter("amount");rls.setAmount(amount);
		String sealNo = request.getParameter("sealNo");rls.setSealNumber(sealNo);		
		try {		
			boolean isSaved = rda.isSaved(rls);
			if(isSaved==true) {
				response.getWriter().write("Saved");
			}else {
				response.getWriter().write("NotSaved");
			}
		} catch (Exception e) {
			log.error("Error on saving on ReleasingController Post\n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}
	}
}
