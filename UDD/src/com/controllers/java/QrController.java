package com.controllers.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Reader;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import sun.misc.BASE64Decoder;

/**
 * Servlet implementation class QrController
 */
@WebServlet("/JSP/qrRead")
public class QrController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Result result = null;
	BinaryBitmap binaryBitmap;
	private Gson gson = new Gson();
	private static final Logger log = Logger.getLogger(QrController.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try { 
			StringBuffer buffer = new StringBuffer(); 
			Reader reader = request.getReader(); 
			int current; 
			while((current = reader.read()) >= 0) 
			buffer.append((char) current); 
			String data = new String(buffer); 
			data = data.substring(data.indexOf(",") + 1); 
			
			FileOutputStream output = new FileOutputStream(new File("C:\\QRImages\\Picture" + "QR_IMAGE" + ".png")); 
			output.write(new BASE64Decoder().decodeBuffer(data)); output.flush(); output.close(); 
			binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(ImageIO.read(new FileInputStream("C:\\QRImages\\PictureQR_IMAGE.png")))));
			result = new MultiFormatReader().decode(binaryBitmap);
	
			String resultToString = result.getText();
			String resultToStringToJson = this.gson.toJson(resultToString);
			
			response.setContentType("application/json;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(resultToStringToJson);
			
			output.close();
		} catch (Exception e) { 
			log.error("Error in QrController \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		} 
	}	
}
