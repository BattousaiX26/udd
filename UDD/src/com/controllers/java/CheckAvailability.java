package com.controllers.java;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.database.java.ConnectionManager;


/**
 * Servlet implementation class CheckAvailability
 */
@WebServlet("/checkAvailability")
public class CheckAvailability extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(CheckAvailability.class);
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 response.setContentType("text/html;charset=UTF-8");
	     PrintWriter out = response.getWriter();
	        try {

	        	ConnectionManager con = new ConnectionManager();
				Connection connect = con.getConnection();				
	            String username = request.getParameter("username");
	            PreparedStatement ps = connect.prepareStatement("select username from tbl_users where username=?");
	            ps.setString(1,username);
	            ResultSet rs = ps.executeQuery();
	             
	            if (!rs.next()) {
	            	out.println("<font color=green>");
	                out.println("("+username+" is available)");
	                out.println("</font>");
	            }
	            else{
	            	out.println("<font color=red>");
	            	out.println("("+username+" is already in use)");
	            	out.println("</font>");
	            }
	            	out.println();
	        }catch (SQLException sqlE) {
				log.error("Problem on retrieving data from CheckAvailability Controller" + sqlE.getMessage());
				log.error("Cause is" + sqlE.getCause());
			} 
	        catch (Exception ex) {
	            out.println("Error ->" + ex.getMessage());
	        } finally {
	            out.close();
	        }
	}
}
