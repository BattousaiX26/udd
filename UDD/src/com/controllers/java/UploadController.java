package com.controllers.java;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.utilities.java.Excel;

/**
 * Servlet implementation class UploadController
 */
@WebServlet("/JSP/uploadExcel")
@MultipartConfig
public class UploadController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static Logger logger = LogManager.getLogger(UploadController.class.getName());
	private Gson gson;
	private Excel excel;
	
    public UploadController() {
    	super();
    	excel = new Excel();  
    	gson = new Gson();
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String jsonResponse = "";
		String json = "";
	    Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
		String fileName; 
		InputStream fileContent = null;
		OutputStream fileOutput = null;
					
		try {
			if(filePart!=null) {
				fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
				fileContent = filePart.getInputStream();
				
				if(fileContent!=null) {
					fileOutput = new FileOutputStream(new File("C://excel_upload//"+fileName), false);
					
					int read = 0;
					byte[] bytes = new byte[1024];

					while ((read = fileContent.read(bytes)) != -1) {
						fileOutput.write(bytes, 0, read);
					}
				}
				boolean isInserted = excel.readAndSaveExcel();
				if(isInserted == true) {			
					json = "successful";
					jsonResponse = this.gson.toJson(json);				
				}else {
					json = "failed";
					jsonResponse = this.gson.toJson(json);
				}
				response.getWriter().write(jsonResponse);
			}
		} catch (Exception e) {
			logger.fatal("Unable to upload excel file! " + e.getMessage());
			logger.fatal("Cause " + e.getCause());
			logger.fatal("Trace " + e.getStackTrace());
			e.printStackTrace();
		}finally {
			if(fileOutput!=null) {
				try {
					fileOutput.close();
				} catch (Exception e2) {
					logger.error("On finally cannot close fileOutput" + e2.getMessage());
					logger.error("Cause " + e2.getCause());
				}
			}if(fileContent!=null) {
				try {
					fileContent.close();
				} catch (IOException e) {
					logger.error("On finally cannot close fileContent" + e.getMessage());
					logger.error("Cause " + e.getCause());
				}
			}
		}
	}
}
