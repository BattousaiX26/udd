package com.controllers.java;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.dao.java.PrintingDao;
import com.entities.java.LocationalClearance;

/**
 * Servlet implementation class PrintingController
 */
@WebServlet("/JSP/generatePrintingForm")
public class PrintingController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private LocationalClearance lc;
	private PrintingDao pd;
	private static final Logger log = Logger.getLogger(PrintingController.class);
	
    public PrintingController() {
        super();
        lc = new LocationalClearance();
        pd = new PrintingDao();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String transactionNumber = request.getParameter("transactionNo");lc.setTransactionNumber(transactionNumber);
		String evaluation="";
		String approvedForPrinting ="";
		String forward = "";
		
		try {
			List<LocationalClearance> itemsTobeEvaluated = pd.getItemsForPrinting(lc);
			    
			//get evaluation status and approval for printing and decide which form to be printed
            for (LocationalClearance lcGetter : itemsTobeEvaluated) {
                evaluation = lcGetter.getEvaluation();   
                approvedForPrinting = lcGetter.getApprovedForPrinting(); 
            }
			
            if(approvedForPrinting==null||evaluation==null) {
            	request.setAttribute("itemsTobeEvaluated", "");	
				forward = "/JSP/lcForm.jsp";
            }else if(approvedForPrinting.equalsIgnoreCase("NO")) {
				request.setAttribute("itemsTobeEvaluated", "");	
				forward = "/JSP/lcForm.jsp";
			}else if(approvedForPrinting.equalsIgnoreCase("YES")) {
				request.setAttribute("itemsTobeEvaluated", itemsTobeEvaluated);	
				if(evaluation.equalsIgnoreCase("permitted")) {
					forward = "/JSP/lcForm.jsp";
				}else {
					forward = "/JSP/lcFormNoa.jsp";
				}
			}		
		} catch (Exception e) {
			log.error("Error on PrintingController there might be some fields on evaluation that are not filled up  \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}				
		RequestDispatcher rd = request.getRequestDispatcher(forward);rd.forward(request, response);	
	}
}
