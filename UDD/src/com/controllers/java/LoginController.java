package com.controllers.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.dao.java.LoginDao;
import com.entities.java.Accounts;
import com.google.gson.Gson;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/login")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();
	
	final static Logger logger = LogManager.getLogger(LoginController.class.getName());
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{   
	        doPost(request, response);
	}
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String forward = "";
		Accounts accounts = new Accounts();
		accounts.setUsername(username);
		accounts.setPassword(password);
		
		try {
			LoginDao loginDao = new LoginDao();
			accounts = loginDao.login(accounts);
						
			if (accounts.isValid()) {
				HttpSession session = request.getSession(true);
				String fullname = accounts.getFullname();
				String userLevel = accounts.getUserLevel();
				String section = accounts.getSection();
				switch(userLevel) {
					case "administrator":
					session.setAttribute("administrator", "administrator");
					break;
					
					case "user":
					session.setAttribute("user", "user");
					break;
				}
				switch(section) {
					case "receiving":
					session.setAttribute("receiving", "receiving");
					forward = "receiving";
					break;
					
					case "evaluation":
					session.setAttribute("evaluation", "evaluation");
					forward = "evaluation";
					break;
					
					case "releasing":
					session.setAttribute("releasing", "releasing");
					forward = "releasing";
					break;
					
					case "printing":
					session.setAttribute("printing", "printing");
					forward = "printing";
					break;
					
					case "finalEvaluation":
						session.setAttribute("finalEvaluation", "finalEvaluation");
						forward = "finalEvaluation";
						break;
				}
				String redirectToPage = this.gson.toJson(forward);
				session.setAttribute("nameOfUser", fullname);	
		        response.getWriter().write(redirectToPage);
			}else {	
				String unableToLogin = this.gson.toJson("loginFailed");
		        response.getWriter().write(unableToLogin);
			}
		}catch(Exception e){
			logger.fatal("Unable to connect to database in LoginController " + e.getMessage());
			logger.error("Cause is " + e.getCause());
		}
	}
}


