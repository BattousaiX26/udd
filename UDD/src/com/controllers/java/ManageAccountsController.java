package com.controllers.java;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.dao.java.ManageAccountsDao;
import com.entities.java.Accounts;


@WebServlet("/JSP/manageAccounts")
public class ManageAccountsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ManageAccountsDao ma;
	List<Accounts> userItems;
	private static final Logger log = Logger.getLogger(ManageAccountsController.class);
	
	public ManageAccountsController() {
        super();
        ma = new ManageAccountsDao();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String forward = "";
		
		if(action.equalsIgnoreCase("view")) {
			try {		
				userItems = ma.getUsers();
				forward = "/JSP/userList.jsp";
				request.setAttribute("userItems", userItems);		
			} catch (Exception e) {
				log.error("Error in getting users on ManageAccountsController \n" + e.getMessage());
				log.error("Cause " + e.getCause());
			}
		}else if(action.equalsIgnoreCase("edit")) {
			try {
				int sid = Integer.parseInt(request.getParameter("id"));
				Accounts accounts = ma.getUserDetails(sid);
				forward = "/JSP/editUser.jsp";
				request.setAttribute("userDetails", accounts);	
			} catch (Exception e) {
				log.error("Error in getUserDetails on ManageAccountsController \n" + e.getMessage());
				log.error("Cause " + e.getCause());
			}
		}else if(action.equalsIgnoreCase("delete")) {
			try {
				int sid = Integer.parseInt(request.getParameter("id"));
				ma.deleteUser(sid);
				userItems = ma.getUsers();
				forward = "/JSP/userList.jsp";
				request.setAttribute("userItems", userItems);			
			} catch (Exception e) {
				log.error("Error in getUsers on ManageAccountsController \n" + e.getMessage());
				log.error("Cause" + e.getCause());
			}
		}
		RequestDispatcher rd = request.getRequestDispatcher(forward);rd.forward(request, response);	
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		int sid = Integer.parseInt(id);
		String username = request.getParameter("username");
		String fullname = request.getParameter("fullname");
		String userLevel = request.getParameter("userLevel");
		String section = request.getParameter("section");
		String status = request.getParameter("status");
		
		Accounts accounts = new Accounts();
		accounts.setId(sid);
		accounts.setUsername(username);
		accounts.setFullname(fullname);
		accounts.setUserLevel(userLevel);
		accounts.setSection(section);
		accounts.setStatus(status);
		
		try {
			boolean updateUserInfo = ma.updateUserInfo(accounts);
			if(updateUserInfo == true) {
				request.setAttribute("isSaved", "Successfully updated user info!");
			}else {
				request.setAttribute("notSaved", "Unable to updated user info!");
			}
			RequestDispatcher rd = request.getRequestDispatcher("/JSP/editUser.jsp"); rd.forward(request, response);	
		} catch (Exception e) {
			log.error("Error in updateUserInfo on ManageAccountsController \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}
	}
}

