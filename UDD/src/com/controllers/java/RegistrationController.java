package com.controllers.java;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.dao.java.RegistrationDao;
import com.entities.java.Accounts;

/**
 * Servlet implementation class RegistrationController
 */
@WebServlet("/register")
public class RegistrationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(RegistrationController.class);
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirm_password");
		String fullname = request.getParameter("fullname");
		String userLevel = request.getParameter("user_level");
		String section = request.getParameter("section");
		
		
		RegistrationDao registration = new RegistrationDao();
		boolean isExisting = registration.userExisting(username);
		
		//check if user exists
		if(isExisting==true) {
			 response.getWriter().write("userExisting");	
		}else {
			try {	
				if(password.equals(confirmPassword)) {
					Accounts accounts = new Accounts();
					accounts.setUsername(username);
					accounts.setPassword(password);
					accounts.setFullname(fullname);
					accounts.setUserLevel(userLevel);
					accounts.setSection(section);
												
					try {
						boolean isSaved = registration.isSaved(accounts);
						
						if(isSaved==true) {
							response.getWriter().write("registrationSuccessful");
						}
					} catch (SQLException e) {
						log.error("Error in saving RegistrationController \n" + e.getMessage());
						log.error("Cause " + e.getCause());
					}
				}else {
					response.getWriter().write("registrationFailed");
				}
			} catch (Exception e) {
				log.error("Error on RegistrationController \n" + e);
				log.error("Cause" + e.getCause());
			}		
		}
	}		
}	
