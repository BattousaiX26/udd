package com.dao.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.database.java.ConnectionManager;
import com.entities.java.Accounts;
import com.utilities.java.EncryptDecrypt;

public class LoginDao {
	final static Logger logger = LogManager.getLogger(LoginDao.class.getName());
	
	 public Accounts login(Accounts accounts) { 
		 String username = accounts.getUsername(); 
		 String password = accounts.getPassword();
		 String passwordQuery = "select * from tbl_users where username=?"; 
	     String passwordEncrypted="";
		 
		 try { 
			 ConnectionManager con = new ConnectionManager();
			 Connection connect = con.getConnection();			 
			 PreparedStatement ps = connect.prepareStatement(passwordQuery);
			 ps.setString(1, username);
			 ResultSet rs = ps.executeQuery();	 
			 if(rs.next()){
				 passwordEncrypted = rs.getString("password");
			 }
			 byte[] original = EncryptDecrypt.decryptValue(passwordEncrypted);		
	         String decryptedPassword = new String(original);
			 String status = rs.getString("status");
	         boolean checker = false;
	         if(password.equals(decryptedPassword) && status.equals("confirmed") ){
	        	 checker = true;
		 	 }
			 if (checker==false) { 
				 accounts.setValid(false); 
			}else if (checker==true) { 		
				 accounts.setValid(true);
				 accounts.setUserLevel(rs.getString("user_level"));
				 accounts.setFullname(rs.getString("fullname"));
				 accounts.setSection(rs.getString("section"));
			} 
		} catch (Exception e) {
			logger.error("Error on LoginDao \n" + e.getMessage());
			logger.error("Cause " + e.getCause());
		}	 
		return accounts; 
	 }
}
