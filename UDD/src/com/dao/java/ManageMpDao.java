package com.dao.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.database.java.ConnectionManager;
import com.entities.java.LocationalClearance;
import com.utilities.java.MpUtilities;

public class ManageMpDao {
	
	private static final String applicationStatus = "RECEIVED";
	private static final Logger log = Logger.getLogger(ManageMpDao.class);
	
	public List<LocationalClearance> search(LocationalClearance lc) throws SQLException{
	    List<LocationalClearance> searchedItems = new ArrayList<LocationalClearance>();
	    Connection connection = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
		try {
			ConnectionManager connect = new ConnectionManager();
			connection = connect.getConnection();
			ps = connection.prepareStatement("select * from tbl_records WHERE MP_NO LIKE ? order by id desc");
			ps.setString(1, "%"+ lc.getSearchedItem() + "%");
			rs = ps.executeQuery();
			
			while(rs.next()) {
				LocationalClearance locational = new LocationalClearance();
				locational.setId(rs.getInt("id")); 
				locational.setCpdoNo(rs.getString("CPDO_NO"));
				locational.setDateApplied(rs.getString("DATE_APPLIED"));
				locational.setMpNo(rs.getString("MP_NO"));
				locational.setDatePosted(rs.getString("DATE_POSTED"));
				locational.setTimePosted(rs.getString("TIME_POSTED"));
				locational.setZoningClass(rs.getString("ZONING_CLASS"));
				locational.setEvaluation(rs.getString("EVALUATION"));
				locational.setVerifier(rs.getString("VERIFIER"));
				locational.setZoningOfficial(rs.getString("ZONING_OFFICIAL"));
				locational.setDateProcessed(rs.getString("DATE_PROCESSED"));
				locational.setTimeProcessed(rs.getString("TIME_PROCESSED"));
				locational.setRemarks(rs.getString("REMARKS"));
				locational.setLotArea(rs.getInt("LOT_AREA"));
				locational.setEncodedBy(rs.getString("ENCODED_BY"));
				locational.setPisc(rs.getString("PISC"));
				locational.setRightOverLand(rs.getString("RIGHT_OVER_LAND"));
				locational.setLandRate(rs.getString("LANDRATE"));
				locational.setBusinessName(rs.getString("BUSINESS_NAME"));
				locational.setTaxPayerName(rs.getString("TAXPAYERS_NAME"));
				locational.setAddress(rs.getString("ADDRESS"));
				locational.setNoOfEmployees(rs.getInt("NO_OF_EMPLOYEES"));
				double val = rs.getDouble("CAPITALIZATION");
				String formattedCapital = MpUtilities.removeScientificNotation(val);
				locational.setCapitalization(formattedCapital); 	
				locational.setBusinessActivityMainTile1("BUSINESS_ACTIVITY_MAINTILE1");
				locational.setBusinessActivitySubTile1(rs.getString("BUSINESS_ACTIVITY_SUBTILE1"));
				locational.setBusinessActivityMainTile2("BUSINESS_ACTIVITY_MAINTILE2");
				locational.setBusinessActivitySubTile2(rs.getString("BUSINESS_ACTIVITY_SUBTILE2"));
				locational.setBusinessActivityMainTile3("BUSINESS_ACTIVITY_MAINTILE3");
				locational.setBusinessActivitySubTile3(rs.getString("BUSINESS_ACTIVITY_SUBTILE3"));
				locational.setBusinessActivityMainTile4("BUSINESS_ACTIVITY_MAINTILE4");
				locational.setBusinessActivitySubTile4(rs.getString("BUSINESS_ACTIVITY_SUBTILE4"));
				locational.setBusinessActivityMainTile5("BUSINESS_ACTIVITY_MAINTILE5");
				locational.setBusinessActivitySubTile5(rs.getString("BUSINESS_ACTIVITY_SUBTILE5"));
				searchedItems.add(locational);
			}	
		} catch (Exception e) {
			log.error("Error on search in ManageMpDao \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}finally {
			try {if(rs!=null) {rs.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
		  return searchedItems;
	}
	public List<LocationalClearance> getTableValues(LocationalClearance lc) throws SQLException{
	    List<LocationalClearance> searchedItems = new ArrayList<LocationalClearance>();
	    Connection connection = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
		try {
			ConnectionManager connect = new ConnectionManager();
			connection = connect.getConnection();
			ps = connection.prepareStatement("select * from tbl_records WHERE id=?");
			ps.setInt(1, lc.getId());
			rs = ps.executeQuery();
			while(rs.next()) {
				LocationalClearance locational = new LocationalClearance();
				locational.setId(rs.getInt("id"));
				locational.setCpdoNo(rs.getString("CPDO_NO"));
				locational.setDateApplied(rs.getString("DATE_APPLIED"));
				locational.setMpNo(rs.getString("MP_NO"));
				locational.setStatus(rs.getString("STATUS"));
				locational.setDatePosted(rs.getString("DATE_POSTED"));
				locational.setTimePosted(rs.getString("TIME_POSTED"));
				locational.setZoningClass(rs.getString("ZONING_CLASS"));
				locational.setEvaluation(rs.getString("EVALUATION"));
				locational.setVerifier(rs.getString("VERIFIER"));
				locational.setZoningOfficial(rs.getString("ZONING_OFFICIAL"));
				locational.setDateProcessed(rs.getString("DATE_PROCESSED"));
				locational.setTimeProcessed(rs.getString("TIME_PROCESSED"));
				locational.setRemarks(rs.getString("REMARKS"));
				locational.setLotArea(rs.getDouble("LOT_AREA"));
				locational.setEncodedBy(rs.getString("ENCODED_BY"));
				locational.setPisc(rs.getString("PISC"));
				locational.setRightOverLand(rs.getString("RIGHT_OVER_LAND"));
				locational.setLandRate(rs.getString("LANDRATE"));
				locational.setBusinessName(rs.getString("BUSINESS_NAME"));
				locational.setTaxPayerName(rs.getString("TAXPAYERS_NAME"));
				locational.setAddress(rs.getString("ADDRESS"));
				locational.setNoOfEmployees(rs.getInt("NO_OF_EMPLOYEES"));
				double val = rs.getDouble("CAPITALIZATION");
				String formattedCapital = MpUtilities.removeScientificNotation(val);
				locational.setCapitalization(formattedCapital);
				locational.setBusinessActivityMainTile1("BUSINESS_ACTIVITY_MAINTILE1");
				locational.setBusinessActivitySubTile1(rs.getString("BUSINESS_ACTIVITY_SUBTILE1"));
				locational.setBusinessActivityMainTile2("BUSINESS_ACTIVITY_MAINTILE2");
				locational.setBusinessActivitySubTile2(rs.getString("BUSINESS_ACTIVITY_SUBTILE2"));
				locational.setBusinessActivityMainTile3("BUSINESS_ACTIVITY_MAINTILE3");
				locational.setBusinessActivitySubTile3(rs.getString("BUSINESS_ACTIVITY_SUBTILE3"));
				locational.setBusinessActivityMainTile4("BUSINESS_ACTIVITY_MAINTILE4");
				locational.setBusinessActivitySubTile4(rs.getString("BUSINESS_ACTIVITY_SUBTILE4"));
				locational.setBusinessActivityMainTile5("BUSINESS_ACTIVITY_MAINTILE5");
				locational.setBusinessActivitySubTile5(rs.getString("BUSINESS_ACTIVITY_SUBTILE5"));
				searchedItems.add(locational);
			}	
		} catch (Exception e) {
			log.error("Unable to getTableValues on ManageMpDao \n" + e.getMessage() );
			log.error("Cause " + e.getCause());
		}finally {
			try {if(rs!=null) {rs.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
		  return searchedItems;
	}
	public List<LocationalClearance> saveToMasterRecords(LocationalClearance lc) throws SQLException{
		List<LocationalClearance> dataWithTransactionNumber = new ArrayList<LocationalClearance>();
		Connection connection = null;
	    PreparedStatement ps = null;
	    String insertQuery = "insert into tbl_master_records(TRANSACTION_NUMBER,BUSINESS_NAME,TAXPAYERS_NAME,NO_OF_EMPLOYEES_MALE,NO_OF_EMPLOYEES_FEMALE,SEX,RIGHT_OVER_LAND,"
	    		+ "LAND_RATE,CAPITALIZATION,ADDRESS,BARANGAY,DISTRICT,BUSINESS_ACTIVITY_MAINTILE1,BUSINESS_ACTIVITY_SUBTILE1,BUSINESS_ACTIVITY_MAINTILE2,BUSINESS_ACTIVITY_SUBTILE2,"
	    		+ "BUSINESS_ACTIVITY_MAINTILE3,BUSINESS_ACTIVITY_SUBTILE3,BUSINESS_ACTIVITY_MAINTILE4,BUSINESS_ACTIVITY_SUBTILE4,BUSINESS_ACTIVITY_MAINTILE5,BUSINESS_ACTIVITY_SUBTILE5,"
	    		+ "MP_NUMBER,CPDO_NUMBER,DATE_APPLIED,DATE_VALID,STATUS,LOT_AREA,PISC,ZONING_CLASS,EVALUATION,ZONING_OFFICIAL,REMARKS,OTHER_REMARKS,ENCODED_BY,YEAR_ADDED,VERIFIER,"
	    		+ "BUSINESS_AREA, APPLICATION_STATUS)"
	    		+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    String transactionNumber = MpUtilities.generateTransactionNumber();
	    String currentYear = MpUtilities.getCurrentYear();
	    String currentDate = MpUtilities.getCurrentDate();
	    String cpdoNo = lc.getCpdoNo();
	    String zoningClass = lc.getZoningClass();
	    String verifier = lc.getVerifier();
	    String status = lc.getStatus();
	    String remarks = lc.getRemarks();
	   
		try {
			ConnectionManager connect = new ConnectionManager();
			connection = connect.getConnection();		
			ps = connection.prepareStatement(insertQuery);
			ps.setString(1, transactionNumber);
			ps.setString(2, lc.getBusinessName());
			ps.setString(3, lc.getTaxPayerName());
			ps.setInt(4, lc.getNoOfEmployeesMale());
			ps.setInt(5, lc.getNoOfEmployeesFemale());
			ps.setString(6, lc.getSex());
			ps.setString(7, lc.getRightOverLand());
			ps.setString(8, lc.getLandRate());
			ps.setString(9, lc.getCapitalization());
			ps.setString(10, lc.getAddress());
			ps.setString(11, lc.getBarangay());
			ps.setInt(12, lc.getDistrict());
			ps.setString(13, lc.getBusinessActivityMainTile1());
			ps.setString(14, lc.getBusinessActivitySubTile1());
			ps.setString(15, lc.getBusinessActivityMainTile2());
			ps.setString(16, lc.getBusinessActivitySubTile2());
			ps.setString(17, lc.getBusinessActivityMainTile3());
			ps.setString(18, lc.getBusinessActivitySubTile3());
			ps.setString(19, lc.getBusinessActivityMainTile4());
			ps.setString(20, lc.getBusinessActivitySubTile4());
			ps.setString(21, lc.getBusinessActivityMainTile5());
			ps.setString(22, lc.getBusinessActivitySubTile5());
			ps.setString(23, lc.getMpNo());
			ps.setString(24, lc.getCpdoNo());
			ps.setString(25, currentDate);
			ps.setInt(26, lc.getDateValid());
			ps.setString(27, lc.getStatus());
			ps.setDouble(28, lc.getLotArea());
			ps.setString(29, lc.getPisc());
			ps.setString(30, lc.getZoningClass());
			ps.setString(31, lc.getEvaluation());
			ps.setString(32, lc.getZoningOfficial());
			ps.setString(33, lc.getRemarks());
			ps.setString(34, lc.getOtherRemarks());
			ps.setString(35, lc.getEncodedBy());
			ps.setString(36, currentYear);
			ps.setString(37, lc.getVerifier());
			ps.setDouble(38, lc.getBusinessArea());
			ps.setString(39, applicationStatus);
			int saveChecker = ps.executeUpdate();
			if(saveChecker>0) {
				LocationalClearance locational = new LocationalClearance();
				locational.setTransactionNumber(transactionNumber);
				locational.setMpNo(lc.getMpNo());
				locational.setBusinessName(lc.getBusinessName());
				locational.setDateApplied(currentDate);
				locational.setCpdoNo(cpdoNo);
				locational.setZoningClass(zoningClass);
				locational.setVerifier(verifier);
				locational.setStatus(status);
				locational.setRemarks(remarks);			
				dataWithTransactionNumber.add(locational);
			}
		} catch (Exception e) {
			log.error("Unable to saveToMasterRecords on ManageMpDao \n" + e.getMessage() );
			log.error("Cause " + e.getCause());
		}finally {
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
		return dataWithTransactionNumber;
	}
}
