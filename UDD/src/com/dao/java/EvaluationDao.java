package com.dao.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.database.java.ConnectionManager;
import com.entities.java.LocationalClearance;
import com.utilities.java.MpUtilities;
import com.utilities.java.QrCode;

public class EvaluationDao {
	
	private static final String APPLICATION_STATUS = "EVALUATED";
	private static final Logger log = Logger.getLogger(EvaluationDao.class);
	 
	public List<LocationalClearance> getItemsToBeEvaluated(LocationalClearance lc) throws SQLException{
		 List<LocationalClearance> itemsTobeEvaluated = new ArrayList<LocationalClearance>();
		 String query="select * from tbl_master_records where TRANSACTION_NUMBER=?";
		 Connection connection = null;
		 PreparedStatement ps = null;
		 ResultSet rs = null;
		 try {	
			 ConnectionManager connect = new ConnectionManager();
			 connection = connect.getConnection();
			 ps = connection.prepareStatement(query);
			 ps.setString(1, lc.getTransactionNumber());
			 rs = ps.executeQuery();
			 while(rs.next()) {
				 LocationalClearance locational = new LocationalClearance();
				 locational.setId(rs.getInt("id"));
				 locational.setTransactionNumber(rs.getString("TRANSACTION_NUMBER"));
				 locational.setBusinessName(rs.getString("BUSINESS_NAME"));
				 locational.setTaxPayerName(rs.getString("TAXPAYERS_NAME"));
				 locational.setNoOfEmployeesMale(rs.getInt("NO_OF_EMPLOYEES_MALE"));
				 locational.setNoOfEmployeesFemale(rs.getInt("NO_OF_EMPLOYEES_FEMALE"));
				 locational.setSex(rs.getString("SEX"));
				 locational.setRightOverLand(rs.getString("RIGHT_OVER_LAND"));
				 locational.setLandRate(rs.getString("LAND_RATE"));		 
				 String capitalization = rs.getString("CAPITALIZATION");
				 if(capitalization.isEmpty()) {			
					 locational.setCapitalization(capitalization);		
				 }else {
					double capital = Double.parseDouble(capitalization);
				 	locational.setCapitalization(MpUtilities.removeScientificNotation(capital));	
				 }	
				 locational.setAddress(rs.getString("ADDRESS"));
				 locational.setBarangay(rs.getString("BARANGAY"));
				 locational.setDistrict(rs.getInt("DISTRICT"));
				 locational.setBusinessActivityMainTile1(rs.getString("BUSINESS_ACTIVITY_MAINTILE1"));
				 locational.setBusinessActivitySubTile1(rs.getString("BUSINESS_ACTIVITY_SUBTILE1"));	
				 locational.setMpNo(rs.getString("MP_NUMBER"));
				 locational.setCpdoNo(rs.getString("CPDO_NUMBER"));
				 locational.setDateApplied(rs.getString("DATE_APPLIED"));
				 locational.setDateValid(rs.getInt("DATE_VALID"));
				 locational.setStatus(rs.getString("STATUS"));
				 locational.setVerifier(rs.getString("VERIFIER"));
				 locational.setLotArea(rs.getDouble("LOT_AREA"));
				 locational.setPisc(rs.getString("PISC"));
				 locational.setZoningClass(rs.getString("ZONING_CLASS"));
				 locational.setEvaluation(rs.getString("EVALUATION"));
				 locational.setZoningOfficial(rs.getString("ZONING_OFFICIAL"));
				 locational.setRemarks(rs.getString("REMARKS"));
				 locational.setOtherRemarks(rs.getString("OTHER_REMARKS"));
				 locational.setRemarks2(rs.getString("REMARKS2"));
				 locational.setOtherRemarks2(rs.getString("OTHER_REMARKS2"));
				 locational.setRemarks3(rs.getString("REMARKS3"));
				 locational.setOtherRemarks3(rs.getString("OTHER_REMARKS3"));
				 locational.setEncodedBy(rs.getString("ENCODED_BY"));
				 locational.setEvaluationOfFacts(rs.getString("EVALUATION_OF_FACTS"));
				 locational.setDateIssued(rs.getString("DATE_ISSUED"));
				 locational.setValidUntil(rs.getString("VALID_UNTIL"));
				 locational.setTctNo(rs.getString("TCT_NO"));
				 locational.setBusinessArea(rs.getDouble("BUSINESS_AREA"));
				 locational.setNoticeOfAction(rs.getString("NOTICE_OF_ACTION"));
				 locational.setOtherNoticeOfAction(rs.getString("OTHER_NOTICE_OF_ACTION"));
				 locational.setApprovedBy(rs.getString("APPROVED_BY"));
				 locational.setApprovedForPrinting(rs.getString("APPROVED_FOR_PRINTING"));
				 //locational.setQrCode(rs.getBytes("QR_CODE"));
				 byte[] qrFromDatabase = rs.getBytes("QR_CODE");
				 if(qrFromDatabase !=null) {
					 byte[] encodeBase64 = Base64.encodeBase64(qrFromDatabase);
					 String base64Encoded = new String(encodeBase64, "UTF-8");
					 locational.setImageQrCode(base64Encoded);
				 }			
				 itemsTobeEvaluated.add(locational);
			 }
		} catch (Exception e) {
			log.error("Unable to getItemsForEvaluation on EvaluationDao \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}finally {
			try {if(rs!=null) {rs.close();}}catch (Exception e) {e.getMessage();}
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.getMessage();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.getMessage();}
		}
		 return itemsTobeEvaluated;
	}	
	public boolean isEvaluationSaved(LocationalClearance lc) throws SQLException{
		 boolean isSaved = false;
		 String updateTableSQL = "update tbl_master_records set BUSINESS_NAME=?, TAXPAYERS_NAME=?, NO_OF_EMPLOYEES_MALE=?, NO_OF_EMPLOYEES_FEMALE=?, SEX=?, RIGHT_OVER_LAND=?, LAND_RATE=?, "
		 		+ "CAPITALIZATION=?, ADDRESS=?, BARANGAY=?, DISTRICT=?, BUSINESS_ACTIVITY_MAINTILE1=?, BUSINESS_ACTIVITY_SUBTILE1=?, "
		 		+ "CPDO_NUMBER=?, DATE_VALID=?, STATUS=?, VERIFIER=?, LOT_AREA=?, PISC=?, ZONING_CLASS=?, EVALUATION=?, ZONING_OFFICIAL=?, REMARKS=?, OTHER_REMARKS=?, "
		 		+ "REMARKS2=?, OTHER_REMARKS2=?, REMARKS3=?, OTHER_REMARKS3=?, EVALUATION_OF_FACTS=?, DATE_ISSUED=?, VALID_UNTIL=?, TCT_NO=?, BUSINESS_AREA=?, NOTICE_OF_ACTION=?, "
		 		+ "OTHER_NOTICE_OF_ACTION=?, QR_CODE=?, TOTAL=?, TOTAL_IN_WORDS=?, APPLICATION_STATUS=?, APPROVED_BY=?, APPROVED_FOR_PRINTING=? where id = ?";
		 Connection connection = null;
		 PreparedStatement ps = null;
		 
		 String transactionNumber = lc.getTransactionNumber();
		 String businessName = lc.getBusinessName();
		 String address = lc.getAddress();
		 String zoningClass = lc.getZoningClass();
		 int district = lc.getDistrict();
		 String barangay = lc.getBarangay();
		 String mpNo = lc.getMpNo();
		 String qrKey = "TRANSACTION NUMBER: " + transactionNumber + "\nMPNUMBER is: " + mpNo + "\nBUSINESS NAME: " + businessName + "\nADDRESS: " + address + "\nDISTRICT: " 
		 + district + "\nBARANGAY: " + barangay + "\nZONING CLASS: " + zoningClass;
	
		 try {
			 String dateIssued = lc.getDateIssued();
			 int dateValid = lc.getDateValid();
			 String validUntil = MpUtilities.generateValidUntil(dateIssued, dateValid);
			 QrCode qc = new QrCode();
			 byte[] qrCode = qc.getQRCodeImage(qrKey);
			 ConnectionManager connect = new ConnectionManager();
			 connection = connect.getConnection();
			 ps = connection.prepareStatement(updateTableSQL);
			 ps.setString(1, lc.getBusinessName());
			 ps.setString(2, lc.getTaxPayerName());
			 ps.setInt(3, lc.getNoOfEmployeesMale());
			 ps.setInt(4, lc.getNoOfEmployeesFemale());
			 ps.setString(5, lc.getSex());
			 ps.setString(6, lc.getRightOverLand());
			 ps.setString(7, lc.getLandRate());
			 ps.setString(8, lc.getCapitalization());
			 ps.setString(9, lc.getAddress());
			 ps.setString(10, lc.getBarangay());
			 ps.setInt(11, lc.getDistrict());
			 ps.setString(12, lc.getBusinessActivityMainTile1());
			 ps.setString(13, lc.getBusinessActivitySubTile1());
			 ps.setString(14, lc.getCpdoNo());
			 ps.setInt(15, lc.getDateValid());
			 ps.setString(16, lc.getStatus());
			 ps.setString(17, lc.getVerifier());
			 ps.setDouble(18, lc.getLotArea());
			 ps.setString(19, lc.getPisc());
			 ps.setString(20, lc.getZoningClass());
			 ps.setString(21, lc.getEvaluation());
			 ps.setString(22, lc.getZoningOfficial());
			 ps.setString(23, lc.getRemarks());
			 ps.setString(24, lc.getOtherRemarks());
			 ps.setString(25, lc.getRemarks2());
			 ps.setString(26, lc.getOtherRemarks2());
			 ps.setString(27, lc.getRemarks3());
			 ps.setString(28, lc.getOtherRemarks3());
			 ps.setString(29, lc.getEvaluationOfFacts());
			 ps.setString(30, lc.getDateIssued());
			 ps.setString(31, validUntil);
			 ps.setString(32, lc.getTctNo());
			 ps.setDouble(33, lc.getBusinessArea());
			 ps.setString(34, lc.getNoticeOfAction());
			 ps.setString(35, lc.getOtherNoticeOfAction());
			 ps.setBytes(36, qrCode);
			 ps.setDouble(37, lc.getTotal());
			 ps.setString(38, lc.getTotalInWords());
			 ps.setString(39, APPLICATION_STATUS);
			 ps.setString(40, lc.getApprovedBy());
			 ps.setString(41, lc.getApprovedForPrinting());
			 ps.setInt(42, lc.getId());
			 
			 int checker = ps.executeUpdate(); 
			 
			 if(checker>0) {
				 isSaved = true;
			 }
		} catch (Exception e) {
			log.error("Unable to save on EvaluationDao \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}finally {		
			try {if(ps!=null) {ps.close();}}catch (Exception e) {log.error(e.getMessage());}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.getMessage();}
		}
		 return isSaved;
	}	
}
