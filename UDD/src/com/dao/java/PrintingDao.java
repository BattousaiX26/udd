package com.dao.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.database.java.ConnectionManager;
import com.entities.java.LocationalClearance;
import com.utilities.java.MpUtilities;

public class PrintingDao {
	private static final Logger log = Logger.getLogger(PrintingDao.class);
	
	public List<LocationalClearance> getItemsForPrinting(LocationalClearance lc) throws SQLException{
		 List<LocationalClearance> itemsForPrinting = new ArrayList<LocationalClearance>();
		 String query="select * from tbl_master_records where TRANSACTION_NUMBER=?";
		 Connection connection = null;
		 PreparedStatement ps = null;
		 ResultSet rs = null;
		 try {	
			 ConnectionManager connect = new ConnectionManager();
			 connection = connect.getConnection();
			 ps = connection.prepareStatement(query);
			 ps.setString(1, lc.getTransactionNumber());
			 rs = ps.executeQuery();
			 while(rs.next()) {
				 LocationalClearance locational = new LocationalClearance();
				 locational.setId(rs.getInt("id"));
				 locational.setTransactionNumber(rs.getString("TRANSACTION_NUMBER"));
				 locational.setBusinessName(rs.getString("BUSINESS_NAME"));
				 locational.setTaxPayerName(rs.getString("TAXPAYERS_NAME"));
				 locational.setNoOfEmployeesMale(rs.getInt("NO_OF_EMPLOYEES_MALE"));
				 locational.setNoOfEmployeesFemale(rs.getInt("NO_OF_EMPLOYEES_FEMALE"));
				 locational.setSex(rs.getString("SEX"));
				 locational.setRightOverLand(rs.getString("RIGHT_OVER_LAND"));
				 locational.setLandRate(rs.getString("LAND_RATE"));		 
				 String capitalization = rs.getString("CAPITALIZATION");
				 if(capitalization.isEmpty()) {			
					 locational.setCapitalization(capitalization);		
				 }else {
					double capital = Double.parseDouble(capitalization);
				 	locational.setCapitalization(MpUtilities.removeScientificNotation(capital));	
				 }	
				 locational.setAddress(rs.getString("ADDRESS"));
				 locational.setBarangay(rs.getString("BARANGAY"));
				 locational.setDistrict(rs.getInt("DISTRICT"));
				 locational.setBusinessActivityMainTile1(rs.getString("BUSINESS_ACTIVITY_MAINTILE1"));
				 locational.setBusinessActivitySubTile1(rs.getString("BUSINESS_ACTIVITY_SUBTILE1"));	
				 locational.setMpNo(rs.getString("MP_NUMBER"));
				 locational.setCpdoNo(rs.getString("CPDO_NUMBER"));
				 locational.setDateApplied(rs.getString("DATE_APPLIED"));
				 locational.setDateValid(rs.getInt("DATE_VALID"));
				 locational.setStatus(rs.getString("STATUS"));
				 locational.setVerifier(rs.getString("VERIFIER"));
				 locational.setLotArea(rs.getDouble("LOT_AREA"));
				 locational.setPisc(rs.getString("PISC"));
				 locational.setZoningClass(rs.getString("ZONING_CLASS"));
				 locational.setEvaluation(rs.getString("EVALUATION"));
				 locational.setZoningOfficial(rs.getString("ZONING_OFFICIAL"));
				 locational.setRemarks(rs.getString("REMARKS"));
				 locational.setOtherRemarks(rs.getString("OTHER_REMARKS"));
				 locational.setRemarks2(rs.getString("REMARKS2"));
				 locational.setOtherRemarks2(rs.getString("OTHER_REMARKS2"));
				 locational.setRemarks3(rs.getString("REMARKS3"));
				 locational.setOtherRemarks3(rs.getString("OTHER_REMARKS3"));
				 locational.setEncodedBy(rs.getString("ENCODED_BY"));
				 locational.setEvaluationOfFacts(rs.getString("EVALUATION_OF_FACTS"));
				 locational.setNoticeOfAction("NOTICE_OF_ACTION");
				 locational.setDateIssued(rs.getString("DATE_ISSUED"));
				 locational.setValidUntil(rs.getString("VALID_UNTIL"));
				 locational.setTctNo(rs.getString("TCT_NO"));
				 locational.setBusinessArea(rs.getDouble("BUSINESS_AREA"));
				 locational.setNoticeOfAction(rs.getString("NOTICE_OF_ACTION"));
				 locational.setOtherNoticeOfAction(rs.getString("OTHER_NOTICE_OF_ACTION"));
				 locational.setApprovedBy(rs.getString("APPROVED_BY"));
				 locational.setApprovedForPrinting(rs.getString("APPROVED_FOR_PRINTING"));
				 //locational.setQrCode(rs.getBytes("QR_CODE"));
				 byte[] qrFromDatabase = rs.getBytes("QR_CODE");
				 if(qrFromDatabase !=null) {
					 byte[] encodeBase64 = Base64.encodeBase64(qrFromDatabase);
					 String base64Encoded = new String(encodeBase64, "UTF-8");
					 locational.setImageQrCode(base64Encoded);
				 }			
				 itemsForPrinting.add(locational);
			 }
		} catch (Exception e) {
			log.error("Unable to getItemsForEvaluation on EvaluationDao \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}finally {
			try {if(rs!=null) {rs.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
		 return itemsForPrinting;
	}	
}
