package com.dao.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.database.java.ConnectionManager;
import com.entities.java.LocationalClearance;
import com.utilities.java.MpUtilities;


public class ReceivingDao {
	
	private static final String APPLICATION_STATUS = "RECEIVED";
	private static final Logger log = Logger.getLogger(ReceivingDao.class);
	
	public List<LocationalClearance> saveToMasterRecords(LocationalClearance lc) throws SQLException{
		List<LocationalClearance> dataWithTransactionNumber = new ArrayList<LocationalClearance>();
		Connection connection = null;
	    PreparedStatement ps = null;
	    String insertQuery = "insert into tbl_master_records(TRANSACTION_NUMBER,BUSINESS_NAME,TAXPAYERS_NAME,NO_OF_EMPLOYEES_MALE,NO_OF_EMPLOYEES_FEMALE,SEX,RIGHT_OVER_LAND,LAND_RATE,"
	    		+ "CAPITALIZATION,ADDRESS,BARANGAY,DISTRICT,BUSINESS_ACTIVITY_MAINTILE1,BUSINESS_ACTIVITY_SUBTILE1,BUSINESS_ACTIVITY_MAINTILE2,BUSINESS_ACTIVITY_SUBTILE2,"
	    		+ "BUSINESS_ACTIVITY_MAINTILE3,BUSINESS_ACTIVITY_SUBTILE3,BUSINESS_ACTIVITY_MAINTILE4,BUSINESS_ACTIVITY_SUBTILE4,BUSINESS_ACTIVITY_MAINTILE5,BUSINESS_ACTIVITY_SUBTILE5,"
	    		+ "MP_NUMBER,CPDO_NUMBER,DATE_APPLIED,DATE_VALID,STATUS,VERIFIER,LOT_AREA,PISC,ZONING_CLASS,EVALUATION,ZONING_OFFICIAL,REMARKS,OTHER_REMARKS,ENCODED_BY,YEAR_ADDED,"
	    		+ "BUSINESS_AREA, APPLICATION_STATUS)"
	    		+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			String transactionNumber = MpUtilities.generateTransactionNumber();
		    String currentYear = MpUtilities.getCurrentYear();
		    String currentDate = MpUtilities.getCurrentDate();	
			ConnectionManager connect = new ConnectionManager();
			connection = connect.getConnection();		
			ps = connection.prepareStatement(insertQuery);
			ps.setString(1, transactionNumber);
			ps.setString(2, lc.getBusinessName());
			ps.setString(3, lc.getTaxPayerName());
			ps.setInt(4, lc.getNoOfEmployeesMale());
			ps.setInt(5, lc.getNoOfEmployeesFemale());
			ps.setString(6, lc.getSex());
			ps.setString(7, lc.getRightOverLand());
			ps.setString(8, lc.getLandRate());
			ps.setString(9, lc.getCapitalization());
			ps.setString(10, lc.getAddress());
			ps.setString(11, lc.getBarangay());
			ps.setInt(12, lc.getDistrict());
			ps.setString(13, lc.getBusinessActivityMainTile1());
			ps.setString(14, lc.getBusinessActivitySubTile1());
			ps.setString(15, lc.getBusinessActivityMainTile2());
			ps.setString(16, lc.getBusinessActivitySubTile2());
			ps.setString(17, lc.getBusinessActivityMainTile3());
			ps.setString(18, lc.getBusinessActivitySubTile3());
			ps.setString(19, lc.getBusinessActivityMainTile4());
			ps.setString(20, lc.getBusinessActivitySubTile4());
			ps.setString(21, lc.getBusinessActivityMainTile5());
			ps.setString(22, lc.getBusinessActivitySubTile5());
			ps.setString(23, lc.getMpNo());
			ps.setString(24, lc.getCpdoNo());
			ps.setString(25, currentDate);
			ps.setInt(26, lc.getDateValid());
			ps.setString(27, lc.getStatus());
			ps.setString(28, lc.getVerifier());
			ps.setDouble(29, lc.getLotArea());
			ps.setString(30, lc.getPisc());
			ps.setString(31, lc.getZoningClass());
			ps.setString(32, lc.getEvaluation());
			ps.setString(33, lc.getZoningOfficial());
			ps.setString(34, lc.getRemarks());
			ps.setString(35, lc.getOtherRemarks());
			ps.setString(36, lc.getEncodedBy());
			ps.setString(37, currentYear);
			ps.setDouble(38, lc.getBusinessArea());
			ps.setString(39,  APPLICATION_STATUS);
			int status = ps.executeUpdate();
			if(status>0) {
				LocationalClearance locational = new LocationalClearance();
				locational.setTransactionNumber(transactionNumber);
				locational.setMpNo(lc.getMpNo());
				locational.setBusinessName(lc.getBusinessName());
				locational.setDateApplied(currentDate);
				dataWithTransactionNumber.add(locational);
			}
		} catch (Exception e) {
			log.error("Unable to saveToMasterRecords on ReceivingDao \n" + e.getMessage() );
			log.error("Cause " + e.getCause());
		}finally {
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
		return dataWithTransactionNumber;
	}
	public List<LocationalClearance> getRecords(LocationalClearance lc) throws SQLException{
		List<LocationalClearance> dataWithTransactionNumber = new ArrayList<LocationalClearance>();
		Connection connection = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
	    String query = "select top 1 * from tbl_master_records where MP_NUMBER=? order by ID desc ";
	    try {
			ConnectionManager connect = new ConnectionManager();
			connection = connect.getConnection();		
			ps = connection.prepareStatement(query);
			ps.setString(1, lc.getMpNo());
			rs=ps.executeQuery();
			
			while(rs.next()) {
				LocationalClearance locational = new LocationalClearance();
				String transactionNumber = rs.getString("TRANSACTION_NUMBER");locational.setTransactionNumber(transactionNumber);
				String businessName = rs.getString("BUSINESS_NAME");locational.setBusinessName(businessName);
				String dateApplied = rs.getString("DATE_APPLIED");locational.setDateApplied(dateApplied);
				String mpNumber = lc.getMpNo();locational.setMpNo(mpNumber);
				String cpdoNo = rs.getString("CPDO_NUMBER");locational.setCpdoNo(cpdoNo);
				String zoningClass = rs.getString("ZONING_CLASS");locational.setZoningClass(zoningClass);
				String verifier = rs.getString("VERIFIER");locational.setVerifier(verifier);
				String status = rs.getString("STATUS");locational.setStatus(status);
				String remarks = rs.getString("REMARKS");locational.setRemarks(remarks);
				dataWithTransactionNumber.add(locational);
			}
		} catch (Exception e) {
			log.error("Unable to getRecords on ReceivingDao \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		} finally {
			try {if(rs!=null) {rs.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
	    return dataWithTransactionNumber;
	}
}
