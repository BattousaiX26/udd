package com.dao.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.database.java.ConnectionManager;
import com.entities.java.LocationalClearance;

public class ExcelDao {
	
	private static final Logger log = Logger.getLogger(ExcelDao.class);
	
	public int InsertRowInDB(LocationalClearance lc) throws SQLException{
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int status = 0;
		try {
			ConnectionManager connect = new ConnectionManager();
			connection = connect.getConnection();
	        ps=connection.prepareStatement("insert into tbl_records(CPDO_NO,DATE_APPLIED,MP_NO,STATUS,DATE_POSTED,TIME_POSTED,ZONING_CLASS,EVALUATION,VERIFIER,"
	        		+ "ZONING_OFFICIAL,DATE_PROCESSED,TIME_PROCESSED,REMARKS,LOT_AREA,ENCODED_BY,PISC,RIGHT_OVER_LAND,LANDRATE,BUSINESS_NAME,TAXPAYERS_NAME,ADDRESS,"
	        		+ "CAPITALIZATION,BUSINESS_ACTIVITY_MAINTILE1,BUSINESS_ACTIVITY_SUBTILE1,BUSINESS_ACTIVITY_MAINTILE2,BUSINESS_ACTIVITY_SUBTILE2,"
	        		+ "BUSINESS_ACTIVITY_MAINTILE3,BUSINESS_ACTIVITY_SUBTILE3,BUSINESS_ACTIVITY_MAINTILE4,BUSINESS_ACTIVITY_SUBTILE4,"
	        		+ "BUSINESS_ACTIVITY_MAINTILE5,BUSINESS_ACTIVITY_SUBTILE5) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	        ps.setString(1, lc.getCpdoNo());
	        ps.setString(2, lc.getDateApplied());
	        ps.setString(3, lc.getMpNo());
	        ps.setString(4, lc.getStatus());
	        ps.setString(5, lc.getDatePosted());
	        ps.setString(6, lc.getTimePosted());
	        ps.setString(7, lc.getZoningClass());
	        ps.setString(8, lc.getEvaluation());
	        ps.setString(9, lc.getVerifier());
	        ps.setString(10, lc.getZoningOfficial());
	        ps.setString(11, lc.getDateProcessed());
	        ps.setString(12, lc.getTimeProcessed());
	        ps.setString(13, lc.getRemarks());
	        ps.setDouble(14, lc.getLotArea());
	        ps.setString(15, lc.getEncodedBy());
	        ps.setString(16, lc.getPisc());
	        ps.setString(17, lc.getRightOverLand());
	        ps.setString(18, lc.getLandRate());
	        ps.setString(19, lc.getBusinessName());
	        ps.setString(20, lc.getTaxPayerName());
	        ps.setString(21, lc.getAddress());
	        ps.setString(22, lc.getCapitalization());
	        ps.setString(23, lc.getBusinessActivityMainTile1());
	        ps.setString(24, lc.getBusinessActivitySubTile1());
	        ps.setString(25, lc.getBusinessActivityMainTile2());
	        ps.setString(26, lc.getBusinessActivitySubTile2());
	        ps.setString(27, lc.getBusinessActivityMainTile3());
	        ps.setString(28, lc.getBusinessActivitySubTile3());
	        ps.setString(29, lc.getBusinessActivityMainTile4());
	        ps.setString(30, lc.getBusinessActivitySubTile4());
	        ps.setString(31, lc.getBusinessActivityMainTile5());
	        ps.setString(32, lc.getBusinessActivitySubTile5());
	        status = ps.executeUpdate();
	        if(status>0) {
	        	 log.info("Successfully saved to Database!");
	        }	       
		} catch (Exception e) {
			log.error("Unable to save excel values on Database! " + e.getMessage() );
			log.error("Cause " + e.getCause());
			log.error("Trace " + e.getStackTrace());
		}finally {
			try {
				if(rs!=null) {
					rs.close();
				}
			} catch (Exception e2) {
				log.error("Cannot close result set object! " + e2);
			}
			try {
				if(ps!=null) {
					ps.close();
				}
			} catch (Exception e2) {
				log.error("Cannot close prepared statement object! " + e2);
			}
			try {
				if(connection!=null) {
					connection.close();
				}
			} catch (Exception e2) {
				log.error("Cannot close connection object! " + e2);
			}
		}
		return status;
	}
}

