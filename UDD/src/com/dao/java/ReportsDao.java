package com.dao.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.database.java.ConnectionManager;
import com.entities.java.LocationalClearance;

public class ReportsDao {

	private static final Logger log = Logger.getLogger(ReportsDao.class);
	
	public List<LocationalClearance> generateReports(String action, String reportCriteria, String inputCriteria){
		List<LocationalClearance> itemsForReports = new ArrayList<LocationalClearance>();
		String query = "";
		switch(action) {
			case "generateNewBusiness":
			query = "select * from tbl_master_records where MP_NUMBER like ?";	
			break;
			
			case "generateOldBusiness":
			query = "select * from tbl_master_records where MP_NUMBER not like ?";
			break;
			
			case "generateFromMpDigit":
			query = "select * from tbl_master_records where MP_NUMBER like ?";
			reportCriteria = inputCriteria;
			break;
			
			case "generateByYearAdded":
			query = "select * from tbl_master_records where YEAR_ADDED like ?";
			reportCriteria = inputCriteria;
			break;
		}
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ConnectionManager connect = new ConnectionManager();
			connection = connect.getConnection();
			ps = connection.prepareStatement(query);
			ps.setString(1, "%"+ reportCriteria + "%");
			rs = ps.executeQuery();
			while(rs.next()) {
				LocationalClearance lce = new LocationalClearance();
				lce.setId(rs.getInt("id"));
				lce.setTransactionNumber(rs.getString("TRANSACTION_NUMBER"));
				lce.setBusinessName(rs.getString("BUSINESS_NAME"));
				lce.setTaxPayerName(rs.getString("TAXPAYERS_NAME"));
				lce.setNoOfEmployeesMale(rs.getInt("NO_OF_EMPLOYEES_MALE"));
				lce.setNoOfEmployeesFemale(rs.getInt("NO_OF_EMPLOYEES_FEMALE"));
				lce.setSex(rs.getString("SEX"));
				lce.setRightOverLand(rs.getString("RIGHT_OVER_LAND"));
				lce.setLandRate(rs.getString("LAND_RATE"));
				lce.setCapitalization(rs.getString("CAPITALIZATION"));
				lce.setAddress(rs.getString("ADDRESS"));
				lce.setBarangay(rs.getString("BARANGAY"));
				lce.setDistrict(rs.getInt("DISTRICT"));
				lce.setBusinessActivityMainTile1(rs.getString("BUSINESS_ACTIVITY_MAINTILE1"));
				lce.setBusinessActivitySubTile1(rs.getString("BUSINESS_ACTIVITY_SUBTILE1"));
				lce.setBusinessActivityMainTile2(rs.getString("BUSINESS_ACTIVITY_MAINTILE2"));
				lce.setBusinessActivitySubTile2(rs.getString("BUSINESS_ACTIVITY_SUBTILE2"));
				lce.setBusinessActivityMainTile3(rs.getString("BUSINESS_ACTIVITY_MAINTILE3"));
				lce.setBusinessActivitySubTile3(rs.getString("BUSINESS_ACTIVITY_SUBTILE3"));
				lce.setBusinessActivityMainTile4(rs.getString("BUSINESS_ACTIVITY_MAINTILE4"));
				lce.setBusinessActivitySubTile4(rs.getString("BUSINESS_ACTIVITY_SUBTILE4"));
				lce.setBusinessActivityMainTile5(rs.getString("BUSINESS_ACTIVITY_MAINTILE5"));
				lce.setBusinessActivitySubTile5(rs.getString("BUSINESS_ACTIVITY_SUBTILE5"));
				lce.setMpNo(rs.getString("MP_NUMBER"));
				lce.setCpdoNo(rs.getString("CPDO_NUMBER"));
				lce.setDateApplied(rs.getString("DATE_APPLIED"));
				lce.setDateValid(rs.getInt("DATE_VALID"));
				lce.setDateIssued(rs.getString("DATE_ISSUED"));
				lce.setValidUntil(rs.getString("VALID_UNTIL"));
				lce.setStatus(rs.getString("STATUS"));
				lce.setVerifier(rs.getString("VERIFIER"));
				lce.setLotArea(rs.getDouble("LOT_AREA"));
				lce.setBusinessArea(rs.getDouble("BUSINESS_AREA"));
				lce.setPisc(rs.getString("PISC"));
				lce.setZoningClass(rs.getString("ZONING_CLASS"));
				lce.setEvaluation(rs.getString("EVALUATION"));
				lce.setZoningOfficial(rs.getString("ZONING_OFFICIAL"));
				lce.setRemarks(rs.getString("REMARKS"));
				lce.setRemarks2(rs.getString("REMARKS2"));
				lce.setRemarks3(rs.getString("REMARKS3"));
				lce.setOtherRemarks(rs.getString("OTHER_REMARKS"));
				lce.setOtherRemarks2(rs.getString("OTHER_REMARKS2"));
				lce.setOtherRemarks3(rs.getString("OTHER_REMARKS3"));
				lce.setNoticeOfAction(rs.getString("NOTICE_OF_ACTION"));
				lce.setOtherNoticeOfAction(rs.getString("OTHER_NOTICE_OF_ACTION"));
				lce.setYearAdded(rs.getString("YEAR_ADDED"));
				lce.setEvaluationOfFacts(rs.getString("EVALUATION_OF_FACTS"));
				lce.setTotal(rs.getDouble("TOTAL"));
				lce.setTotalInWords(rs.getString("TOTAL_IN_WORDS"));
				lce.setApplicationStatus(rs.getString("APPLICATION_STATUS"));
				itemsForReports.add(lce);
			}
		} catch (Exception e) {
			log.error("Error on generating reports on ReportsDao \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		} finally {
			try {if(rs!=null) {rs.close();} } catch (Exception e2) {e2.printStackTrace();}
			try {if(ps!=null) {ps.close();} } catch (Exception e2) {e2.printStackTrace();}
			try {if(rs!=connection) {connection.close();} } catch (Exception e2) {e2.printStackTrace();}
		}
		return itemsForReports;
	}
}
