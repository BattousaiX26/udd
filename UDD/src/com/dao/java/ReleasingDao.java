package com.dao.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.database.java.ConnectionManager;
import com.entities.java.LocationalClearance;
import com.entities.java.Releasing;
import com.utilities.java.MpUtilities;

public class ReleasingDao {
	private ConnectionManager connect;
	private static final String APPLICATION_STATUS = "RELEASED";
	private static final Logger log = Logger.getLogger(ReleasingDao.class);
	 
	public ReleasingDao() {
		connect = new ConnectionManager();
	}
	public boolean isSaved(Releasing rls){
		 boolean isSaved = false;
		 boolean isSavedReleasing = false;
		 boolean isUpdatedStatus = false;
		 //saving on two tables
		 String query="insert into tbl_releasing_records (MP_NUMBER,TRANSACTION_NUMBER,BUSINESS_NAME,TAXPAYERS_NAME,RECEIPT_NUMBER,AMOUNT,SEAL_NUMBER,DATE_ENCODED) values(?,?,?,?,?,?,?,?)";
		 String query2="update tbl_master_records set APPLICATION_STATUS=? where TRANSACTION_NUMBER = ?";
		 Connection connection = null;
		 PreparedStatement ps = null; 
		 try {	
			 String dateEncoded = MpUtilities.getCurrentDate();
			 connection = connect.getConnection();
			 ps = connection.prepareStatement(query);
			 ps.setString(1, rls.getMpNumber());
			 ps.setString(2, rls.getTransactionNumber());
			 ps.setString(3, rls.getBusinessName());
			 ps.setString(4, rls.getTaxPayerName());
			 ps.setString(5, rls.getReceiptNumber());
			 ps.setString(6, rls.getAmount());
			 ps.setString(7, rls.getSealNumber());
			 ps.setString(8, dateEncoded);
			 
			 int checker = ps.executeUpdate();
			 if(checker>0) {
				 isSavedReleasing = true;
			 }
		} catch (Exception e) {
			log.error("Unable to save on ReleasingDao using String query \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}finally {
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
		try {
			 connection = connect.getConnection();
			 ps = connection.prepareStatement(query2);
			 ps.setString(1, APPLICATION_STATUS);
			 ps.setString(2, rls.getTransactionNumber());
			 int checker2 = ps.executeUpdate();
			 if(checker2>0) {
				 isUpdatedStatus = true;
			 }
		} catch (Exception e) {
			log.error("Unable to save on ReleasingDao using String query2 \n" + e.getMessage());
			log.error("Cause" + e.getCause());
		}finally {
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
		if(isSavedReleasing==true&&isUpdatedStatus==true) {
			isSaved = true;
		}
		return isSaved;
	}	
	public List<Releasing> getMasterRecords(Releasing rls){
		List<Releasing> releasingRecords = new ArrayList<Releasing>();
		String query = "select top 1 * from tbl_master_records where TRANSACTION_NUMBER=? order by id desc";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			connection = connect.getConnection();
			ps = connection.prepareStatement(query);
			ps.setString(1, rls.getTransactionNumber());
			rs = ps.executeQuery();
			while(rs.next()) {
				Releasing assign = new Releasing();
				int id = rs.getInt("id");assign.setId(id);
				String mpNumber = rs.getString("MP_NUMBER");assign.setMpNumber(mpNumber);
				String transactionNumber = rs.getString("TRANSACTION_NUMBER");assign.setTransactionNumber(transactionNumber);
				String taxPayerName = rs.getString("TAXPAYERS_NAME");assign.setTaxPayerName(taxPayerName);
				String businessName = rs.getString("BUSINESS_NAME");assign.setBusinessName(businessName);
				double amount = rs.getDouble("TOTAL");
				String amountFormatted = MpUtilities.retainTwoDecimal(amount);assign.setAmount(amountFormatted);
				releasingRecords.add(assign);
			}
		} catch (Exception e) {
			log.error("Error on getMasterRecords in ReleasingDao \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}finally {
			try {if(rs!=null) {rs.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
		return releasingRecords;
	}
	public List<Releasing> getReleasingRecords(Releasing rls){
		List<Releasing> releasingRecords = new ArrayList<Releasing>();
		String query = "select top 1 * from tbl_releasing_records where TRANSACTION_NUMBER=? order by id desc";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			connection = connect.getConnection();
			ps = connection.prepareStatement(query);
			ps.setString(1, rls.getTransactionNumber());
			rs = ps.executeQuery();
			while(rs.next()) {
				Releasing assign = new Releasing();
				int id = rs.getInt("id");assign.setId(id);
				String mpNumber = rs.getString("MP_NUMBER");assign.setMpNumber(mpNumber);
				String transactionNumber = rs.getString("TRANSACTION_NUMBER");assign.setTransactionNumber(transactionNumber);
				String businessName = rs.getString("BUSINESS_NAME");assign.setBusinessName(businessName);
				String receiptNumber = rs.getString("RECEIPT_NUMBER");assign.setReceiptNumber(receiptNumber);
				String taxPayerName = rs.getString("TAXPAYERS_NAME");assign.setTaxPayerName(taxPayerName);
				String amount = rs.getString("AMOUNT");assign.setAmount(amount);
				String sealNumber = rs.getString("SEAL_NUMBER");assign.setSealNumber(sealNumber);
				String dateEncoded = rs.getString("DATE_ENCODED");assign.setDateEncoded(dateEncoded);
				releasingRecords.add(assign);
			}
		} catch (Exception e) {
			log.error("Error on getReleasingRecords in ReleasingDao \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}finally {
			try {if(rs!=null) {rs.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
		return releasingRecords;	
	}
	public List<LocationalClearance> getItemsWithStatus(LocationalClearance lc) throws SQLException{
		 List<LocationalClearance> items = new ArrayList<LocationalClearance>();
		 Connection connection = null;
		 PreparedStatement ps = null;
		 ResultSet rs = null;
		 String query = "select * from tbl_master_records where TRANSACTION_NUMBER=?"; 
		 try {
			 connection = connect.getConnection();
			 ps = connection.prepareStatement(query);
			 ps.setString(1, lc.getTransactionNumber());
			 rs = ps.executeQuery();
			 while(rs.next()) {
				LocationalClearance assign = new LocationalClearance();
				int id = rs.getInt("id");assign.setId(id);
				String mpNumber = rs.getString("MP_NUMBER");assign.setMpNo(mpNumber);
				String transactionNumber = rs.getString("TRANSACTION_NUMBER");assign.setTransactionNumber(transactionNumber);
				String businessName = rs.getString("BUSINESS_NAME");assign.setBusinessName(businessName);
				String address = rs.getString("ADDRESS");assign.setAddress(address);
				String applicationStatus = rs.getString("APPLICATION_STATUS");assign.setStatus(applicationStatus);
				String dateApplied = rs.getString("DATE_APPLIED");assign.setDateApplied(dateApplied);
				String dateIssued  = rs.getString("DATE_ISSUED");assign.setDateIssued(dateIssued);
				String barangay = rs.getString("BARANGAY");assign.setBarangay(barangay);
				int district = rs.getInt("DISTRICT");assign.setDistrict(district);
				items.add(assign);
			 }
		} catch (Exception e) {
			log.error("Error on getItemsWithStatus in ReleasingDao \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}finally {
			try {if(rs!=null) {rs.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
		return items;
	}
	public List<Releasing> generateOrderOfPayment(Releasing rls){
		List<Releasing> releasingRecords = new ArrayList<Releasing>();
//		String query = "select top 1 *,tbl_master_records.ADDRESS,tbl_master_records.ADDRESS,tbl_master_records.DISTRICT,tbl_master_records.BARANGAY,tbl_master_records.DATE_VALID"
//				+ ", tbl_master_records.TOTAL, tbl_master_records.TOTAL_IN_WORDS from tbl_releasing_records "
//				+ "JOIN  tbl_master_records ON tbl_master_records.TRANSACTION_NUMBER = tbl_releasing_records.TRANSACTION_NUMBER where tbl_releasing_records.TRANSACTION_NUMBER=? order by tbl_releasing_records.id desc";
		String query ="select * from tbl_master_records where TRANSACTION_NUMBER=?";
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			connection = connect.getConnection();
			ps = connection.prepareStatement(query);
			ps.setString(1, rls.getTransactionNumber());
			rs = ps.executeQuery();
			while(rs.next()) {
				Releasing assign = new Releasing();
				int id = rs.getInt("id");assign.setId(id);
				String mpNumber = rs.getString("MP_NUMBER");assign.setMpNumber(mpNumber);
				String transactionNumber = rs.getString("TRANSACTION_NUMBER");assign.setTransactionNumber(transactionNumber);
				String businessName = rs.getString("BUSINESS_NAME");assign.setBusinessName(businessName);
				String taxPayerName = rs.getString("TAXPAYERS_NAME");assign.setTaxPayerName(taxPayerName);
				String address = rs.getString("ADDRESS");assign.setAddress(address);
				int district = rs.getInt("DISTRICT");assign.setDistrict(district);
				String barangay = rs.getString("BARANGAY");assign.setBarangay(barangay);
				String totalInWords = rs.getString("TOTAL_IN_WORDS");assign.setTotalInWords(totalInWords);
				double total = rs.getDouble("TOTAL");
				String formattedTotal = MpUtilities.retainTwoDecimal(total);assign.setTotal(formattedTotal);
				String dateValid = rs.getString("DATE_VALID");assign.setDateValid(dateValid);
				releasingRecords.add(assign);
			}
		} catch (Exception e) {
			log.error("Error in generateOrderOfPayment in ReleasingDao \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}finally {
			try {if(rs!=null) {rs.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
		return releasingRecords;	
	}
}
