package com.dao.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.database.java.ConnectionManager;
import com.entities.java.Accounts;

public class ManageAccountsDao {
	private ConnectionManager connect;
	final static Logger logger = LogManager.getLogger(ManageAccountsDao.class.getName());
	public ManageAccountsDao() {
		super();
		connect = new ConnectionManager();
	}
	public List<Accounts> getUsers() throws SQLException{	 
		
		 List<Accounts> userItems = new ArrayList<Accounts>();	
		 Connection connection = null;
		 PreparedStatement ps = null;
		 ResultSet rs = null;	 
		 
		 try {
			 connection = connect.getConnection();
			 ps = connection.prepareStatement("select * from tbl_users order by id desc");
			 rs = ps.executeQuery();
			 
			 while(rs.next()) {
				 Accounts accounts = new Accounts();
				 accounts.setId(rs.getInt("id"));
				 accounts.setUsername(rs.getString("username"));
				 accounts.setFullname(rs.getString("fullname"));
				 accounts.setUserLevel(rs.getString("user_level"));
				 accounts.setSection(rs.getString("section"));
				 accounts.setStatus(rs.getString("status"));
				 userItems.add(accounts);
			 }
		} catch (Exception e) {
			logger.error("Unable to get users on ManageAccountsDao " + e.getMessage());
			logger.error("Cause " + e.getCause());
		}finally {
			try {if(rs!=null) {rs.close();}}catch (Exception e) {logger.error("Unable to close result set object" + e.getMessage());}
			try {if(ps!=null) {ps.close();}}catch (Exception e) {logger.error("Unable to close prepared statement object" + e.getMessage());}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {logger.error("Unable to close connection object" + e.getMessage());}
		}
		 return userItems;
	}
	public void deleteUser(int id) throws SQLException {
		
		Connection connection = null;
		PreparedStatement ps = null;
		
        try {
			connection = connect.getConnection();
            ps = connection.prepareStatement("delete from tbl_users where id=?");
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
        	logger.error("Unable to delete users on ManageAccountsDao object" + e.getMessage());
        	logger.error("Cause " + e.getCause());
        }finally {
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
    }	
	public Accounts getUserDetails(int id) throws SQLException {
		
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Accounts userInfo = new Accounts();
        
		try {
			connection = connect.getConnection();
            ps = connection.prepareStatement("select * from tbl_users where id=?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            
            while(rs.next()) {
            	userInfo.setId(rs.getInt("id"));
            	userInfo.setUsername(rs.getString("username"));
            	userInfo.setFullname(rs.getString("fullname"));
            	userInfo.setUserLevel(rs.getString("user_level"));
            	userInfo.setSection(rs.getString("section"));
            	userInfo.setStatus(rs.getString("status"));
            }
        } catch (SQLException e) {
        	logger.error("Unable to getUserDetails on ManageAccountsDao " + e.getMessage());
        	logger.error("Cause " + e.getCause());
        }finally {
        	try {if(rs!=null) {rs.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
        return userInfo;      
    }
	public boolean updateUserInfo(Accounts accounts) {
		
		boolean isSaved = false;
		Connection connection = null;
		PreparedStatement ps = null;
		
		try {
			connection = connect.getConnection();
			ps = connection.prepareStatement("update tbl_users set username=?, fullname=?, user_level=?, section=?, status=? where id=?");
			ps.setString(1, accounts.getUsername());
			ps.setString(2, accounts.getFullname());
			ps.setString(3, accounts.getUserLevel());
			ps.setString(4, accounts.getSection());
			ps.setString(5, accounts.getStatus());
			ps.setInt(6, accounts.getId());
			int status = ps.executeUpdate();
			if(status>0) {
				isSaved = true;
			}
		} catch (Exception e) {
			logger.error("Unable to update user info " + e.getMessage());
			logger.error("Cause " + e.getCause());
		}finally {
			try {if(ps!=null) {ps.close();}}catch (Exception e) {e.printStackTrace();}
			try {if(connection!=null) {connection.close();}}catch (Exception e) {e.printStackTrace();}
		}
		return isSaved;
	}
}
