package com.dao.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.database.java.ConnectionManager;
import com.entities.java.Accounts;
import com.utilities.java.EncryptDecrypt;;

public class RegistrationDao {
	
	private static final Logger log = Logger.getLogger(RegistrationDao.class);
	
	public boolean isSaved(Accounts accounts) throws SQLException {
		boolean saved = false;
		String query = "insert into tbl_users (username, password, fullname, user_level, section, status) values(?,?,?,?,?,?)";
		String password = accounts.getPassword();
		byte[] encryptedPassword = EncryptDecrypt.encryptValue(password);
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
			
		try {
			ConnectionManager connectionManager = new ConnectionManager(); 
			connection=connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, accounts.getUsername());
			preparedStatement.setString(2, Base64.encodeBase64String(encryptedPassword));
			preparedStatement.setString(3, accounts.getFullname());
			preparedStatement.setString(4, accounts.getUserLevel());
			preparedStatement.setString(5, accounts.getSection());
			preparedStatement.setString(6, "pending");
			int save = preparedStatement.executeUpdate();
			
			if(save!=0) {
				saved = true;
			}
		}catch (Exception e) {
			log.error("Unable to save user account " + e.getMessage());
			log.error("Cause " + e.getCause());
		}finally {	
			try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
			try { if (connection != null) connection.close(); } catch (Exception e) {};	
		}
		return saved;
	}
	public boolean userExisting(String username) {
		boolean isExisting = false;
		String query = "select * from tbl_users where username=?";
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		try {
			ConnectionManager connectionManager = new ConnectionManager(); 
			connection=connectionManager.getConnection();
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, username);
			resultSet = preparedStatement.executeQuery();
			
			if(resultSet.next()) {
				isExisting = true;
			}
		} catch (Exception e) {
			log.error("Unable to check user existence " + e.getMessage());
			log.error("Cause " + e.getCause());
		}finally {
			try { if (resultSet != null) resultSet.close(); } catch (Exception e) {};	
			try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {};
			try { if (connection != null) connection.close(); } catch (Exception e) {};	
		}		
		return isExisting; 
	}
}
