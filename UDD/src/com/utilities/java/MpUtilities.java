package com.utilities.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;
import com.database.java.ConnectionManager;

public class MpUtilities {
	private static final Logger log = Logger.getLogger(MpUtilities.class);
	
	public static String generateValidUntil(String dateIssued, int dateValid) {
		String dateToString = "";
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
			LocalDate date = LocalDate.parse(dateIssued, formatter);
			date= date.plusYears(dateValid);
			dateToString = date.format(formatter);	
		} catch (Exception e) {
			log.error("Unable to generateValidUntil on MpUtilities \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}
		return dateToString;
	}
	public static String getCurrentYear() {
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		String yearInString = String.valueOf(year);		
		return yearInString;
	}
	public static String getCurrentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String time = sdf.format(date);
		return time;
	}
	public static int convertToInteger(String input) {
		int convertedToInt = 0;
		try {
			convertedToInt = Integer.parseInt(input);
		} catch (NumberFormatException e) {
			log.error("Unable to convert input to integer on MpUtilities \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		} catch(Exception ex) {
			log.error("Other errors on convertToInteger of MpUtilities \n" + ex.getMessage());
			log.error("Cause " + ex.getCause());
		}
		return convertedToInt;
	}
	public static double convertToDouble(String input) {
		double convertedToDouble = 0;
		try {			
			convertedToDouble = Double.parseDouble(input);
		} catch (NumberFormatException e) {
			log.error("Unable to convert input to double on MpUtilities \n" + e.getMessage());
		} catch(Exception ex) {
			log.error("Other errors on convertToDouble of MpUtilities\n" + ex.getCause());
		}
		return convertedToDouble;
	}
	public static double withCommaToDouble(String input) {
		NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);		
		double d = 0;
		
		try {		
			Number number = format.parse(input);
			d = number.doubleValue(); 
		} catch (NumberFormatException e) {
			log.error("Unable to convert input with comma to double on MpUtilities \n" + e.getMessage());
		} catch(Exception ex) {
			log.error("Other errors on converting to double with comma of MpUtilities\n" + ex.getCause());
		}
		return d;
	}
	public static String removeScientificNotation(double input) {
		String formattedString = "";
		try {
			DecimalFormat df = new DecimalFormat("#");
	        df.setMaximumFractionDigits(2);
	        formattedString = df.format(input);
		} catch (Exception e) {
			log.error("Unable to removeScientifictNotation on MpUtilities \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}
		return formattedString;
	}
	public static String retainTwoDecimal(double input) {
		String formattedString = "";		
		try {
	        formattedString = String.format("%.2f", input);
		} catch (Exception e) {
			log.error("Unable to retainTwoDecimal on MpUtilities \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}
		return formattedString;
	}
	public static String getLast5Digits(String input) {
		String incrementedNumber = "";
		try {
			if (input.length() == 5) {
				incrementedNumber=input;
			} else if (input.length() > 5) {
				String trimmedFiveDigits = input.substring(input.length() - 5);
				int numberToIncrement = MpUtilities.convertToInteger(trimmedFiveDigits);
				numberToIncrement = numberToIncrement + 1;
				incrementedNumber=String.format("%05d", numberToIncrement);
			} 
		} catch (Exception e) {
			log.error("Error on getLast5Digits in MpUtilities \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}
		return incrementedNumber;
	}
	public static String generateTransactionNumber() throws SQLException {
		String transactionNumber = "";
		String currentYear = MpUtilities.getCurrentYear();
		String prefixOfTransactionNumber = "ZAU" + "-" +  currentYear;
		Connection connection = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;	  
		try {
			ConnectionManager connect = new ConnectionManager();
			connection = connect.getConnection();					
			ps = connection.prepareStatement("Select top 1 * from tbl_master_records where YEAR_ADDED=? order by ID desc ");
			ps.setString(1, currentYear);
			rs = ps.executeQuery();
			if(rs.next()) {
				String existingTransactionNumber = rs.getString("TRANSACTION_NUMBER");
				String count = MpUtilities.getLast5Digits(existingTransactionNumber);
				transactionNumber = prefixOfTransactionNumber + "-" + count ;
			}else {
				String defaultCount = "00001";
				transactionNumber = prefixOfTransactionNumber + "-" + defaultCount;
			}
		} catch (Exception e) {
			log.error("Error on generating transaction number in MpUtilities \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		} finally {
			rs.close();
			ps.close();
			connection.close();
		}
		return transactionNumber;		
	}
	public static String getFirstTwofString(String str) {	
	    return str.length() > 2 ? str.substring(str.length() - 2) : str;
	}	
}
