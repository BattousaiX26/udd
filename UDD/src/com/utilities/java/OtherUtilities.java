package com.utilities.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.database.java.ConnectionManager;
import com.entities.java.LocationalClearance;

public class OtherUtilities {
	
	private static final Logger log = Logger.getLogger(OtherUtilities.class);
	
	public List<LocationalClearance> getBarangayList(LocationalClearance lc) throws SQLException{
		List<LocationalClearance> barangayList = new ArrayList<LocationalClearance>();
		Connection connection = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
	    String searchBarangay = "select BARANGAY from tbl_barangay_list where district=?";  
		try {
			ConnectionManager connect = new ConnectionManager();
			connection = connect.getConnection();		
			ps = connection.prepareStatement(searchBarangay);
			ps.setInt(1, lc.getDistrict());
			rs = ps.executeQuery();
			
			while(rs.next()) {
				LocationalClearance locational = new LocationalClearance();
				locational.setBarangay(rs.getString("BARANGAY"));
				barangayList.add(locational);
			}
		} catch (Exception e) {
			log.error("Error in getBarangayList on OtherUtilities \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}finally {
			rs.close();
			ps.close();
			connection.close();
		}
		return barangayList;
	}
}
