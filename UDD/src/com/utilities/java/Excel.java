package com.utilities.java;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import com.dao.java.ExcelDao;
import com.entities.java.LocationalClearance;


public class Excel {
	
	public static final String EXCEL_FILE_PATH = "C:/excel_upload/lc_excel.xlsx";
	final static Logger logger = LogManager.getLogger(Excel.class.getName());
	private LocationalClearance lc;
	private ExcelDao ed;
	
	public Excel() {
		super();
		lc = new LocationalClearance();
		ed = new ExcelDao();
	}
	
	public boolean readAndSaveExcel() throws InvalidFormatException {
		
		File file = new File(EXCEL_FILE_PATH);
		int isSaved = 0;
		boolean isSuccessful = false;
		if(file.exists()) {
			try {	
				// Creating a Workbook from an Excel file (.xls or .xlsx)
				Workbook workbook = WorkbookFactory.create(new File(EXCEL_FILE_PATH));
				
				// Retrieving the number of sheets in the Workbook
		        System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");
		         
		        // 1. You can obtain a sheetIterator and iterate over it
		        Iterator<Sheet> sheetIterator = workbook.sheetIterator();
		        System.out.println("Retrieving Sheets using Iterator");
		        while (sheetIterator.hasNext()) {
		            Sheet sheet = sheetIterator.next();
		            System.out.println("=> " + sheet.getSheetName());
		        }
		        
		        // Getting the Sheet at index zero
		        Sheet sheet = workbook.getSheetAt(0);
		        
		        // Create a DataFormatter to format and get each cell's value as String
		        DataFormatter dataFormatter = new DataFormatter();
		        
		        // 1. You can obtain a rowIterator and columnIterator and iterate over them
		        System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
		        Iterator<Row> rowIterator = sheet.rowIterator();
		        if(rowIterator.hasNext()) {
		        	rowIterator.next();
		        	while (rowIterator.hasNext()) {
			            Row row = rowIterator.next();

			            // Now let's iterate over the columns of the current row
			            Iterator<Cell> cellIterator = row.cellIterator();

			            while (cellIterator.hasNext()) {
			                Cell cell = cellIterator.next();
			                String cellValue = dataFormatter.formatCellValue(cell);
			                System.out.print(cellValue + "\t");
			            }
			            String cpdoNo = dataFormatter.formatCellValue(row.getCell(0));lc.setCpdoNo(cpdoNo);
			            String dateApplied = dataFormatter.formatCellValue(row.getCell(1));lc.setDateApplied(dateApplied);
			            String mpNo = dataFormatter.formatCellValue(row.getCell(2));lc.setMpNo(mpNo);
			            String status = dataFormatter.formatCellValue(row.getCell(3));lc.setStatus(status);
			            String datePosted = dataFormatter.formatCellValue(row.getCell(4));lc.setDatePosted(datePosted);
			            String timePosted = dataFormatter.formatCellValue(row.getCell(5));lc.setTimePosted(timePosted);
			            String zoningClass = dataFormatter.formatCellValue(row.getCell(6));lc.setZoningClass(zoningClass);
			            String evaluation = dataFormatter.formatCellValue(row.getCell(7));lc.setEvaluation(evaluation);
			            String verifier = dataFormatter.formatCellValue(row.getCell(8));lc.setVerifier(verifier);
			            String zoningOfficial = dataFormatter.formatCellValue(row.getCell(9));lc.setZoningOfficial(zoningOfficial);
			            String dateProcessed = dataFormatter.formatCellValue(row.getCell(10));lc.setDateProcessed(dateProcessed);
			            String timeProcessed = dataFormatter.formatCellValue(row.getCell(11));lc.setTimeProcessed(timeProcessed);
			            String remarks = dataFormatter.formatCellValue(row.getCell(12));lc.setRemarks(remarks);			       
			            String lotAreaChecker = dataFormatter.formatCellValue(row.getCell(13));
			            if(lotAreaChecker!=null) {	
			            	if(lotAreaChecker.contains(",")) {
			            		double lotArea = MpUtilities.withCommaToDouble(lotAreaChecker);
			            		lc.setLotArea(lotArea);
			            	}else {
			            		double lotArea = MpUtilities.convertToDouble(lotAreaChecker);
			            		System.out.println(lotArea + " is lot area");
				            	lc.setLotArea(lotArea);
			            	}		            	
			            }
			            String encodedBy = dataFormatter.formatCellValue(row.getCell(14));lc.setEncodedBy(encodedBy);
			            String pisc = dataFormatter.formatCellValue(row.getCell(15));lc.setPisc(pisc);
			            String rightOverLand = dataFormatter.formatCellValue(row.getCell(16));lc.setRightOverLand(rightOverLand);
			            String landRate = dataFormatter.formatCellValue(row.getCell(17));lc.setLandRate(landRate);
			            String businessName = dataFormatter.formatCellValue(row.getCell(18));lc.setBusinessName(businessName);			            
			            String taxPayerName = dataFormatter.formatCellValue(row.getCell(19));lc.setTaxPayerName(taxPayerName);
			            String address = dataFormatter.formatCellValue(row.getCell(20));lc.setAddress(address);
			            String capitalization = dataFormatter.formatCellValue(row.getCell(21));lc.setCapitalization(capitalization);
			            String businessActivityMainTile1 = dataFormatter.formatCellValue(row.getCell(22));lc.setBusinessActivityMainTile1(businessActivityMainTile1);
			            String businessActivitySubtile1 = dataFormatter.formatCellValue(row.getCell(23));lc.setBusinessActivitySubTile1(businessActivitySubtile1);
			            String businessActivityMainTile2 = dataFormatter.formatCellValue(row.getCell(24));lc.setBusinessActivityMainTile2(businessActivityMainTile2);
			            String businessActivitySubtile2 = dataFormatter.formatCellValue(row.getCell(25));lc.setBusinessActivitySubTile2(businessActivitySubtile2);
			            String businessActivityMaintile3 = dataFormatter.formatCellValue(row.getCell(26));lc.setBusinessActivityMainTile3(businessActivityMaintile3);
			            String businessActivitySubtile3 = dataFormatter.formatCellValue(row.getCell(27));lc.setBusinessActivitySubTile3(businessActivitySubtile3);
			            String businessActivityMainTile4 = dataFormatter.formatCellValue(row.getCell(28));lc.setBusinessActivityMainTile4(businessActivityMainTile4);
			            String businessActivitySubTile4 = dataFormatter.formatCellValue(row.getCell(29));lc.setBusinessActivitySubTile4(businessActivitySubTile4); 
			            String businessActivityMainTile5 = dataFormatter.formatCellValue(row.getCell(30));lc.setBusinessActivityMainTile5(businessActivityMainTile5);
			            String businessActivitySubtile5 = dataFormatter.formatCellValue(row.getCell(31));lc.setBusinessActivitySubTile5(businessActivitySubtile5);
			       
			            try {
			            	isSaved = ed.InsertRowInDB(lc);
			            	if(isSaved>0) {
			            		isSuccessful = true;
			            	}
			            	
						} catch (Exception e) {
							logger.fatal("Unable to save excel rows to database on Excel " + e.getMessage());
							logger.error("Cause " + e.getCause());
							logger.error("Trace " + e.getStackTrace());
						}    
			            System.out.println();
			        } 
		        }	            
		        if(workbook!=null) {
		            // Closing the workbook
			        workbook.close();
		        }  
			}catch (IOException e) {
				logger.fatal("Unable to read excel on Excel class " + e.getMessage());
				logger.error("Cause " + e.getCause());
				logger.error("Trace " + e.getStackTrace());
			}
		}else {
			System.out.println("File does not exists!");
		}
		return isSuccessful;
	}
}
