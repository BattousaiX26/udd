package com.utilities.java;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class EncryptDecrypt {
	
	private static final Logger log = Logger.getLogger(EncryptDecrypt.class);
	
	public static byte[] encryptValue(String value){
		String key = "Bar12345Bar12345";
		String initVector = "RandomInitVector";
		byte encrypted[] = null; 
		
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            encrypted = cipher.doFinal(value.getBytes());
		} catch (Exception e) {
			log.error("Error in ecrypting value on EncryptDecrypt \n" + e.getMessage());
			log.error("Cause " + e.getCause());
		}		
		return encrypted;				
	}
	public static byte[] decryptValue (String encryptedValue) {
		 String key = "Bar12345Bar12345"; // 128 bit key
	     String initVector = "RandomInitVector"; // 16 bytes IV
	     byte[] original = null;
	     
	     try {
	    	 IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
	         SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
	         Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	         cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
	         original = cipher.doFinal(Base64.decodeBase64(encryptedValue));
		} catch (Exception e) {
			log.error("Error in decrypting value on EncryptDecrypt \n" + e.getMessage());
			log.error("Cause" + e.getCause());
		}   
	     return original;
	 }
}
